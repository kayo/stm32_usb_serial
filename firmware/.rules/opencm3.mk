# opencm3 library

libopencm3.platform ?= stm32f0
libopencm3.BASEDIR ?= $(LIBSDIR)/opencm3/
libopencm3.CDEFS := USE_OPENCM3
libopencm3.CDIRS := $(libopencm3.BASEDIR)include
libopencm3.LDDIRS := $(libopencm3.BASEDIR)lib
libopencm3.LIB := $(libopencm3.BASEDIR)lib/libopencm3_$(libopencm3.platform).a
libopencm3.LDSCRIPTS := $(libopencm3.BASEDIR)lib/libopencm3_$(libopencm3.platform).ld

build: build.libopencm3
build.libopencm3: $(libopencm3.LIB)
$(libopencm3.LIB):
	@echo TARGET libopencm3 BUILD
	-$(Q)CFLAGS='-std=c99 $(if $(call option-true,$(stalin.lto)),-flto)' make -C $(libopencm3.BASEDIR)

clean: clean.libopencm3
clean.libopencm3:
	@echo TARGET libopencm3 CLEAN
	-$(Q)make -C $(libopencm3.BASEDIR) clean
