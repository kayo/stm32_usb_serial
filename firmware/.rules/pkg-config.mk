# <library-name> <package-name>
define pkg-config-rules
$(1).CFLAGS := $(shell pkg-config --cflags $(2))
$(1).LDFLAGS := $(shell pkg-config --libs $(2))
endef

pkg-config = $(eval $(call pkg-config-rules,$(1),$(or $(2),$(1))))
