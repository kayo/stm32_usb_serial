# USB Serial firmware #

This firmware implements advanced USB to UART interface on STM32F103xx microcontrollers with USB hardware.

## Features ##

* Compound USB device which includes:
    - Generic USB CDC ACM interfaces for data exchange
    - USB HID interface for configure device functions
* Up to three independent UART ports
    - Hi-speed duplex data exchange
    - Hardware-driven I/O using DMA
* Different physical layers support
    - UART TX/RX lines
    - RS485 phy (optional)
    - RS232 phy (optional)
* Configurable indication using LED﻿s
    - Up to seven software-controlled LED﻿s
    - Independent LED function and mode setup

## Building ##

This project uses complex make rules which helps configure, compile, debug and flash firmware.
You need any GCC-based ARM None-EABI toolchain to be able to build this firmware.

To compile firmware use the next command:
```
make build
```
or simply:
```
make
```

To cleanup previous compilation run the next:
```
make clean
```

## Flashing ##

To flash firmware you need SWD/JTAG interface and Open﻿OCD tool.

Before first programming you usually need deactivate flash-memory protection:
```
make unprotect
```

You can write firmware to device by the next command:
```
make flash.img.main
```

Firmware writing do not erase configuration memory so you need to do it manually after any changes of configuration structure. The next command can help you to erase all flash memory at once:
```
make erase
```
Of course after that you need re-upload firmware to device.

After final programming you can activate flash-memory protection back:
```
make protect
```

## Hacking ##

The next commands help you to develop and debug firmware.

Start Open﻿OCD by the command:
```
make debug
```

Begin debugging using that:
```
make debug.main
```

*[USB]: Universal Serial Bus
*[CDC]: Communication Device Class
*[ACM]: Abstract Control Model
*[HID]: Human Interface Device
*[I/O]: Input/Output
*[LED]: Light-Emitting Diode
*[UART]: Universal Asynchronous Receiver Transmitter
*[DMA]: Direct Memory Access
*[ARM]: Advanced RISC Machines
*[EABI]: Embedded Application Binary Interface
*[JTAG]: Joint Test Action Group (IEEE 1149 Debug Interface)
*[SWD]: Serial Wire Debug
*[GCC]: GNU Compiler Collection
*[GNU]: GNU Not Unix
*[OCD]: On-Chip Debugger
