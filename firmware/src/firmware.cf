[option "device"]
info = The name of device
type = str
key = FIRMWARE_DEVICE
def = usb_serial

[option "version"]
info = The version of firmware
type = str
key = FIRMWARE_VERSION
def = $(shell git describe --tags)

[option "language"]
info = The native language
type = val
key = FIRMWARE_LANGUAGE
def = en

[option "io.packet_size"]
info = The maximum size of I/O packet
type = val
key = IO_PACKET_SIZE
def = 64

[option "io.queue_length"]
info = The maximum size of I/O queue
type = val
key = IO_QUEUE_LENGTH
def = 255

[option "usb.vendor.id"]
info = The vendor ID for USB device
type = val
key = USB_VENDOR_ID
#def = 0483
def = 6666 # VID reserved for prototypes

[option "usb.product.id"]
info = The product ID for USB device
type = val
key = USB_PRODUCT_ID
#def = 5740 #7270
def = 0001 # The arbitrary PID for prototyping

[option "usb.vendor"]
info = The vendor string for USB device
type = str
key = USB_VENDOR_STR
def = Illumium.Org

[option "usb.product"]
info = The product string for USB device
type = str
key = USB_PRODUCT_STR
def = USB to 3 UART/RS485 adapter

[option "usb.ctrl_buffer_size"]
info = The size of USB control buffer
type = val
key = USB_CTRL_BUFFER_SIZE
def = 1024

[option "usb.ctrl_packet_size"]
info = The packet size for control (zero) end-point
type = val
key = USB_CTRL_PACKET_SIZE
def = 32

[option "usb.max_power"]
info = The maximum supplied power of USB device in mA
type = val
key = USB_MAX_POWER
def = 100

[option "usb.poll_in_isr"]
info = Poll USB using interrupt service routines
type = opt
key = USB_POLL_IN_ISR
def = y

[option "usb.suspend_support"]
info = Have or not suspend support
type = opt
key = USB_SUSPEND_SUPPORT
def = n

[option "usb.cdc.data_packet_size"]
info = The data packet size for CDC
type = val
key = USB_CDC_DATA_PACKET_SIZE
def = $(io.packet_size)

[option "usb.cdc.comm_packet_size"]
info = The control packet size for CDC
type = val
key = USB_CDC_COMM_PACKET_SIZE
def = 16

[option "usb.cdc.serial_protocol"]
info = Protocol for USB CDC interface and function descriptors (NONE or AT)
type = val
key = USB_CDC_SERIAL_PROTOCOL
def = NONE

[option "usb.cdc.serial_state_notification"]
info = Send serial state notifications
type = opt
key = USB_CDC_SERIAL_STATE_NOTIFICATION
def = y

[option "config_through_hid"]
info = Switch RS485 mode using HID
type = opt
key = CONFIG_THROUGH_HID
def = y

[option "usb.hid.req_packet_size"]
info = The HID request packet size
type = val
key = USB_HID_REQ_PACKET_SIZE
def = 16

[option "usb.hid.res_packet_size"]
info = The HID response packet size
type = val
key = USB_HID_RES_PACKET_SIZE
def = 32

[option "uart.baudrate"]
info = The default baud rate for UART
type = val
key = UART_BAUDRATE_DEF
def = 9600

[option "uart.databits"]
info = The default data bits for UART
type = val
key = UART_DATABITS_DEF
def = 8

[option "uart.stopbits"]
info = The default stop bits for UART
type = val
key = UART_STOPBITS_DEF
def = 1

[option "uart.parity"]
info = The default parity for UART
type = val
key = UART_PARITY_DEF
def = none

[option "uart.packet_size"]
info = The packet size for UARTs
type = val
def = $(io.packet_size)

[option "uart.queue_size"]
info = The queue size for UARTs                                                                                               
type = val
def = 128

[option "uart.rx.packet_size"]
info = The rx packet size for UARTs
type = val
key = UART_RX_PACKET_SIZE
def = $(uart.packet_size)

[option "uart.rx.queue_size"]                                                                                                 
info = The rx queue size for UARTs                                                                                            
type = val
key = UART_RX_QUEUE_SIZE
def = $(uart.queue_size)

[option "uart.tx.packet_size"]
info = The tx packet size for UARTs
type = val
key = UART_TX_PACKET_SIZE
def = $(uart.packet_size)

[option "uart.tx.queue_size"]                                                                                                 
info = The tx queue size for UARTs                                                                                            
type = val
key = UART_TX_QUEUE_SIZE
def = $(uart.queue_size)

[option "leds.light_time"]
info = The minimum time of LED light in mS
type = val
key = LEDS_LIGHT_TIME_MS
def = 150

[option "leds.blink_cycle"]
info = The on time of LED blink in mS
type = val
key = LEDS_BLINK_CYCLE_MS
def = 100

[option "leds.blink_pause"]
info = The off time of LED blink in mS
type = val
key = LEDS_BLINK_PAUSE_MS
def = 100

[option "leds.flash_cycle"]
info = The on time of LED flash in mS
type = val
key = LEDS_FLASH_CYCLE_MS
def = 50

[option "leds.flash_period"]
info = The off time of LED flash in mS
type = val
key = LEDS_FLASH_PAUSE_MS
def = 450
