#include <stddef.h>

#include <libopencm3/usb/usbd.h>
#include <libopencm3/usb/hid.h>

#include "macro.h"
#include "config.h"
#include "dbglog.h"

#include "usb_def.h"
#include "hid_def.h"
#include "hid_proto.h"

#include "usb_dev.h"
#include "acm_dev.h"
#include "hid_dev.h"
#include "uart_dev.h"
#include "leds_dev.h"

#include "events.h"
#include "setting.h"

#define USB_HID_VERSION 0111 /* 1.11 */
#define USB_HID_VERSION_DEF _CAT2(0x, USB_HID_VERSION)

#if CONFIG_THROUGH_HID

#ifdef usb_hid
#define CTL_CONF usb_hid
#define CTL_IN_EP USB_ENDPOINT_ADDR_IN(CTL_CONF)
#define CTL_OUT_EP USB_ENDPOINT_ADDR_OUT(CTL_CONF)
#endif /* usb_hid */

#define CTL_INDEX (ACM_COUNT)
#define CTL_CTRL_IF (CTL_INDEX*2)

#ifdef CTL_CONF
static const struct usb_endpoint_descriptor ctrl_endp[] = {
  {
    .bLength = USB_DT_ENDPOINT_SIZE,              /* The size of descriptor in bytes */
    .bDescriptorType = USB_DT_ENDPOINT,           /* The type of descriptor */
    .bEndpointAddress = CTL_OUT_EP,               /* The address of end-point */
    /* The value of seventh bit means data direction: 0 - output, 1 - input */
    .bmAttributes = USB_ENDPOINT_ATTR_INTERRUPT,  /* The bits of attributes */
    /* The transfer type, the type of synchronization and the usage type for isochronous */
    .wMaxPacketSize = USB_HID_REQ_PACKET_SIZE,    /* The maximum size of packet */
    .bInterval = 10,                              /* The polling interval */
  },
  {
    .bLength = USB_DT_ENDPOINT_SIZE,              /* The size of descriptor in bytes */
    .bDescriptorType = USB_DT_ENDPOINT,           /* The type of descriptor */
    .bEndpointAddress = CTL_IN_EP,                /* The address of end-point */
    /* The value of seventh bit means data direction: 0 - output, 1 - input */
    .bmAttributes = USB_ENDPOINT_ATTR_INTERRUPT,  /* The bits of attributes */
    /* The transfer type, the type of synchronization and the usage type for isochronous */
    .wMaxPacketSize = USB_HID_RES_PACKET_SIZE,    /* The maximum size of packet */
    .bInterval = 10,                              /* The polling interval */
  }
};
#endif /* CTL_CONF */

static const uint8_t ctl_report_descriptor[] = {
  /**/HID_USAGE_PAGE(USB_UART3),
  /**/HID_USAGE(USB_UART3),
  
  /**/HID_REPORT_ID(HID_REPORT_REBOOT),
  /**/HID_COLLECTION(APPLICATION),
  /**/  HID_REPORT_SIZE(8),
  /**/  HID_REPORT_COUNT(1),
  /**/  HID_FEATURE(CONSTANT, VARIABLE, ABSOLUTE),
  /**/HID_END_COLLECTION(APPLICATION),
  
  /**/HID_REPORT_ID(HID_REPORT_RS485_MODE),
  /**/HID_COLLECTION(APPLICATION),
  /**/  HID_USAGE(USB_UART3),
  /**/  HID_COLLECTION(PHYSICAL),
  /**/    HID_USAGE(UART_ID(uart_name_a)),
  /**/    HID_USAGE(UART_ID(uart_name_b)),
  /**/    HID_USAGE(UART_ID(uart_name_c)),
  /**/    HID_LOGICAL_MINIMUM(1, rs485_mode_min),
  /**/    HID_LOGICAL_MAXIMUM(1, rs485_mode_max),
  /**/    HID_REPORT_SIZE(rs485_mode_len),
  /**/    HID_REPORT_COUNT(ACM_COUNT),
  /**/    HID_FEATURE(DATA, VARIABLE, ABSOLUTE), /* mode bits */
  /**/    HID_REPORT_SIZE(8 - rs485_mode_len * ACM_COUNT),
  /**/    HID_REPORT_COUNT(1),
  /**/    HID_FEATURE(CONSTANT), /* padding */
  /**/  HID_END_COLLECTION(PHYSICAL),
  /**/HID_END_COLLECTION(APPLICATION),

#define LED_MODE_REPORT(name)                        \
  /**/  HID_COLLECTION(PHYSICAL),                    \
  /**/    HID_USAGE(LED_ID(_CAT2(led_name_, name))), \
  /**/    HID_COLLECTION(LOGICAL),                   \
  /**/      HID_USAGE(LED_EVT),                      \
  /**/      HID_LOGICAL_MINIMUM(1, led_evt_min),     \
  /**/      HID_LOGICAL_MAXIMUM(1, led_evt_max),     \
  /**/      HID_REPORT_SIZE(led_evt_len),            \
  /**/      HID_REPORT_COUNT(1),                     \
  /**/      HID_FEATURE(DATA, VARIABLE, ABSOLUTE),   \
  /**/      HID_USAGE(LED_ACT),                      \
  /**/      HID_LOGICAL_MINIMUM(1, led_act_min),     \
  /**/      HID_LOGICAL_MAXIMUM(1, led_act_max),     \
  /**/      HID_REPORT_SIZE(led_act_len),            \
  /**/      HID_REPORT_COUNT(1),                     \
  /**/      HID_FEATURE(DATA, VARIABLE, ABSOLUTE),   \
  /**/    HID_END_COLLECTION(LOGICAL),               \
  /**/  HID_END_COLLECTION(PHYSICAL),
  
  /**/HID_REPORT_ID(HID_REPORT_LEDS_MODE),
  /**/HID_COLLECTION(APPLICATION),
  /**/  LED_MODE_REPORT(red)
  /**/  LED_MODE_REPORT(green)
  /**/  LED_MODE_REPORT(yellow)
  /**/  /*_MAP(LED_MODE_REPORT, leds_list)*/
  /**/HID_END_COLLECTION(APPLICATION),
};

static const struct {
  struct usb_hid_descriptor hid_descriptor;
  struct {
    uint8_t bReportDescriptorType;
    uint16_t wDescriptorLength;
  } __packed__ hid_report;
} __packed__ ctl_functional_descriptors = {
  .hid_descriptor = {
    .bLength = sizeof(ctl_functional_descriptors),
    .bDescriptorType = USB_DT_HID,
    .bcdHID = USB_HID_VERSION_DEF,
    .bCountryCode = 0,    /* Zero means not localized */
    .bNumDescriptors = 1, /* The number of descriptors */
  },
  /* The report descriptor */
  .hid_report = {
    .bReportDescriptorType = USB_DT_REPORT,                  /* The type of descriptor */
    .wDescriptorLength = sizeof(ctl_report_descriptor), /* The size of report descriptor */
  },
};

const struct usb_interface_descriptor hid_ctrl_iface = {
  .bLength = USB_DT_INTERFACE_SIZE,            /* The size of descriptor in bytes */
  .bDescriptorType = USB_DT_INTERFACE,         /* The type of descriptor */
  .bInterfaceNumber = CTL_CTRL_IF,             /* The interface serial number */
  .bAlternateSetting = 0,                      /* The value to select alternative setting */
#ifdef CTL_IN_EP
  .bNumEndpoints = 2,                          /* The number of end-points used by interface */
#else
  .bNumEndpoints = 0,                          /* The number of end-points used by interface */
#endif
  .bInterfaceClass = USB_CLASS_HID,            /* The class of interface */
  .bInterfaceSubClass = 0,                     /* Not a boot mode mouse or keyboard */
  .bInterfaceProtocol = 0,                     /* No protocol specified */
  .iInterface = 0,                             /* The intex of interface name string */
  /* end of descriptor */
#ifdef CTL_IN_EP
  .endpoint = ctrl_endp,                       /* The pointer to interface end-points */
#else
  .endpoint = NULL,                            /* The pointer to interface end-points */
#endif
  
  .extra = &ctl_functional_descriptors,
  .extralen = sizeof(ctl_functional_descriptors),
};

static int
ctl_control_request(usbd_device *usbd_dev, struct usb_setup_data *req,
                    uint8_t **buf, uint16_t *len,
                    usbd_control_complete_callback *complete) {
  (void)usbd_dev;
  (void)complete;
  
  if (req->wIndex != CTL_CTRL_IF) {
    return USBD_REQ_NEXT_CALLBACK;
  }
  
  if (USB_REQ_IN(req)) {
    switch (req->bRequest) {
    case USB_REQ_GET_DESCRIPTOR:
      switch (req->wValue) {
      case HID_REQ_VALUE(USB_DT_REPORT, 0):
        *buf = (uint8_t*)ctl_report_descriptor;
        *len = sizeof(ctl_report_descriptor);
        return USBD_REQ_HANDLED;
        /*
      case HID_REQ_VALUE(USB_DT_HID, 0):
        *buf = (uint8_t*)&ctl_functional_descriptors;
        *len = sizeof(ctl_functional_descriptors);
        return USBD_REQ_HANDLED;
        */
      }
      break;
    case HID_REQ_GET_REPORT:
      switch (req->wValue) {
      case HID_REQ_VALUE(HID_REPORT_FEATURE, HID_REPORT_REBOOT): {
        struct hid_report_reboot *report = (struct hid_report_reboot*)*buf;
        
        report->report_id = HID_REPORT_REBOOT;
        *len = sizeof(struct hid_report_reboot);
        
        return USBD_REQ_HANDLED;
      } break;
      case HID_REQ_VALUE(HID_REPORT_FEATURE, HID_REPORT_RS485_MODE): {
        struct hid_report_uart_opts *report = (struct hid_report_uart_opts*)*buf;
        
        report->report_id = HID_REPORT_RS485_MODE;
        report->uart = setting.uart;
        *len = sizeof(struct hid_report_uart_opts);
        
        return USBD_REQ_HANDLED;
      } break;
      case HID_REQ_VALUE(HID_REPORT_FEATURE, HID_REPORT_LEDS_MODE): {
        struct hid_report_leds_opts *report = (struct hid_report_leds_opts*)*buf;
        
        report->report_id = HID_REPORT_LEDS_MODE;
        report->leds = setting.leds;
        *len = sizeof(struct hid_report_leds_opts);
        
        return USBD_REQ_HANDLED;
      } break;
      }
      break;
    }
  } else { /* USB_REQ_OUT(req) */
    switch (req->bRequest) {
    case HID_REQ_SET_IDLE:
      return USBD_REQ_HANDLED;
    case HID_REQ_SET_REPORT:
      switch (req->wValue) {
      case HID_REQ_VALUE(HID_REPORT_FEATURE, HID_REPORT_REBOOT): {
        struct hid_report_reboot *report = (struct hid_report_reboot*)*buf;

        if (*len == sizeof(struct hid_report_reboot) &&
            report->report_id == HID_REPORT_REBOOT) {
          
          set_event(evt_system_reboot);
          
          return USBD_REQ_HANDLED;
        }
      } break;
      case HID_REQ_VALUE(HID_REPORT_FEATURE, HID_REPORT_RS485_MODE): {
        struct hid_report_uart_opts *report = (struct hid_report_uart_opts*)*buf;
        
        if (*len == sizeof(struct hid_report_uart_opts) &&
            report->report_id == HID_REPORT_RS485_MODE) {
          
          setting.uart = report->uart;
          setting_save();
          
          acm_set_rs485();
          
          return USBD_REQ_HANDLED;
        }
      } break;
      case HID_REQ_VALUE(HID_REPORT_FEATURE, HID_REPORT_LEDS_MODE): {
        struct hid_report_leds_opts *report = (struct hid_report_leds_opts*)*buf;
        
        if (*len == sizeof(struct hid_report_leds_opts) &&
            report->report_id == HID_REPORT_LEDS_MODE) {
          
          setting.leds = report->leds;
          setting_save();
          
          leds_set_mode();
          
          return USBD_REQ_HANDLED;
        }
      } break;
      }
      break;
    }
  }
  
  return USBD_REQ_NOTSUPP;
}

#ifdef CTL_OUT_EP
static void ctl_data_rx_cb(usbd_device *usbd_dev, uint8_t ep) {
  (void)usbd_dev;
  (void)ep;

  
}
#endif

void hid_set_config(usbd_device *usbd_dev) {
  #ifdef CONFIG_THROUGH_HID
#ifdef CTL_OUT_EP
  usbd_ep_setup(usbd_dev,
                CTL_OUT_EP,
                USB_ENDPOINT_ATTR_INTERRUPT,
                USB_HID_REQ_PACKET_SIZE,
                ctl_data_rx_cb);
#endif /* CTL_OUT_EP */
#ifdef CTL_IN_EP
  usbd_ep_setup(usbd_dev,
                CTL_IN_EP,
                USB_ENDPOINT_ATTR_INTERRUPT,
                USB_HID_RES_PACKET_SIZE,
                NULL);
#endif /* CTL_IN_EP */
  
  usbd_register_control_callback(usbd_dev,
                                 USB_REQ_TYPE_INTERFACE,
                                 USB_REQ_TYPE_RECIPIENT,
                                 ctl_control_request);
#endif /* CONFIG_THROUGH_HID */
}

#endif /* CONFIG_THROUGH_HID */
