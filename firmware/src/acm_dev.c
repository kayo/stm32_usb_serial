#include <stddef.h>

#include <libopencm3/usb/usbd.h>
#include <libopencm3/usb/cdc.h>

#include "macro.h"
#include "config.h"
#include "dbglog.h"

#include "cdc_def.h"

#include "usb_dev.h"
#include "acm_dev.h"
#include "uart_dev.h"
#include "leds_dev.h"

#include "setting.h"

#define USB_CDC_PROTOCOL_DEF _CAT2(USB_CDC_PROTOCOL_, USB_CDC_SERIAL_PROTOCOL)

#define USB_CDC_VERSION 0110 /* 1.1 */
#define USB_CDC_VERSION_DEF _CAT2(0x, USB_CDC_VERSION)

#define ACM_DATA_IF(index) ((index)*2+1)
#define ACM_DATA_IF_INDEX(id) (((id)-1)/2)
#define ACM_COMM_IF(index) ((index)*2)
#define ACM_COMM_IF_INDEX(id) ((id)/2)

#ifdef usb_acm0
#define ACM0_CONF usb_acm0
#define ACM0_DATA_IN_EP USB_ENDPOINT_ADDR_IN(_NTH0(ACM0_CONF))
#define ACM0_DATA_OUT_EP USB_ENDPOINT_ADDR_OUT(_NTH0(ACM0_CONF))
#define ACM0_DATA_IF ACM_DATA_IF(ACM0_INDEX)
#define ACM0_COMM_EP USB_ENDPOINT_ADDR_IN(_NTH1(ACM0_CONF))
#define ACM0_COMM_IF ACM_COMM_IF(ACM0_INDEX)
/* index */
#define ACM0_INDEX 0
uart_def(ACM0_INDEX);
#endif /* usb_acm0 */

#ifdef usb_acm1
#define ACM1_CONF usb_acm1
#define ACM1_DATA_IN_EP USB_ENDPOINT_ADDR_IN(_NTH0(ACM1_CONF))
#define ACM1_DATA_OUT_EP USB_ENDPOINT_ADDR_OUT(_NTH0(ACM1_CONF))
#define ACM1_DATA_IF ACM_DATA_IF(ACM1_INDEX)
#define ACM1_COMM_EP USB_ENDPOINT_ADDR_IN(_NTH1(ACM1_CONF))
#define ACM1_COMM_IF ACM_COMM_IF(ACM1_INDEX)
/* index */
#ifdef ACM0_CONF
#define ACM1_INDEX 1
#else /* ACM0_CONF */
#define ACM1_INDEX 0
#endif /* ACM0_CONF */
uart_def(ACM1_INDEX);
#endif /* usb_acm1 */

#ifdef usb_acm2
#define ACM2_CONF usb_acm2
#define ACM2_DATA_IN_EP USB_ENDPOINT_ADDR_IN(_NTH0(ACM2_CONF))
#define ACM2_DATA_OUT_EP USB_ENDPOINT_ADDR_OUT(_NTH0(ACM2_CONF))
#define ACM2_DATA_IF ACM_DATA_IF(ACM2_INDEX)
#define ACM2_COMM_EP USB_ENDPOINT_ADDR_IN(_NTH1(ACM2_CONF))
#define ACM2_COMM_IF ACM_COMM_IF(ACM2_INDEX)
/* index */
#ifdef ACM0_CONF
#ifdef ACM1_CONF
#define ACM2_INDEX 2
#else /* ACM1_CONF */
#define ACM2_INDEX 1
#endif /* ACM1_CONF */
#else /* ACM0_CONF */
#ifdef ACM1_CONF
#define ACM2_INDEX 1
#else /* ACM1_CONF */
#define ACM2_INDEX 0
#endif /* ACM1_CONF */
#endif /* ACM0_CONF */
uart_def(ACM2_INDEX);
#endif /* usb_acm2 */

static struct usb_cdc_line_coding acm_line_coding[ACM_COUNT] = {
  [0 ... ACM_COUNT - 1] = {
    UART_BAUDRATE_DEF,
    _CAT2(uart_stopbits_, UART_STOPBITS_DEF),
    _CAT2(uart_parity_, UART_PARITY_DEF),
    UART_DATABITS_DEF
  }
};

/*
 * This notification endpoint isn't implemented. According to CDC spec its
 * optional, but its absence causes a NULL pointer dereference in Linux
 * cdc_acm driver.
 */
static const struct usb_endpoint_descriptor comm_endp[] = {
#ifdef ACM0_CONF
  {
    .bLength = USB_DT_ENDPOINT_SIZE,              /* The size of descriptor in bytes */
    .bDescriptorType = USB_DT_ENDPOINT,           /* The type of descriptor */
    .bEndpointAddress = ACM0_COMM_EP,             /* The address of end-point */
    /* The value of seventh bit means data direction: 0 - output, 1 - input */
    .bmAttributes = USB_ENDPOINT_ATTR_INTERRUPT,  /* The bits of attributes */
    /* The transfer type, the type of synchronization and the usage type for isochronous */
    .wMaxPacketSize = USB_CDC_COMM_PACKET_SIZE,   /* The maximum size of packet */
    .bInterval = 255,                             /* The polling interval */
  },
#endif /* ACM0_CONF */
#ifdef ACM1_CONF
  {
    .bLength = USB_DT_ENDPOINT_SIZE,              /* The size of descriptor in bytes */
    .bDescriptorType = USB_DT_ENDPOINT,           /* The type of descriptor */
    .bEndpointAddress = ACM1_COMM_EP,             /* The address of end-point */
    /* The value of seventh bit means data direction: 0 - output, 1 - input */
    .bmAttributes = USB_ENDPOINT_ATTR_INTERRUPT,  /* The bits of attributes */
    /* The transfer type, the type of synchronization and the usage type for isochronous */
    .wMaxPacketSize = USB_CDC_COMM_PACKET_SIZE,   /* The maximum size of packet */
    .bInterval = 255,                             /* The polling interval */
  },
#endif /* ACM1_CONF */
#ifdef ACM2_CONF
  {
    .bLength = USB_DT_ENDPOINT_SIZE,              /* The size of descriptor in bytes */
    .bDescriptorType = USB_DT_ENDPOINT,           /* The type of descriptor */
    .bEndpointAddress = ACM2_COMM_EP,             /* The address of end-point */
    /* The value of seventh bit means data direction: 0 - output, 1 - input */
    .bmAttributes = USB_ENDPOINT_ATTR_INTERRUPT,  /* The bits of attributes */
    /* The transfer type, the type of synchronization and the usage type for isochronous */
    .wMaxPacketSize = USB_CDC_COMM_PACKET_SIZE,   /* The maximum size of packet */
    .bInterval = 255,                             /* The polling interval */
  },
#endif /* ACM2_CONF */
};

static const struct usb_endpoint_descriptor data_endp[] = {
#ifdef ACM0_CONF
  {
    .bLength = USB_DT_ENDPOINT_SIZE,              /* The size of descriptor in bytes */
    .bDescriptorType = USB_DT_ENDPOINT,           /* The type of descriptor */
    .bEndpointAddress = ACM0_DATA_OUT_EP,         /* The address of end-point */
    /* The value of seventh bit means data direction: 0 - output, 1 - input */
    .bmAttributes = USB_ENDPOINT_ATTR_BULK,       /* The bits of attributes */
    /* The transfer type, the type of synchronization and the usage type for isochronous */
    .wMaxPacketSize = USB_CDC_DATA_PACKET_SIZE,   /* The maximum size of packet */
    .bInterval = 0,                               /* The polling interval (ignored) */
  },
  {
    .bLength = USB_DT_ENDPOINT_SIZE,              /* The size of descriptor in bytes */
    .bDescriptorType = USB_DT_ENDPOINT,           /* The type of descriptor */
    .bEndpointAddress = ACM0_DATA_IN_EP,          /* The address of end-point */
    /* The value of seventh bit means data direction: 0 - output, 1 - input */
    .bmAttributes = USB_ENDPOINT_ATTR_BULK,       /* The bits of attributes */
    /* The transfer type, the type of synchronization and the usage type for isochronous */
    .wMaxPacketSize = USB_CDC_DATA_PACKET_SIZE,   /* The maximum size of packet */
    .bInterval = 0,                               /* The polling interval (ignored) */
  },
#endif /* ACM0_CONF */
#ifdef ACM1_CONF
  {
    .bLength = USB_DT_ENDPOINT_SIZE,              /* The size of descriptor in bytes */
    .bDescriptorType = USB_DT_ENDPOINT,           /* The type of descriptor */
    .bEndpointAddress = ACM1_DATA_OUT_EP,         /* The address of end-point */
    /* The value of seventh bit means data direction: 0 - output, 1 - input */
    .bmAttributes = USB_ENDPOINT_ATTR_BULK,       /* The bits of attributes */
    /* The transfer type, the type of synchronization and the usage type for isochronous */
    .wMaxPacketSize = USB_CDC_DATA_PACKET_SIZE,   /* The maximum size of packet */
    .bInterval = 0,                               /* The polling interval (ignored) */
  },
  {
    .bLength = USB_DT_ENDPOINT_SIZE,              /* The size of descriptor in bytes */
    .bDescriptorType = USB_DT_ENDPOINT,           /* The type of descriptor */
    .bEndpointAddress = ACM1_DATA_IN_EP,          /* The address of end-point */
    /* The value of seventh bit means data direction: 0 - output, 1 - input */
    .bmAttributes = USB_ENDPOINT_ATTR_BULK,       /* The bits of attributes */
    /* The transfer type, the type of synchronization and the usage type for isochronous */
    .wMaxPacketSize = USB_CDC_DATA_PACKET_SIZE,   /* The maximum size of packet */
    .bInterval = 0,                               /* The polling interval (ignored) */
  },
#endif /* ACM1_CONF */
#ifdef ACM2_CONF
  {
    .bLength = USB_DT_ENDPOINT_SIZE,              /* The size of descriptor in bytes */
    .bDescriptorType = USB_DT_ENDPOINT,           /* The type of descriptor */
    .bEndpointAddress = ACM2_DATA_OUT_EP,         /* The address of end-point */
    /* The value of seventh bit means data direction: 0 - output, 1 - input */
    .bmAttributes = USB_ENDPOINT_ATTR_BULK,       /* The bits of attributes */
    /* The transfer type, the type of synchronization and the usage type for isochronous */
    .wMaxPacketSize = USB_CDC_DATA_PACKET_SIZE,   /* The maximum size of packet */
    .bInterval = 0,                               /* The polling interval (ignored) */
  },
  {
    .bLength = USB_DT_ENDPOINT_SIZE,              /* The size of descriptor in bytes */
    .bDescriptorType = USB_DT_ENDPOINT,           /* The type of descriptor */
    .bEndpointAddress = ACM2_DATA_IN_EP,          /* The address of end-point */
    /* The value of seventh bit means data direction: 0 - output, 1 - input */
    .bmAttributes = USB_ENDPOINT_ATTR_BULK,       /* The bits of attributes */
    /* The transfer type, the type of synchronization and the usage type for isochronous */
    .wMaxPacketSize = USB_CDC_DATA_PACKET_SIZE,   /* The maximum size of packet */
    .bInterval = 0,                               /* The polling interval (ignored) */
  },
#endif /* ACM2_CONF */
};

static const struct {
  struct usb_cdc_header_descriptor header;
  struct usb_cdc_call_management_descriptor call_mgmt;
  struct usb_cdc_acm_descriptor acm;
  struct usb_cdc_union_descriptor cdc_union;
} __packed__ acm_functional_descriptors[] = {
#ifdef ACM0_CONF
  {
    .header = {
      .bFunctionLength = sizeof(struct usb_cdc_header_descriptor),
      .bDescriptorType = CS_INTERFACE,
      .bDescriptorSubtype = USB_CDC_TYPE_HEADER,
      .bcdCDC = USB_CDC_VERSION_DEF,
    },
    .call_mgmt = {
      .bFunctionLength = sizeof(struct usb_cdc_call_management_descriptor),
      .bDescriptorType = CS_INTERFACE,
      .bDescriptorSubtype = USB_CDC_TYPE_CALL_MANAGEMENT,
      .bmCapabilities = USB_CDC_CALL_MGMT_DOES_NOT_HANDLE,
      .bDataInterface = ACM0_DATA_IF,
    },
    .acm = {
      .bFunctionLength = sizeof(struct usb_cdc_acm_descriptor),
      .bDescriptorType = CS_INTERFACE,
      .bDescriptorSubtype = USB_CDC_TYPE_ACM,
      .bmCapabilities = USB_CDC_ACM_SUP_LINE_CODING_AND_STATE,
    },
    .cdc_union = {
      .bFunctionLength = sizeof(struct usb_cdc_union_descriptor),
      .bDescriptorType = CS_INTERFACE,
      .bDescriptorSubtype = USB_CDC_TYPE_UNION,
      .bControlInterface = ACM0_COMM_IF,
      .bSubordinateInterface0 = ACM0_DATA_IF,
    },
  },
#endif /* ACM0_CONF */
#ifdef ACM1_CONF
  {
    .header = {
      .bFunctionLength = sizeof(struct usb_cdc_header_descriptor),
      .bDescriptorType = CS_INTERFACE,
      .bDescriptorSubtype = USB_CDC_TYPE_HEADER,
      .bcdCDC = USB_CDC_VERSION_DEF,
    },
    .call_mgmt = {
      .bFunctionLength = sizeof(struct usb_cdc_call_management_descriptor),
      .bDescriptorType = CS_INTERFACE,
      .bDescriptorSubtype = USB_CDC_TYPE_CALL_MANAGEMENT,
      .bmCapabilities = USB_CDC_CALL_MGMT_DOES_NOT_HANDLE,
      .bDataInterface = ACM1_DATA_IF,
    },
    .acm = {
      .bFunctionLength = sizeof(struct usb_cdc_acm_descriptor),
      .bDescriptorType = CS_INTERFACE,
      .bDescriptorSubtype = USB_CDC_TYPE_ACM,
      .bmCapabilities = USB_CDC_ACM_SUP_LINE_CODING_AND_STATE,
    },
    .cdc_union = {
      .bFunctionLength = sizeof(struct usb_cdc_union_descriptor),
      .bDescriptorType = CS_INTERFACE,
      .bDescriptorSubtype = USB_CDC_TYPE_UNION,
      .bControlInterface = ACM1_COMM_IF,
      .bSubordinateInterface0 = ACM1_DATA_IF,
    },
  },
#endif /* ACM1_CONF */
#ifdef ACM2_CONF
  {
    .header = {
      .bFunctionLength = sizeof(struct usb_cdc_header_descriptor),
      .bDescriptorType = CS_INTERFACE,
      .bDescriptorSubtype = USB_CDC_TYPE_HEADER,
      .bcdCDC = USB_CDC_VERSION_DEF,
    },
    .call_mgmt = {
      .bFunctionLength = sizeof(struct usb_cdc_call_management_descriptor),
      .bDescriptorType = CS_INTERFACE,
      .bDescriptorSubtype = USB_CDC_TYPE_CALL_MANAGEMENT,
      .bmCapabilities = USB_CDC_CALL_MGMT_DOES_NOT_HANDLE,
      .bDataInterface = ACM2_DATA_IF,
    },
    .acm = {
      .bFunctionLength = sizeof(struct usb_cdc_acm_descriptor),
      .bDescriptorType = CS_INTERFACE,
      .bDescriptorSubtype = USB_CDC_TYPE_ACM,
      .bmCapabilities = USB_CDC_ACM_SUP_LINE_CODING_AND_STATE,
    },
    .cdc_union = {
      .bFunctionLength = sizeof(struct usb_cdc_union_descriptor),
      .bDescriptorType = CS_INTERFACE,
      .bDescriptorSubtype = USB_CDC_TYPE_UNION,
      .bControlInterface = ACM2_COMM_IF,
      .bSubordinateInterface0 = ACM2_DATA_IF,
    },
  },
#endif /* ACM2_CONF */
};

const struct usb_interface_descriptor acm_comm_iface[] = {
#ifdef ACM0_CONF
  {
    .bLength = USB_DT_INTERFACE_SIZE,            /* The size of descriptor in bytes */
    .bDescriptorType = USB_DT_INTERFACE,         /* The type of descriptor */
    .bInterfaceNumber = ACM0_COMM_IF,            /* The interface serial number */
    .bAlternateSetting = 0,                      /* The value to select alternative setting */
    .bNumEndpoints = 1,                          /* The number of end-points used by interface */
    .bInterfaceClass = USB_CLASS_CDC,            /* The class of interface */
    .bInterfaceSubClass = USB_CDC_SUBCLASS_ACM,  /* The sub class of interface */
    .bInterfaceProtocol = USB_CDC_PROTOCOL_DEF,  /* The interface protocol */
    .iInterface = 0,                             /* The intex of interface name string */
    /* end of descriptor */
    .endpoint = &comm_endp[ACM0_INDEX],          /* The pointer to interface end-point */
    
    .extra = &acm_functional_descriptors[ACM0_INDEX],
    .extralen = sizeof(acm_functional_descriptors[0]),
  },
#endif /* ACM0_CONF */
#ifdef ACM1_CONF
  {
    .bLength = USB_DT_INTERFACE_SIZE,            /* The size of descriptor in bytes */
    .bDescriptorType = USB_DT_INTERFACE,         /* The type of descriptor */
    .bInterfaceNumber = ACM1_COMM_IF,            /* The interface serial number */
    .bAlternateSetting = 0,                      /* The value to select alternative setting */
    .bNumEndpoints = 1,                          /* The number of end-points used by interface */
    .bInterfaceClass = USB_CLASS_CDC,            /* The class of interface */
    .bInterfaceSubClass = USB_CDC_SUBCLASS_ACM,  /* The sub class of interface */
    .bInterfaceProtocol = USB_CDC_PROTOCOL_DEF,  /* The interface protocol */
    .iInterface = 0,                             /* The intex of interface name string */
    /* end of descriptor */
    .endpoint = &comm_endp[ACM1_INDEX],          /* The pointer to interface end-point */
    
    .extra = &acm_functional_descriptors[ACM1_INDEX],
    .extralen = sizeof(acm_functional_descriptors[0]),
  },
#endif /* ACM1_CONF */
#ifdef ACM2_CONF
  {
    .bLength = USB_DT_INTERFACE_SIZE,            /* The size of descriptor in bytes */
    .bDescriptorType = USB_DT_INTERFACE,         /* The type of descriptor */
    .bInterfaceNumber = ACM2_COMM_IF,            /* The interface serial number */
    .bAlternateSetting = 0,                      /* The value to select alternative setting */
    .bNumEndpoints = 1,                          /* The number of end-points used by interface */
    .bInterfaceClass = USB_CLASS_CDC,            /* The class of interface */
    .bInterfaceSubClass = USB_CDC_SUBCLASS_ACM,  /* The sub class of interface */
    .bInterfaceProtocol = USB_CDC_PROTOCOL_DEF,  /* The interface protocol */
    .iInterface = 0,                             /* The intex of interface name string */
    /* end of descriptor */
    .endpoint = &comm_endp[ACM2_INDEX],          /* The pointer to interface end-point */
    
    .extra = &acm_functional_descriptors[ACM2_INDEX],
    .extralen = sizeof(acm_functional_descriptors[0]),
  },
#endif /* ACM2_CONF */
};

const struct usb_interface_descriptor acm_data_iface[] = {
#ifdef ACM0_CONF
  {
    .bLength = USB_DT_INTERFACE_SIZE,     /* The size of descriptor in bytes */
    .bDescriptorType = USB_DT_INTERFACE,  /* The type of descriptor */
    .bInterfaceNumber = ACM0_DATA_IF,     /* The interface serial number */
    .bAlternateSetting = 0,               /* The value to select alternative setting */
    .bNumEndpoints = 2,                   /* The number of end-points used by interface */
    .bInterfaceClass = USB_CLASS_DATA,    /* The class of interface */
    .bInterfaceSubClass = 0,              /* The sub class of interface */
    .bInterfaceProtocol = 0,              /* The interface protocol */
    .iInterface = 0,                      /* The intex of interface name string */
    /* end of descriptor */
    .endpoint = &data_endp[ACM0_INDEX*2], /* The pointer to interface end-point */
  },
#endif /* ACM0_CONF */
#ifdef ACM1_CONF
  {
    .bLength = USB_DT_INTERFACE_SIZE,     /* The size of descriptor in bytes */
    .bDescriptorType = USB_DT_INTERFACE,  /* The type of descriptor */
    .bInterfaceNumber = ACM1_DATA_IF,     /* The interface serial number */
    .bAlternateSetting = 0,               /* The value to select alternative setting */
    .bNumEndpoints = 2,                   /* The number of end-points used by interface */
    .bInterfaceClass = USB_CLASS_DATA,    /* The class of interface */
    .bInterfaceSubClass = 0,              /* The sub class of interface */
    .bInterfaceProtocol = 0,              /* The interface protocol */
    .iInterface = 0,                      /* The intex of interface name string */
    /* end of descriptor */
    .endpoint = &data_endp[ACM1_INDEX*2], /* The pointer to interface end-point */
  },
#endif /* ACM1_CONF */
#ifdef ACM2_CONF
  {
    .bLength = USB_DT_INTERFACE_SIZE,     /* The size of descriptor in bytes */
    .bDescriptorType = USB_DT_INTERFACE,  /* The type of descriptor */
    .bInterfaceNumber = ACM2_DATA_IF,     /* The interface serial number */
    .bAlternateSetting = 0,               /* The value to select alternative setting */
    .bNumEndpoints = 2,                   /* The number of end-points used by interface */
    .bInterfaceClass = USB_CLASS_DATA,    /* The class of interface */
    .bInterfaceSubClass = 0,              /* The sub class of interface */
    .bInterfaceProtocol = 0,              /* The interface protocol */
    .iInterface = 0,                      /* The intex of interface name string */
    /* end of descriptor */
    .endpoint = &data_endp[ACM2_INDEX*2], /* The pointer to interface end-point */
  },
#endif /* ACM2_CONF */
};

/*
  An interface association allows the device to group a set of interfaces to
  represent one logical device to be managed by one host driver.
*/
const struct usb_iface_assoc_descriptor acm_iface_assoc[] = {
#ifdef ACM0_CONF
  {
    .bLength = USB_DT_INTERFACE_ASSOCIATION_SIZE,     /* The size of descriptor in bytes */
    .bDescriptorType = USB_DT_INTERFACE_ASSOCIATION,  /* The type of descriptor */
    .bFirstInterface = ACM0_COMM_IF,                  /* The first interface that is part of this group */
    .bInterfaceCount = 2,                             /* The number of included interfaces */
    .bFunctionClass = USB_CLASS_CDC,                  /* The class of function */
    .bFunctionSubClass = USB_CDC_SUBCLASS_ACM,        /* The sub class of function */
    .bFunctionProtocol = USB_CDC_PROTOCOL_DEF,        /* The function protocol */
    .iFunction = 0,                                   /* The intex of function name string */
  },
#endif /* ACM0_CONF */
#ifdef ACM1_CONF
  {
    .bLength = USB_DT_INTERFACE_ASSOCIATION_SIZE,     /* The size of descriptor in bytes */
    .bDescriptorType = USB_DT_INTERFACE_ASSOCIATION,  /* The type of descriptor */
    .bFirstInterface = ACM1_COMM_IF,                  /* The first interface that is part of this group */
    .bInterfaceCount = 2,                             /* The number of included interfaces */
    .bFunctionClass = USB_CLASS_CDC,                  /* The class of function */
    .bFunctionSubClass = USB_CDC_SUBCLASS_ACM,        /* The sub class of function */
    .bFunctionProtocol = USB_CDC_PROTOCOL_DEF,        /* The function protocol */
    .iFunction = 0,                                   /* The intex of function name string */
  },
#endif /* ACM1_CONF */
#ifdef ACM2_CONF
  {
    .bLength = USB_DT_INTERFACE_ASSOCIATION_SIZE,     /* The size of descriptor in bytes */
    .bDescriptorType = USB_DT_INTERFACE_ASSOCIATION,  /* The type of descriptor */
    .bFirstInterface = ACM2_COMM_IF,                  /* The first interface that is part of this group */
    .bInterfaceCount = 2,                             /* The number of included interfaces */
    .bFunctionClass = USB_CLASS_CDC,                  /* The class of function */
    .bFunctionSubClass = USB_CDC_SUBCLASS_ACM,        /* The sub class of function */
    .bFunctionProtocol = USB_CDC_PROTOCOL_DEF,        /* The function protocol */
    .iFunction = 0,                                   /* The intex of function name string */
  },
#endif /* ACM2_CONF */
};

static uint8_t tx_state = 0;

static int
acm_control_request(usbd_device *usbd_dev, struct usb_setup_data *req,
                    uint8_t **buf, uint16_t *len,
                    usbd_control_complete_callback *complete) {
  (void)usbd_dev;
  (void)complete;
  
  switch (req->wIndex) {
#ifdef ACM0_CONF
  case ACM0_COMM_IF:
#endif /* ACM0_CONF */
#ifdef ACM1_CONF
  case ACM1_COMM_IF:
#endif /* ACM1_CONF */
#ifdef ACM2_CONF
  case ACM2_COMM_IF:
#endif /* ACM2_CONF */
    break;
  default:
    return USBD_REQ_NEXT_CALLBACK;
  }
  
  if (USB_REQ_IN(req)) {
    switch (req->bRequest) {
    case USB_CDC_REQ_GET_LINE_CODING: {
      struct usb_cdc_line_coding *line_coding =
        (struct usb_cdc_line_coding*)*buf;
      uint8_t id = ACM_COMM_IF_INDEX(req->wIndex);

      if (id >= ACM_COUNT) {
        return USBD_REQ_NOTSUPP;
      }
      
      *line_coding = acm_line_coding[id];
      *len = sizeof(*line_coding);
    } return USBD_REQ_HANDLED;
    } /* switch (req->bRequest) */
  } else { /* if (USB_REQ_OUT(req)) */
    switch (req->bRequest) {
    case USB_CDC_REQ_SET_CONTROL_LINE_STATE: {
      if (req->wValue & USB_CDC_LINE_STATE_ACTIVATE_CARRIER) {
        /* Terminal attached */
        void (*uart_on)(void);
        
        switch (req->wIndex) {
#ifdef ACM0_CONF
        case ACM0_COMM_IF:
          uart_on = uart_nm(ACM0_INDEX, on);
          break;
#endif /* ACM0_CONF */
#ifdef ACM1_CONF
        case ACM1_COMM_IF:
          uart_on = uart_nm(ACM1_INDEX, on);
          break;
#endif /* ACM1_CONF */
#ifdef ACM2_CONF
        case ACM2_COMM_IF:
          uart_on = uart_nm(ACM2_INDEX, on);
          break;
#endif /* ACM2_CONF */
        default:
          return USBD_REQ_NOTSUPP;
        }
        /* initialize uart */
        uart_on();
        /* set line coding */
        const struct usb_cdc_line_coding *line_coding =
          &acm_line_coding[req->wIndex];
        
        uart_fn(ACM0_INDEX, conf,
                line_coding->dwDTERate,
                line_coding->bDataBits,
                line_coding->bCharFormat,
                line_coding->bParityType);
      } else {
        /* Terminal detached */
        void (*uart_off)(void);
        
        switch (req->wIndex) {
#ifdef ACM0_CONF
        case ACM0_COMM_IF:
          uart_off = uart_nm(ACM0_INDEX, off);
          break;
#endif /* ACM0_CONF */
#ifdef ACM1_CONF
        case ACM1_COMM_IF:
          uart_off = uart_nm(ACM1_INDEX, off);
          break;
#endif /* ACM1_CONF */
#ifdef ACM2_CONF
        case ACM2_COMM_IF:
          uart_off = uart_nm(ACM2_INDEX, off);
          break;
#endif /* ACM2_CONF */
        default:
          return USBD_REQ_NOTSUPP;
        }
        /* finalize uart */
        uart_off();
        tx_state = 0;
      }
      
#if USB_CDC_SERIAL_STATE_NOTIFICATION
      /*
       * This Linux cdc_acm driver requires this to be implemented
       * even though it's optional in the CDC spec, and we don't
       * advertise it in the ACM functional descriptor.
       */
      struct usb_cdc_serial_state_notification notif;
      
      /* We echo signals back to host as notification. */
      notif.req.bmRequestType = USB_REQ_TYPE_IN | USB_REQ_TYPE_CLASS | USB_REQ_TYPE_INTERFACE;
      notif.req.bNotification = USB_CDC_NOTIFY_SERIAL_STATE;
      notif.req.wValue = 0;
      notif.req.wIndex = req->wIndex;
      notif.req.wLength = sizeof(notif.data);
      notif.data.bmSerialState = req->wValue & 0x3;
      
      uint8_t comm_ep;
      
      switch (req->wIndex) {
#ifdef ACM0_CONF
      case ACM0_COMM_IF:
        comm_ep = ACM0_COMM_EP;
        break;
#endif /* ACM0_CONF */
#ifdef ACM1_CONF
      case ACM1_COMM_IF:
        comm_ep = ACM1_COMM_EP;
        break;
#endif /* ACM1_CONF */
#ifdef ACM2_CONF
      case ACM2_COMM_IF:
        comm_ep = ACM2_COMM_EP;
        break;
#endif /* ACM2_CONF */
      default:
        return USBD_REQ_NOTSUPP;
      }
      
      usbd_ep_write_packet(usbd_dev, comm_ep, &notif, sizeof(notif));
#endif /* USB_CDC_SERIAL_STATE_NOTIFICATION */
    } return USBD_REQ_HANDLED;
    case USB_CDC_REQ_SET_LINE_CODING: {
      const struct usb_cdc_line_coding *line_coding =
        (const struct usb_cdc_line_coding*)*buf;
      
      if (*len < sizeof(*line_coding)) {
        return USBD_REQ_NOTSUPP;
      }
      
      void (*uart_conf)(uint32_t baudrate, uint8_t databits, uint8_t stopbits, uint8_t parity);
      
      switch (req->wIndex) {
#ifdef ACM0_CONF
      case ACM0_COMM_IF:
        uart_conf = uart_nm(ACM0_INDEX, conf);
        break;
#endif /* ACM0_CONF */
#ifdef ACM1_CONF
      case ACM1_COMM_IF:
        uart_conf = uart_nm(ACM1_INDEX, conf);
        break;
#endif /* ACM1_CONF */
#ifdef ACM2_CONF
      case ACM2_COMM_IF:
        uart_conf = uart_nm(ACM2_INDEX, conf);
        break;
#endif /* ACM2_CONF */
      default:
        return USBD_REQ_NOTSUPP;
      }
      
      dbglog(info, "uart conf");

      uint8_t id = ACM_COMM_IF_INDEX(req->wIndex);
      acm_line_coding[id] = *line_coding;
      
      uart_conf(line_coding->dwDTERate,
                line_coding->bDataBits,
                line_coding->bCharFormat,
                line_coding->bParityType);
    } return USBD_REQ_HANDLED;
    } /* switch (req->bRequest) */
  }
  
  return USBD_REQ_NOTSUPP;
}

static void acm_data_rx_cb(usbd_device *usbd_dev, uint8_t ep) {
  bool (*uart_tx_request)(uart_idx_t *idx, void **ptr);
  void (*uart_tx_confirm)(uart_idx_t idx, uart_len_t len);
  
  switch (ep) {
#ifdef ACM0_CONF
  case ACM0_DATA_OUT_EP:
    uart_tx_request = uart_nm(ACM0_INDEX, tx_request);
    uart_tx_confirm = uart_nm(ACM0_INDEX, tx_confirm);
    break;
#endif
#ifdef ACM1_CONF
  case ACM1_DATA_OUT_EP:
    uart_tx_request = uart_nm(ACM1_INDEX, tx_request);
    uart_tx_confirm = uart_nm(ACM1_INDEX, tx_confirm);
    break;
#endif
#ifdef ACM2_CONF
  case ACM2_DATA_OUT_EP:
    uart_tx_request = uart_nm(ACM2_INDEX, tx_request);
    uart_tx_confirm = uart_nm(ACM2_INDEX, tx_confirm);
    break;
#endif
  default:
    return;
  }

  leds_onoff(transfer);
  leds_onoff(transmission);
  
  void *buf;
  uart_idx_t idx; 
  
  if (!uart_tx_request(&idx, &buf)) {
    dbglog(info, "set nak");
    usbd_ep_nak_set(usbd_dev, ep, 1);
    
    return;
  }
  
  uint16_t len = usbd_ep_read_packet(usbd_dev, ep, buf, USB_CDC_DATA_PACKET_SIZE);
  
  if (len > 0) {
    dbglog(info, "uart send %u", len);
  }
  
  uart_tx_confirm(idx, len);
  
  //usbd_ep_nak_set(usbd_dev, ep, 0);
}

static void acm_state_tx(usbd_device *usbd_dev, uint8_t ep) {
  uint8_t (*uart_status)(void);
  uint8_t comm_if;

  switch (ep) {
#ifdef ACM0_CONF
  case ACM0_COMM_EP:
    uart_status = uart_nm(ACM0_INDEX, status);
    comm_if = ACM0_COMM_IF;
    break;
#endif
#ifdef ACM1_CONF
  case ACM1_COMM_EP:
    uart_status = uart_nm(ACM1_INDEX, status);
    comm_if = ACM1_COMM_IF;
    break;
#endif
#ifdef ACM2_CONF
  case ACM2_COMM_EP:
    uart_status = uart_nm(ACM2_INDEX, status);
    comm_if = ACM2_COMM_IF;
    break;
#endif
  default:
    return;
  }
  
  uint8_t status = uart_status();
  
  if (status & uart_state_mask) {
#if USB_CDC_SERIAL_STATE_NOTIFICATION
    /*
     * This Linux cdc_acm driver requires this to be implemented
     * even though it's optional in the CDC spec, and we don't
     * advertise it in the ACM functional descriptor.
     */
    struct usb_cdc_serial_state_notification notif;
    
    /* We echo signals back to host as notification. */
    notif.req.bmRequestType = USB_REQ_TYPE_IN | USB_REQ_TYPE_CLASS | USB_REQ_TYPE_INTERFACE;
    notif.req.bNotification = USB_CDC_NOTIFY_SERIAL_STATE;
    notif.req.wValue = 0;
    notif.req.wIndex = comm_if;
    notif.req.wLength = sizeof(notif.data);
    notif.data.bmSerialState = status;
    
    usbd_ep_write_packet(usbd_dev, ep, &notif, sizeof(notif));
#endif /* USB_CDC_SERIAL_STATE_NOTIFICATION */
  }
}

static void acm_data_tx_cb(usbd_device *usbd_dev, uint8_t ep) {
  uint8_t active_flag;
  uint8_t needzlp_flag;
  bool (*uart_rx_request)(uart_idx_t *idx, const void **ptr, uart_len_t *len);
  void (*uart_rx_confirm)(uart_idx_t idx);
  
  switch (ep) {
#ifdef ACM0_CONF
  case ACM0_DATA_OUT_EP:
  case ACM0_DATA_IN_EP:
    ep = ACM0_DATA_IN_EP;
    active_flag = 1 << ACM0_INDEX;
    uart_rx_request = uart_nm(ACM0_INDEX, rx_request);
    uart_rx_confirm = uart_nm(ACM0_INDEX, rx_confirm);
    break;
#endif
#ifdef ACM1_CONF
  case ACM1_DATA_OUT_EP:
  case ACM1_DATA_IN_EP:
    ep = ACM1_DATA_IN_EP;
    active_flag = 1 << ACM1_INDEX;
    uart_rx_request = uart_nm(ACM1_INDEX, rx_request);
    uart_rx_confirm = uart_nm(ACM1_INDEX, rx_confirm);
    break;
#endif
#ifdef ACM2_CONF
  case ACM2_DATA_OUT_EP:
  case ACM2_DATA_IN_EP:
    ep = ACM1_DATA_IN_EP;
    active_flag = 1 << ACM2_INDEX;
    uart_rx_request = uart_nm(ACM2_INDEX, rx_request);
    uart_rx_confirm = uart_nm(ACM2_INDEX, rx_confirm);
    break;
#endif
  default:
    return;
  }

  needzlp_flag = active_flag << 4;
  
  uart_len_t len;
  const void *buf;
  uart_idx_t idx;
  
  if (!uart_rx_request(&idx, &buf, &len)) {
    if (tx_state & needzlp_flag) { /* send ZLP */
      tx_state &= ~needzlp_flag;
      usbd_ep_write_packet(usbd_dev, ep, buf, 0);
    } else {
      tx_state &= ~active_flag;
    }
    return;
  }

  tx_state |= active_flag;
  
  leds_onoff(transfer);
  leds_onoff(reception);
  
  if (len == usbd_ep_write_packet(usbd_dev, ep, buf, len)) {
    dbglog(info, "acm sent %u", len);
    uart_rx_confirm(idx);
    if (len == USB_CDC_DATA_PACKET_SIZE) {
      tx_state |= needzlp_flag; /* need ZLP after last packet */
    } else {
      tx_state &= ~needzlp_flag; /* no need ZLP after last packet */
    }
  } else {
    dbglog(info, "acm send error");
  }
}

static void acm_data_tx(usbd_device *usbd_dev, uint8_t ep, uint8_t active_flag) {
  if (tx_state & active_flag) {
    return;
  }
  
  acm_data_tx_cb(usbd_dev, ep);
}

void acm_set_config(usbd_device *usbd_dev) {
#ifdef ACM0_CONF
  usbd_ep_setup(usbd_dev,
                ACM0_DATA_OUT_EP,
                USB_ENDPOINT_ATTR_BULK,
                USB_CDC_DATA_PACKET_SIZE,
                acm_data_rx_cb);
  usbd_ep_setup(usbd_dev,
                ACM0_DATA_IN_EP,
                USB_ENDPOINT_ATTR_BULK,
                USB_CDC_DATA_PACKET_SIZE,
                acm_data_tx_cb);
  usbd_ep_setup(usbd_dev,
                ACM0_COMM_EP,
                USB_ENDPOINT_ATTR_INTERRUPT,
                USB_CDC_COMM_PACKET_SIZE,
                NULL);
#endif /* ACM0_CONF */
#ifdef ACM1_CONF
  usbd_ep_setup(usbd_dev,
                ACM1_DATA_OUT_EP,
                USB_ENDPOINT_ATTR_BULK,
                USB_CDC_DATA_PACKET_SIZE,
                acm_data_rx_cb);
  usbd_ep_setup(usbd_dev,
                ACM1_DATA_IN_EP,
                USB_ENDPOINT_ATTR_BULK,
                USB_CDC_DATA_PACKET_SIZE,
                acm_data_tx_cb);
  usbd_ep_setup(usbd_dev,
                ACM1_COMM_EP,
                USB_ENDPOINT_ATTR_INTERRUPT,
                USB_CDC_COMM_PACKET_SIZE,
                NULL);
#endif /* ACM1_CONF */
#ifdef ACM2_CONF
  usbd_ep_setup(usbd_dev,
                ACM2_DATA_OUT_EP,
                USB_ENDPOINT_ATTR_BULK,
                USB_CDC_DATA_PACKET_SIZE,
                acm_data_rx_cb);
  usbd_ep_setup(usbd_dev,
                ACM2_DATA_IN_EP,
                USB_ENDPOINT_ATTR_BULK,
                USB_CDC_DATA_PACKET_SIZE,
                acm_data_tx_cb);
  usbd_ep_setup(usbd_dev,
                ACM2_COMM_EP,
                USB_ENDPOINT_ATTR_INTERRUPT,
                USB_CDC_COMM_PACKET_SIZE,
                NULL);
#endif /* ACM2_CONF */

#if defined(ACM0_CONF) || defined(ACM1_CONF) || defined(ACM2_CONF)
  usbd_register_control_callback(usbd_dev,
                                 USB_REQ_TYPE_CLASS | USB_REQ_TYPE_INTERFACE,
                                 USB_REQ_TYPE_TYPE | USB_REQ_TYPE_RECIPIENT,
                                 acm_control_request);
#endif /* defined(ACM0_CONF) || defined(ACM1_CONF) || defined(ACM2_CONF) */
}

void acm_set_rs485(void) {
#ifdef ACM0_CONF
  uart_fn(ACM0_INDEX, set_rs485, rs485_mode_get(setting.uart.rs485, ACM0_INDEX));
#endif
#ifdef ACM1_CONF
  uart_fn(ACM1_INDEX, set_rs485, rs485_mode_get(setting.uart.rs485, ACM1_INDEX));
#endif
#ifdef ACM2_CONF
  uart_fn(ACM2_INDEX, set_rs485, rs485_mode_get(setting.uart.rs485, ACM2_INDEX));
#endif
}

void acm_dev_init(void) {
#ifdef ACM0_CONF
  uart_fn(ACM0_INDEX, init);
#endif
#ifdef ACM1_CONF
  uart_fn(ACM1_INDEX, init);
#endif
#ifdef ACM2_CONF
  uart_fn(ACM2_INDEX, init);
#endif
  
  acm_set_rs485();
}

void acm_dev_done(void) {
#ifdef ACM0_CONF
  uart_fn(ACM0_INDEX, done);
#endif
#ifdef ACM1_CONF
  uart_fn(ACM1_INDEX, done);
#endif
#ifdef ACM2_CONF
  uart_fn(ACM2_INDEX, done);
#endif
}

#ifdef ACM0_CONF
void uart_fn(ACM0_INDEX, rx_handle, void) {
  acm_data_tx(usb_dev, ACM0_DATA_IN_EP, 1 << ACM0_INDEX);
}
void uart_fn(ACM0_INDEX, st_handle, void) {
  acm_state_tx(usb_dev, ACM0_COMM_EP);
}
#endif
#ifdef ACM1_CONF
void uart_fn(ACM1_INDEX, rx_handle, void) {
  acm_data_tx(usb_dev, ACM1_DATA_IN_EP, 1 << ACM1_INDEX);
}
void uart_fn(ACM1_INDEX, st_handle, void) {
  acm_state_tx(usb_dev, ACM1_COMM_EP);
}
#endif
#ifdef ACM2_CONF
void uart_fn(ACM2_INDEX, rx_handle, void) {
  acm_data_tx(usb_dev, ACM2_DATA_IN_EP, 1 << ACM2_INDEX);
}
void uart_fn(ACM2_INDEX, st_handle, void) {
  acm_state_tx(usb_dev, ACM2_COMM_EP);
}
#endif

#if DBGLOG_MATCH(err)
void acm_log_poll(void) {
  ringbuf_size_t len;
  const void *ptr = ringbuf_read(&dbglog_buffer, &len);
  
  if (ptr != NULL) {
    if (len > USB_CDC_DATA_PACKET_SIZE) {
      len = USB_CDC_DATA_PACKET_SIZE;
    }
    if (0 != usbd_ep_write_packet(usb_dev, ACM2_DATA_IN_EP, ptr, len)) {
      ringbuf_seek(&dbglog_buffer, len);
    }
  }
}
#endif
