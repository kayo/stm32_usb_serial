#include "io.h"

/* I/O chunks queue */
fifo_d(io);

/* I/O chunks pool */
pool_d(io);

/* I/O pool instance */
/* + 1 byte for size */
pool_inst(io, io_pool, IO_PACKET_SIZE + 1, IO_QUEUE_LENGTH);

void io_reset(fifo_io_t *fifo) {
  for (; io_fill(fifo) > 0; ) {
    io_idx_t last;
    
    fifo_io_shift(fifo, &last);
    pool_io_free(&io_pool, last);
  }
}

bool io_put_req(fifo_io_t *fifo, io_idx_t *index, void **data, io_len_t *size) {
  if (0 == fifo_io_free(fifo)) {
    /* we have no free space in the queue */
    return false;
  }
  
  if (!pool_io_alloc(&io_pool, index)) {
    /* we have no free space in the pool */
    return false;
  }
  
  io_val_t *ptr = pool_io_pointer(&io_pool, *index);

  ptr[0] = 0;
  *size = IO_PACKET_SIZE;
  *data = &ptr[1];
  
  return true;
}

void io_put_end(fifo_io_t *fifo, io_idx_t index, io_len_t size) {
  if (size > 0) {
    io_val_t *ptr = pool_io_pointer(&io_pool, index);
    ptr[0] += size;
    fifo_io_push(fifo, index);
  } else {
    pool_io_free(&io_pool, index);
  }
}

bool io_get_req(fifo_io_t *fifo, io_idx_t *index, const void **data, io_len_t *size) {
  if (!fifo_io_shift(fifo, index)) {
    /* we have no data in the queue */
    return false;
  }
  
  const io_val_t *ptr = pool_io_pointer(&io_pool, *index);
  
  *size = ptr[0];
  *data = &ptr[1];
  
  return true;
}

void io_get_end(fifo_io_t *fifo, io_idx_t index) {
  (void)fifo;
  pool_io_free(&io_pool, index);
}
