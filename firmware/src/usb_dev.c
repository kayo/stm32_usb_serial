#include <stddef.h>

#include <libopencm3/stm32/desig.h>
#include <libopencm3/stm32/rcc.h>
#include <libopencm3/stm32/gpio.h>

#if USB_POLL_IN_ISR
#include <libopencm3/cm3/nvic.h>
#endif

#include "helper.h"
#include "macro.h"
#include "config.h"
#include "dbglog.h"

#include "usb_def.h"

#include "usb_dev.h"
#include "acm_dev.h"
#include "hid_dev.h"
#include "leds_dev.h"

#include "delay.h"

#ifdef usb_lines
#define LINES_CONF usb_lines
#define MINUS_CONF _UNWR(_NTH0(LINES_CONF))
#define MINUS_PORT _CAT2(GPIO, _NTH0(MINUS_CONF))
#define MINUS_PAD _CAT2(GPIO, _NTH1(MINUS_CONF))
#define PLUS_CONF _UNWR(_NTH1(LINES_CONF))
#define PLUS_PORT _CAT2(GPIO, _NTH0(PLUS_CONF))
#define PLUS_PAD _CAT2(GPIO, _NTH1(PLUS_CONF))
#endif

#ifdef usb_detect
#define DETECT_CONF usb_detect
#define DETECT_PORT _CAT2(GPIO, _NTH0(DETECT_CONF))
#define DETECT_PAD _CAT2(GPIO, _NTH1(DETECT_CONF))
#define DETECT_LEVEL _NTH2(DETECT_CONF)
#endif

#ifdef usb_connect
#define CONNECT_CONF usb_connect
#define CONNECT_PORT _CAT2(GPIO, _NTH0(CONNECT_CONF))
#define CONNECT_PAD _CAT2(GPIO, _NTH1(CONNECT_CONF))
#define CONNECT_LEVEL _NTH2(CONNECT_CONF)
#endif

#define USB_VERSION 0200 /* 2.0 */
#define USB_VERSION_DEF _CAT2(0x, USB_VERSION)

#ifdef STM32F1
#define USB_WAKEUP_IRQ NVIC_USB_WAKEUP_IRQ
#define USB_HIGH_PRI_IRQ NVIC_USB_HP_CAN_TX_IRQ
#define USB_LOW_PRI_IRQ NVIC_USB_LP_CAN_RX0_IRQ
#define usb_high_pri_isr usb_hp_can_tx_isr
#define usb_low_pri_isr usb_lp_can_rx0_isr
#endif /* STM32F1 */

static const struct usb_device_descriptor dev = {
  .bLength = USB_DT_DEVICE_SIZE,             /* The size of descriptor in bytes */
  .bDescriptorType = USB_DT_DEVICE,          /* The type of descriptor */
  .bcdUSB = USB_VERSION_DEF,                 /* The USB version number (USB 2) */
  .bDeviceClass = USB_CLASS_MISC,            /* The class of device */
  /* The zero means that each interface determine its own class */
  .bDeviceSubClass = USB_SUB_CLASS_COMMON,   /* The sub class of device */
  .bDeviceProtocol = USB_PROT_IFACE_ASSOC,   /* The protocol code */
  .bMaxPacketSize0 = USB_CTRL_PACKET_SIZE,   /* The maximum size of the packet for zero end-point */
  .idVendor = _CAT2(0x, USB_VENDOR_ID),      /* The vendor ID */
  .idProduct = _CAT2(0x, USB_PRODUCT_ID),    /* The product ID */
  .bcdDevice = 0x0100,                       /* The device version number */
  .iManufacturer = 1,                        /* The index of vendor name string */
  .iProduct = 2,                             /* The index of product name string */
  .iSerialNumber = 3,                        /* The index of serial number string */
  /* The zero index means no string for this field */
  .bNumConfigurations = 1,                   /* The number of available configurations */
};

static const struct usb_interface ifaces[] = {
  acm_ifaces
  hid_ifaces
};

static const struct usb_config_descriptor config = {
  .bLength = USB_DT_CONFIGURATION_SIZE,    /* The size of descriptor in bytes */
  .bDescriptorType = USB_DT_CONFIGURATION, /* The type of descriptor */
  .wTotalLength = 0,                       /* The total length of data in bytes */
  .bNumInterfaces = countof(ifaces),       /* The number of interfaces */
  .bConfigurationValue = 1,                /* The configuration id which is used to select it */
  .iConfiguration = 0,                     /* The index of configuration name string */
  .bmAttributes = USB_CONFIG_ATTR_DEFAULT, /* The bits of configuration attributes */
  /*
    7 - reserved (1)
    6 - self powered
    5 - remote wakeup
    4 .. 0 - reserved (0)
   */
  .bMaxPower = ((USB_MAX_POWER)+1)/2,      /* The maximum bus power (2mA per unit) */
  /* end of descriptor */
  .interface = ifaces,                     /* The pointer to interfaces */
};

static char serial_number[25];

static const char *usb_strings[] = {
  USB_VENDOR_STR,
  USB_PRODUCT_STR,
  serial_number,
};

static int
usb_control_request(usbd_device *usbd_dev, struct usb_setup_data *req,
                    uint8_t **buf, uint16_t *len,
                    usbd_control_complete_callback *complete) {
  (void)usbd_dev;
  (void)req;
  (void)buf;
  (void)len;
  (void)complete;
  
  leds_onoff(control);
  
  return USBD_REQ_NEXT_CALLBACK;
}

static void usb_set_config(usbd_device *usbd_dev, uint16_t wValue) {
  (void)wValue;
  
  usbd_register_control_callback(usbd_dev,
                                 USB_REQ_TYPE_CLASS | USB_REQ_TYPE_INTERFACE,
                                 USB_REQ_TYPE_TYPE | USB_REQ_TYPE_RECIPIENT,
                                 usb_control_request);
  
  acm_set_config(usbd_dev);
  hid_set_config(usbd_dev);
}

static inline void usb_dev_connect(void) {
#ifdef CONNECT_CONF
  gpio_set_mode(CONNECT_PORT, GPIO_MODE_OUTPUT_2_MHZ,
                GPIO_CNF_OUTPUT_PUSHPULL, CONNECT_PAD);
#endif
}

static inline void usb_dev_disconnect(void) {
#ifdef CONNECT_CONF
  gpio_set_mode(CONNECT_PORT, GPIO_MODE_INPUT,
                GPIO_CNF_INPUT_FLOAT, CONNECT_PAD);
#endif
}

/* USB device handle */
usbd_device *usb_dev;
 
/* Buffer to be used for control requests */
static uint8_t usb_control_buffer[USB_CTRL_BUFFER_SIZE];

#if MCU_FREQUENCY_MHZ == 48
#define USB_PRE RCC_CFGR_USBPRE_PLL_CLK_NODIV
#elif MCU_FREQUENCY_MHZ == 72
#define USB_PRE RCC_CFGR_USBPRE_PLL_CLK_DIV1_5
#else
#error "You must set mcu.frequency to 48 or 72 MHz for correct USB operation!"
#endif

void usb_dev_init(void) {
  desig_get_unique_id_as_string(serial_number, sizeof(serial_number));
  
  acm_dev_init();
  
  //rcc_periph_clock_enable(RCC_USB);
  
  rcc_set_usbpre(USB_PRE);

#ifdef DETECT_CONF
  gpio_set_mode(DETECT_PORT, GPIO_MODE_INPUT,
                GPIO_CNF_INPUT_FLOAT, DETECT_PAD);
#endif

#ifdef CONNECT_CONF
#if CONNECT_LEVEL
  gpio_set(CONNECT_PORT, CONNECT_PAD);
#else
  gpio_clear(CONNECT_PORT, CONNECT_PAD);
#endif
#endif

#if USB_POLL_IN_ISR
#if USB_SUSPEND_SUPPORT
  nvic_enable_irq(USB_WAKEUP_IRQ);
#endif
  nvic_enable_irq(USB_LOW_PRI_IRQ);
  //nvic_enable_irq(USB_HIGH_PRI_IRQ);
#endif
  
#ifdef LINES_CONF
#if MINUS_PORT == PLUS_PORT
  gpio_set_mode(MINUS_PORT, GPIO_MODE_OUTPUT_2_MHZ,
                GPIO_CNF_OUTPUT_PUSHPULL, MINUS_PAD | PLUS_PAD);
  gpio_clear(MINUS_PORT, MINUS_PAD | PLUS_PAD);
#else
  gpio_set_mode(MINUS_PORT, GPIO_MODE_OUTPUT_2_MHZ,
                GPIO_CNF_OUTPUT_PUSHPULL, MINUS_PAD);
  gpio_set_mode(PLUS_PORT, GPIO_MODE_OUTPUT_2_MHZ,
                GPIO_CNF_OUTPUT_PUSHPULL, PLUS_PAD);
  gpio_clear(MINUS_PORT, MINUS_PAD);
  gpio_clear(PLUS_PORT, PLUS_PAD);
#endif
#endif
  
  usb_dev_disconnect();
  
  delay_ms(200);
  
  usb_dev = usbd_init(&st_usbfs_v1_usb_driver,
                      &dev,
                      &config,
                      usb_strings,
                      sizeof(usb_strings)/sizeof(usb_strings[0]),
                      usb_control_buffer,
                      sizeof(usb_control_buffer));
  
  usbd_register_set_config_callback(usb_dev, usb_set_config);
  
  usb_dev_connect();

#if !USB_POLL_IN_ISR
  for (;;) usbd_poll(usb_dev);
#endif
}

void usb_dev_done(void) {
  usb_dev_disconnect();

#if USB_POLL_IN_ISR
#if USB_SUSPEND_SUPPORT
  nvic_disable_irq(USB_WAKEUP_IRQ);
#endif
  nvic_disable_irq(USB_LOW_PRI_IRQ);
  //nvic_disable_irq(USB_HIGH_PRI_IRQ);
#endif
  
  rcc_periph_clock_disable(RCC_USB);
  
  acm_dev_done();
}

#if USB_POLL_IN_ISR

#if USB_SUSPEND_SUPPORT
void usb_wakeup_isr(void) {
  usbd_poll(usb_dev);
}
#endif

void usb_low_pri_isr(void) {
  usbd_poll(usb_dev);
}

/*
void usb_high_pri_isr(void) {
  usbd_poll(usb_dev);
}
*/
#endif
