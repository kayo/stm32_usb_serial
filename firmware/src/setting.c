#include "setting.h"
#include "hid_proto.h"
#include "leds_dev.h"
#include "persist.h"

setting_t setting;

void setting_load(void) {
  persist_load(&setting);
}

void setting_save(void) {
  persist_save(&setting);
}
