#include <libopencm3/stm32/rcc.h>
#include <libopencm3/stm32/gpio.h>
#include <libopencm3/cm3/nvic.h>

#include <stddef.h>
#include <stdbool.h>

#include "systick.h"
#include "monitor.h"
#include "memtool.h"
#include "reset.h"
#include "ticks.h"
#include "delay.h"

#include "dbglog.h"
#include "leds_dev.h"
#include "usb_dev.h"
#include "uart_dev.h"
#include "acm_dev.h"
#include "events.h"
#include "setting.h"

#include "macro.h"
#include "config.h"

events_t events;

static void init(void) {
  /* Setup MCU clock */
  _CAT3(rcc_clock_setup_in_hse_8mhz_out_, MCU_FREQUENCY_MHZ, mhz)();
  
  /* Enable GPIO clocks */
  rcc_periph_clock_enable(RCC_GPIOA);
  rcc_periph_clock_enable(RCC_GPIOB);
  rcc_periph_clock_enable(RCC_GPIOC);

  /* Enable AFIO clock */
  rcc_periph_clock_enable(RCC_AFIO);

  /* Enable DMA1 clock */
  rcc_periph_clock_enable(RCC_DMA1);
  
  /* Disable JTAG only SWD, USE PD01 as GPIO */
  gpio_primary_remap(AFIO_MAPR_SWJ_CFG_JTAG_OFF_SW_ON, AFIO_MAPR_PD01_REMAP);
}

#if 1
static void done(void) {
  /* Deconfigure all GPIOs */
  gpio_set_mode(GPIOA, GPIO_MODE_INPUT,
                GPIO_CNF_INPUT_FLOAT, GPIO_ALL);
  gpio_set_mode(GPIOB, GPIO_MODE_INPUT,
                GPIO_CNF_INPUT_FLOAT, GPIO_ALL);
  gpio_set_mode(GPIOC, GPIO_MODE_INPUT,
                GPIO_CNF_INPUT_FLOAT, GPIO_ALL);
  
  /* Disable DMA1 clock */
  rcc_periph_clock_disable(RCC_DMA1);
  
  /* Disable AFIO clock */
  rcc_periph_clock_disable(RCC_AFIO);
  
  /* Disable GPIO clocks */
  rcc_periph_clock_disable(RCC_GPIOA);
  rcc_periph_clock_disable(RCC_GPIOB);
  rcc_periph_clock_disable(RCC_GPIOC);
}
#else
#define done()
#endif

/* main control */

memtool_infodef(mem_info);
monitor_def();

int main(void) {
  memtool_prefill();
  
  init();
  
  systick_init();
  monitor_init();
  events_init();
  delay_init();
  
  setting_load();
  
  leds_init();
  leds_set_mode();
  usb_dev_init();
  
  leds_on(connection);
  
  for (; !has_event(evt_system_reboot);
       ticks_tick(SYSTICK_PERIOD_MS)) {
    monitor_wait();
    
    acm_log_poll();
    leds_step();
    
    memtool_measure(&mem_info);
  }
  
  leds_off(connection);

  usb_dev_done();
  leds_done();
  
  done();
  
  do_system_reset();
  
  return 0;
}
