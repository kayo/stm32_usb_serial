#include <stddef.h>

#include <libopencm3/stm32/rcc.h>
#include <libopencm3/stm32/gpio.h>
#include <libopencm3/stm32/usart.h>
#include <libopencm3/stm32/dma.h>
#include <libopencm3/stm32/crc.h>
#include <libopencm3/cm3/nvic.h>

#include "macro.h"
#include "config.h"

#include "helper.h"
#include "uart_dev.h"

#ifndef id
#define id
#endif

uart_def(id);

#define UART_CONF _CAT3(uart, id, _dev)
#define UART_NUM _NTH0(UART_CONF)
#define UART_RCC _CAT2(RCC_USART, UART_NUM)
#define UART_DEV _CAT2(USART, UART_NUM)
#define UART_IRQ _CAT3(NVIC_USART, UART_NUM, _IRQ)
#define uart_isr _CAT3(usart, UART_NUM, _isr)

#define RX_CONF _UNWR(_NTH1(UART_CONF))
#define RX_PORT _CAT2(GPIO, _NTH0(RX_CONF))
#define RX_PAD _CAT2(GPIO, _NTH1(RX_CONF))
#define RX_AF _CAT2(GPIO_AF, _NTH2(RX_CONF))

#define TX_CONF _UNWR(_NTH2(UART_CONF))
#define TX_PORT _CAT2(GPIO, _NTH0(TX_CONF))
#define TX_PAD _CAT2(GPIO, _NTH1(TX_CONF))
#define TX_AF _CAT2(GPIO_AF, _NTH2(TX_CONF))

#define DMA_CONF _CAT3(uart, id, _dma)
#define DMA_NUM _NTH0(DMA_CONF)
#define DMA_DEV _CAT2(DMA, DMA_NUM)
#define DMA_RX_CONF _NTH1(DMA_CONF)
#define DMA_TX_CONF _NTH2(DMA_CONF)

#define DMA_RX_NUM DMA_RX_CONF
#define DMA_RX_CH _CAT2(DMA_CHANNEL, DMA_RX_NUM)
#define DMA_RX_IRQ _CAT5(NVIC_DMA, DMA_NUM, _CHANNEL, DMA_RX_NUM, _IRQ)
#define dma_rx_isr _CAT5(dma, DMA_NUM, _channel, DMA_RX_NUM, _isr)

#define DMA_TX_NUM DMA_TX_CONF
#define DMA_TX_CH _CAT2(DMA_CHANNEL, DMA_TX_NUM)
#define DMA_TX_IRQ _CAT5(NVIC_DMA, DMA_NUM, _CHANNEL, DMA_TX_NUM, _IRQ)
#define dma_tx_isr _CAT5(dma, DMA_NUM, _channel, DMA_TX_NUM, _isr)

//#if defined(_CAT3(uart, id, _rs485))
#define RS485_CONF _CAT3(uart, id, _rs485)

#define RS485_na 0
#define RS485_re 1

#define DE_CONF _UNWR(_NTH0(RS485_CONF))
#define DE_PORT _CAT2(GPIO, _NTH0(DE_CONF))
#define DE_PAD _CAT2(GPIO, _NTH1(DE_CONF))
#define DE_AF _CAT2(GPIO_AF, _NTH2(DE_CONF))

#if RS485_re == _CAT2(RS485_, _NTH1(RS485_CONF))
#define RE_CONF _UNWR(_NTH2(RS485_CONF))
#define RE_PORT _CAT2(GPIO, _NTH0(RE_CONF))
#define RE_PAD _CAT2(GPIO, _NTH1(RE_CONF))
#define RE_AF _CAT2(GPIO_AF, _NTH2(RE_CONF))
#endif
//#endif

#ifdef STM32F1
#define USART_TDR USART_DR
#define USART_RDR USART_DR

static inline uint32_t usart_read_flag(uint32_t usart) {
  return USART_SR(usart);
}

#define usart_clear_flag(usart, flag)
#else /* !STM32F1 */

#define USART_STOPBITS_1 USART_CR2_STOP_1_0BIT
#define USART_SR_IDLE USART_ISR_IDLE
#define USART_SR_LBD USART_ISR_LBDF
#define USART_SR_ORE USART_ISR_ORE
#define USART_SR_NE USART_ISR_NF
#define USART_SR_FE USART_ISR_FE
#define USART_SR_PE USART_ISR_PE

static inline uint32_t usart_read_flag(uint32_t usart) {
  return USART_ISR(usart);
}

static inline void usart_clear_flag(uint32_t usart, uint32_t flag) {
  USART_ICR(usart) |= flag;
}
#endif /* <STM32F1 */

static inline uint32_t usart_enabled(uint32_t usart) {
  return USART_CR1(usart) & USART_CR1_UE;
}

static inline void usart_enable_tx_complete_interrupt(uint32_t usart) {
  USART_CR1(usart) |= USART_CR1_TCIE;
}

static inline void usart_disable_tx_complete_interrupt(uint32_t usart) {
  USART_CR1(usart) &= ~USART_CR1_TCIE;
}

static inline uint32_t usart_tx_complete_interrupt_enabled(uint32_t usart) {
  return USART_CR1(usart) & USART_CR1_TCIE;
}

static inline void usart_clear_tx_complete_flag(uint32_t usart) {
  USART_SR(usart) &= ~USART_SR_TC;
}

static inline void usart_enable_rx_idle_interrupt(uint32_t usart) {
  USART_CR1(usart) |= USART_CR1_IDLEIE;
}

static inline void usart_disable_rx_idle_interrupt(uint32_t usart) {
  USART_CR1(usart) &= ~USART_CR1_IDLEIE;
}

static inline void usart_clear_rx_idle_flag(uint32_t usart) {
  /*USART_SR(usart) &= ~USART_SR_IDLE;*/
  (void)USART_SR(usart);
  (void)USART_DR(usart);
}

static inline uint32_t usart_rx_idle_interrupt_enabled(uint32_t usart) {
  return USART_CR1(usart) & USART_CR1_IDLEIE;
}

#define USART_SR_ERR (USART_SR_ORE | USART_SR_PE | USART_SR_FE | USART_SR_LBD)
#define USART_CR1_ERRIE (USART_CR1_PEIE/* | USART_CR1_RXNEIE*/)
#define USART_CR2_ERRIE (USART_CR2_LBDIE)
#define USART_CR3_ERRIE (USART_CR3_EIE)

static inline void usart_enable_error_interrupts(uint32_t usart) {
  USART_CR1(usart) |= USART_CR1_ERRIE;
  USART_CR2(usart) |= USART_CR2_ERRIE;
  USART_CR3(usart) |= USART_CR3_ERRIE;
}

static inline void usart_disable_error_interrupts(uint32_t usart) {
  USART_CR1(usart) &= ~USART_CR1_ERRIE;
  USART_CR2(usart) &= ~USART_CR2_ERRIE;
  USART_CR3(usart) &= ~USART_CR3_ERRIE;
}

static inline void usart_clear_error_flags(uint32_t usart) {
  USART_SR(usart) &= ~USART_SR_LBD;
  (void)USART_SR(usart);
  (void)USART_DR(usart);
}

static inline uint32_t dma_is_active(uint32_t dma, uint8_t channel) {
  return DMA_CCR(dma, channel) & DMA_CCR_EN;
}

static inline uint32_t dma_count_data(uint32_t dma, uint8_t channel) {
  return DMA_CNDTR(dma, channel);
}

enum {
  rs485en = (1 << 4),
  rs485tx = (1 << 5),
};

static uint8_t state = 0;

static void rs485_sw(void) {
#ifdef STM32F1
  if (state & rs485en) {
    if (state & rs485tx) {
#if defined(RE_CONF) && defined(DE_CONF) && RE_PORT == DE_PORT
      /* disable receiver and enable driver */
      gpio_set(RE_PORT, RE_PAD | DE_PAD);
#else
#ifdef RE_CONF
      /* disable receiver */
      gpio_set(RE_PORT, RE_PAD);
#endif
#ifdef DE_CONF
      /* enable driver */
      gpio_set(DE_PORT, DE_PAD);
#endif
#endif
    } else {
#if defined(RE_CONF) && defined(DE_CONF) && RE_PORT == DE_PORT
      /* disable driver and enable receiver */
      gpio_clear(DE_PORT, RE_PAD | DE_PAD);
#else
#ifdef DE_CONF
      /* disable driver */
      gpio_clear(DE_PORT, DE_PAD);
#endif
#ifdef RE_CONF
      /* enable receiver */
      gpio_clear(RE_PORT, RE_PAD);
#endif
#endif
    }
  } else {
#ifdef RE_CONF
    /* disable receiver */
    gpio_set(RE_PORT, RE_PAD);
#endif
#ifdef DE_CONF
    /* disable driver */
    gpio_clear(DE_PORT, DE_PAD);
#endif
  }
#endif /* STM32F1 */
}

static void rs485_tx(void) {
  state |= rs485tx;
  rs485_sw();
}

static void rs485_rx(void) {
  state &= ~rs485tx;
  rs485_sw();
}

static io_idx_t rx_index;
io_queue(rx_queue, UART_RX_QUEUE_SIZE);

static io_idx_t tx_index;
io_queue(tx_queue, UART_TX_QUEUE_SIZE);

static void tx_disable(void) {
  /* Stop RX DMA transfer */
  dma_disable_channel(DMA_DEV, DMA_TX_CH);
  
  io_get_end(&tx_queue, tx_index);
}

static void tx_enable(void) {
  const void *tx_ptr;
  io_len_t tx_len;
  
  io_get_req(&tx_queue, &tx_index, &tx_ptr, &tx_len);
  
  rs485_tx();
  
  dma_set_memory_address(DMA_DEV, DMA_TX_CH, (uint32_t)tx_ptr);
  /* Set max number of data bytes */
  dma_set_number_of_data(DMA_DEV, DMA_TX_CH, tx_len);

  usart_clear_tx_complete_flag(UART_DEV);
  usart_enable_tx_complete_interrupt(UART_DEV);
  
  /* Start RX DMA transfer */
  dma_enable_channel(DMA_DEV, DMA_TX_CH);
}

static void rx_disable(void) {
  /* Stop RX DMA transfer */
  dma_disable_channel(DMA_DEV, DMA_RX_CH);
  
  io_len_t rx_len = IO_PACKET_SIZE - dma_count_data(DMA_DEV, DMA_RX_CH);
  
  io_put_end(&rx_queue, rx_index, rx_len);
}

static void rx_enable(void) {
  void *rx_ptr;
  io_len_t rx_len;
  
  io_put_req(&rx_queue, &rx_index, &rx_ptr, &rx_len);

  if (usart_get_flag(UART_DEV, USART_SR_RXNE)) {
    io_put_byte(&rx_ptr, &rx_len, USART_RDR(UART_DEV));
  }
  
  dma_set_memory_address(DMA_DEV, DMA_RX_CH, (uint32_t)rx_ptr);
  /* Set max number of data bytes */
  dma_set_number_of_data(DMA_DEV, DMA_RX_CH, rx_len);
  
  /* Start RX DMA transfer */
  dma_enable_channel(DMA_DEV, DMA_RX_CH);
}

void __weak__ uart_fn(id, rx_handle, void) {}

static inline void rx_handle(void) {
  if (io_fill(&rx_queue) > 0) {
    uart_fn(id, rx_handle);
  }
}

void uart_fn(id, conf, uint32_t baudrate, uint8_t databits, uint8_t stopbits, uint8_t parity) {
  usart_disable(UART_DEV);
  
  /* Setup USART parameters */
	usart_set_baudrate(UART_DEV, baudrate);
	usart_set_databits(UART_DEV, databits);
  
  {
    uint32_t stopbits_value = USART_STOPBITS_1;

    switch (stopbits) {
    case uart_stopbits_1p5:
      stopbits_value = USART_STOPBITS_1_5;
      break;
    case uart_stopbits_2:
      stopbits_value = USART_STOPBITS_2;
      break;
    }
    
    usart_set_stopbits(UART_DEV, stopbits_value);
  }

  {
    uint32_t parity_value = USART_PARITY_NONE;
    
    switch (parity) {
    case uart_parity_odd:
      parity_value = USART_PARITY_ODD;
      break;
    case uart_parity_even:
      parity_value = USART_PARITY_EVEN;
      break;
    }
    
    usart_set_parity(UART_DEV, parity_value);
  }
  
  usart_enable(UART_DEV);
}

void uart_fn(id, set_rs485, bool enable) {
  if (enable) {
    state |= rs485en;
  } else {
    state &= ~rs485en;
  }
  
  rs485_sw();
}

void uart_fn(id, init, void) {
  rcc_periph_clock_enable(UART_RCC);

  nvic_enable_irq(DMA_TX_IRQ);
  nvic_enable_irq(DMA_RX_IRQ);
  nvic_enable_irq(UART_IRQ);

#ifdef STM32F1
  /* Setup GPIO pin TX for transmit */
	gpio_set_mode(TX_PORT, GPIO_MODE_OUTPUT_50_MHZ,
                GPIO_CNF_OUTPUT_ALTFN_PUSHPULL, TX_PAD);
  /* Setup GPIO pin RX for receive */
	gpio_set_mode(RX_PORT, GPIO_MODE_INPUT,
                GPIO_CNF_INPUT_FLOAT, RX_PAD);

#ifdef DE_PORT
  gpio_clear(DE_PORT, DE_PAD); /* disable driver */
  gpio_set_mode(DE_PORT, GPIO_MODE_OUTPUT_50_MHZ,
                GPIO_CNF_OUTPUT_PUSHPULL, DE_PAD);
#endif
  
#ifdef RE_PORT
  gpio_set(RE_PORT, RE_PAD); /* disable receiver */
  gpio_set_mode(RE_PORT, GPIO_MODE_OUTPUT_50_MHZ,
                GPIO_CNF_OUTPUT_PUSHPULL, RE_PAD);
#endif
#else /* !STM32F1 */
  gpio_mode_setup(TX_PORT, GPIO_MODE_AF, GPIO_PUPD_NONE, TX_PAD);
  gpio_set_af(TX_PORT, TX_AF, TX_PAD);
  gpio_set_output_options(TX_PORT, GPIO_OTYPE_PP, GPIO_OSPEED_HIGH, TX_PAD);

#ifdef DE_PORT
  gpio_mode_setup(DE_PORT, GPIO_MODE_OUTPUT, GPIO_PUPD_NONE, DE_PAD);
  gpio_set_af(DE_PORT, DE_AF, DE_PAD);
  gpio_set_output_options(DE_PORT, GPIO_OTYPE_PP, GPIO_OSPEED_HIGH, DE_PAD);
#endif
  
  gpio_mode_setup(RX_PORT, GPIO_MODE_AF, GPIO_PUPD_NONE, RX_PAD);
  gpio_set_af(RX_PORT, RX_AF, RX_PAD);
  gpio_set_output_options(RX_PORT, GPIO_OTYPE_PP, GPIO_OSPEED_HIGH, RX_PAD);
#endif /* <STM32F1 */

  /* rs485 */
#ifdef RS485_CONF
#ifndef STM32F1
  USART_CR3(UART_DEV) &= ~(USART_CR3_DEP); // | USART_CR3_OVRDIS | USART_CR3_ONEBIT);
  USART_CR3(UART_DEV) |= USART_CR3_DEM | USART_CR3_DDRE;
  //USART_CR2(UART_DEV) |= USART_CR2_TXINV | USART_CR2_RXINV;
#endif
#endif
  
  usart_set_mode(UART_DEV, USART_MODE_TX_RX);
  usart_set_flow_control(UART_DEV, USART_FLOWCONTROL_NONE);
  
  /* Setup USART DMA transfer */
  
  /* TX transfer DMA */
  dma_channel_reset(DMA_DEV, DMA_TX_CH);
  dma_set_priority(DMA_DEV, DMA_TX_CH, DMA_CCR_PL_MEDIUM);
  dma_set_peripheral_size(DMA_DEV, DMA_TX_CH, DMA_CCR_PSIZE_8BIT);
  dma_set_memory_size(DMA_DEV, DMA_TX_CH, DMA_CCR_MSIZE_8BIT);
  dma_enable_memory_increment_mode(DMA_DEV, DMA_TX_CH);
  dma_set_read_from_memory(DMA_DEV, DMA_TX_CH);
  dma_set_peripheral_address(DMA_DEV, DMA_TX_CH, (uint32_t)&USART_TDR(UART_DEV));
  
  /* RX transfer DMA */
  dma_channel_reset(DMA_DEV, DMA_RX_CH);
  dma_set_priority(DMA_DEV, DMA_RX_CH, DMA_CCR_PL_MEDIUM);
  dma_set_peripheral_size(DMA_DEV, DMA_RX_CH, DMA_CCR_PSIZE_8BIT);
  dma_set_memory_size(DMA_DEV, DMA_RX_CH, DMA_CCR_MSIZE_8BIT);
  dma_enable_memory_increment_mode(DMA_DEV, DMA_RX_CH);
  dma_set_read_from_peripheral(DMA_DEV, DMA_RX_CH);
  dma_set_peripheral_address(DMA_DEV, DMA_RX_CH, (uint32_t)&USART_RDR(UART_DEV));
  
  dma_enable_transfer_complete_interrupt(DMA_DEV, DMA_TX_CH);
  /*dma_enable_transfer_error_interrupt(DMA_DEV, DMA_TX_CH);*/
  dma_enable_transfer_complete_interrupt(DMA_DEV, DMA_RX_CH);
  /*dma_enable_transfer_error_interrupt(DMA_DEV, DMA_RX_CH);*/
  
  usart_enable_tx_dma(UART_DEV);
  usart_enable_rx_dma(UART_DEV);
}

void uart_fn(id, on, void) {
  /* Reset buffers to initial state */
  io_reset(&rx_queue);
  io_reset(&tx_queue);

  usart_enable_error_interrupts(UART_DEV);
  usart_enable_rx_idle_interrupt(UART_DEV);
  
  /* Enable the USART */
	usart_enable(UART_DEV);
  usart_clear_rx_idle_flag(UART_DEV);
  rx_enable();
}

void uart_fn(id, off, void) {
  rx_disable();
  rx_handle();
  /* Disable the USART */
	usart_disable(UART_DEV);
  usart_disable_rx_idle_interrupt(UART_DEV);
  usart_disable_error_interrupts(UART_DEV);
}

void uart_fn(id, done, void) {
  usart_disable_tx_dma(UART_DEV);
  usart_disable_rx_dma(UART_DEV);
  
  dma_disable_transfer_complete_interrupt(DMA_DEV, DMA_TX_CH);
  /*dma_disable_transfer_error_interrupt(DMA_DEV, DMA_TX_CH);*/
  dma_disable_transfer_complete_interrupt(DMA_DEV, DMA_RX_CH);
  /*dma_disable_transfer_error_interrupt(DMA_DEV, DMA_RX_CH);*/
  
  dma_disable_channel(DMA_DEV, DMA_TX_CH);
  dma_disable_channel(DMA_DEV, DMA_RX_CH);
  
  nvic_disable_irq(UART_IRQ);
  nvic_disable_irq(DMA_RX_IRQ);
  nvic_disable_irq(DMA_TX_IRQ);
  
  rcc_periph_clock_disable(UART_RCC);
}

bool uart_fn(id, tx_request, uart_idx_t *idx, void **tx_ptr) {
  io_len_t tx_len;

  /* Actually allocate space to write a new data */
  return io_put_req(&tx_queue, idx, tx_ptr, &tx_len);
}

void uart_fn(id, tx_confirm, uart_idx_t idx, uart_len_t len) {
  /* Actually finalize write operation */
  io_put_end(&tx_queue, idx, len);
  
  if (!dma_is_active(DMA_DEV, DMA_TX_CH) &&
      io_fill(&tx_queue) > 0) {
    usart_disable_tx_complete_interrupt(UART_DEV);
    /* When the transmission isn't already started we must start transmission */
    tx_enable();
  }
}

static uint8_t status = 0;

uint8_t uart_fn(id, status, void) {
  return status | (usart_enabled(UART_DEV) ? uart_active_state : 0);
}

void __weak__ uart_fn(id, st_handle, void) {}

void uart_isr(void) {
  uint32_t flags = usart_read_flag(UART_DEV);
  
  /* RX idle detection sequence */
  if ((flags & USART_SR_IDLE) &&
      usart_rx_idle_interrupt_enabled(UART_DEV) &&
      dma_is_active(DMA_DEV, DMA_RX_CH) &&
      dma_count_data(DMA_DEV, DMA_RX_CH) < IO_PACKET_SIZE) {
    usart_clear_rx_idle_flag(UART_DEV);
    rx_disable();
    rx_enable();
  }
  
  /* RS485 driver to receiver switch sequence */
  if ((flags & USART_SR_TC) &&
      usart_tx_complete_interrupt_enabled(UART_DEV) &&
      !dma_is_active(DMA_DEV, DMA_TX_CH)) {
    usart_clear_tx_complete_flag(UART_DEV);
    /* Disable transmitter */
    rs485_rx();
    usart_disable_tx_complete_interrupt(UART_DEV);
  }

  if ((flags & USART_SR_ERR)) {
    usart_clear_error_flags(UART_DEV);
    
    status
      = ((flags & USART_SR_ORE) ? uart_overrun_error : 0)
      | ((flags & USART_SR_PE) ? uart_parity_error : 0)
      | ((flags & USART_SR_FE) ? uart_framing_error : 0)
      | ((flags & USART_SR_LBD) ? uart_break_state : 0)
      ;
    
    uart_fn(id, st_handle);
    
    status = 0;
  }
  
  usart_clear_flag(UART_DEV, flags);
  rx_handle();
}

void dma_tx_isr(void) {
  /*bool error = dma_get_interrupt_flag(DMA_DEV, DMA_TX_CH, DMA_TEIF);*/
  if (dma_get_interrupt_flag(DMA_DEV, DMA_TX_CH, DMA_TCIF)) {
    dma_clear_interrupt_flags(DMA_DEV, DMA_TX_CH,
                              DMA_GIF |
                              DMA_TEIF |
                              DMA_TCIF);
    
    tx_disable();
    if (io_fill(&tx_queue) > 0) {
      /* We need try to resume transmission when we have data in queue */
      tx_enable();
    }
  }
}

bool uart_fn(id, rx_request, uart_idx_t *idx, const void **ptr, uart_len_t *len) {
  /* Actually prepare reading of received data */
  return io_get_req(&rx_queue, idx, ptr, len);
}

void uart_fn(id, rx_confirm, uart_idx_t idx) {
  io_get_end(&rx_queue, idx);
}

void dma_rx_isr(void) {
  /*bool error = dma_get_interrupt_flag(DMA_DEV, DMA_RX_CH, DMA_TEIF);*/
  if (dma_get_interrupt_flag(DMA_DEV, DMA_RX_CH, DMA_TCIF)) {
    dma_clear_interrupt_flags(DMA_DEV, DMA_RX_CH,
                              DMA_GIF |
                              DMA_TEIF |
                              DMA_TCIF);
    rx_disable();
    rx_enable();
    rx_handle();
  }
}
