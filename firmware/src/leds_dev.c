#include <libopencm3/stm32/rcc.h>
#include <libopencm3/stm32/gpio.h>

#include "ticks.h"
#include "led_dev.h"
#include "leds_dev.h"

#include "helper.h"
#include "macro.h"
#include "config.h"

#define LIGHT_ON_TIME roundof(ticks_t, LEDS_LIGHT_TIME_MS)

#define BLINK_ON_TIME roundof(ticks_t, LEDS_BLINK_CYCLE_MS)
#define BLINK_OFF_TIME roundof(ticks_t, LEDS_BLINK_PAUSE_MS)

#define FLASH_ON_TIME roundof(ticks_t, LEDS_FLASH_CYCLE_MS)
#define FLASH_OFF_TIME roundof(ticks_t, LEDS_FLASH_PAUSE_MS)

_MAP(led_def, leds_list);

void leds_init(void) {
  _MAP(; led_init, leds_list);
}

void leds_done(void) {
  _MAP(; led_done, leds_list);
}

static uint8_t leds_mode[] = {
#define led_mode_def(id) led_mode_mk(led_evt_unused, led_act_light),
  _MAP(led_mode_def, leds_list)
#undef led_mode_def
};

void leds_set_mode(void) {
  uint8_t led = 0;

  for (; led < countof(leds_mode); led++) {
    leds_conf(led, setting.leds.mode[led]);
  }
}

#define fire_event(event) (event_state |= (1 << (event)))
#define blow_event(event) (event_state &= ~(1 << (event)))
#define have_event(event) ((event_state >> (event)) & 0x1)
static uint32_t event_state = 0;

static void turn_on(uint8_t led) {
#define led_on_func(id) case _CAT2(led_name_, id): led_on(id); break;
  switch (led) {
    _MAP(led_on_func, leds_list);
  }
#undef led_on_func
}

static void turn_off(uint8_t led) {
#define led_off_func(id) case _CAT2(led_name_, id): led_off(id); break;
  switch (led) {
    _MAP(led_off_func, leds_list);
  }
#undef led_off_func
}

static char turned_on(uint8_t led) {
#define led_get_func(id) case _CAT2(led_name_, id): return led_get(id);
  switch (led) {
    _MAP(led_get_func, leds_list);
  }
#undef led_get_func
  return 0;
}

static inline char turned_off(uint8_t led) {
  return !turned_on(led);
}

void leds_conf(uint8_t led, uint8_t mode) {
  leds_mode[led] = mode;
}

#define fire_led(led) (leds_state |= (1 << (led)))
#define blow_led(led) (leds_state &= ~(1 << (led)))
#define have_led(led) ((leds_state >> (led)) & 0x1)
static uint8_t leds_state = 0;

static ticks_t leds_time[] = {
#define led_time_def(id) TICKS_MIN,
  _MAP(led_time_def, leds_list)
#undef led_time_def
};

void leds_trig(uint8_t event, uint8_t state) {
  if (state & led_turn_on) {
    fire_event(event);

    leds_step();
  }
  
  if (state & led_turn_off) {
    blow_event(event);
  }
}

void leds_step(void) {
  uint8_t led = 0;
  ticks_t now = ticks_read();

  for (; led < countof(leds_time); led++) {
    if (have_led(led)) { /* led is active */
      ticks_t delta = ticks_diff(leds_time[led], now);
      ticks_t delay = LIGHT_ON_TIME;
      
      switch (led_act_get(leds_mode[led])) {
      case led_act_blink:
        delay = turned_on(led) ? BLINK_ON_TIME : BLINK_OFF_TIME;
        break;
      case led_act_flash:
        delay = turned_on(led) ? FLASH_ON_TIME : FLASH_OFF_TIME;
        break;
      }

      if (delta >= delay) { /* timeout reached */
        leds_time[led] = now;
        
        if (turned_on(led)) { /* led was turned on */
          if (led_act_get(leds_mode[led]) != led_act_light ||
              !have_event(led_evt_get(leds_mode[led]))) {
            turn_off(led);
          }

          if (!have_event(led_evt_get(leds_mode[led]))) {
            blow_led(led);
          }
        } else { /* led was turned off */
          if (led_act_get(leds_mode[led]) != led_act_light &&
              have_event(led_evt_get(leds_mode[led]))) {
            turn_on(led);
          } else {
            blow_led(led);
          }
        }
      }
    } else {
      if (have_event(led_evt_get(leds_mode[led]))) { /* led is inactive */
        /* turn on led */
        leds_time[led] = now;
        fire_led(led);
        turn_on(led);
      }
    }
  }
}
