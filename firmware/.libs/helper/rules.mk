libhelper.BASEPATH := $(dir $(lastword $(MAKEFILE_LIST)))

TARGET.LIBS += libhelper
libhelper.INHERIT := firmware
libhelper.CDIRS := $(libhelper.BASEPATH)include
libhelper.SRCS := $(addprefix $(libhelper.BASEPATH)src/,\
  ringbuf.c \
  fifo.c \
  pool.c \
  dbglog.c \
  ticks.c)

TARGET.OPTS += libhelper
libhelper.OPTS := $(addprefix $(libhelper.BASEPATH)src/,\
  dbglog.cf)
