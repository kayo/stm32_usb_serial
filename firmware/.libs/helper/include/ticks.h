#ifndef __TICKS_H__
#define __TICKS_H__

#include <stdint.h>

#define TICKS_MIN 0
#define TICKS_MAX UINT32_MAX
typedef uint32_t ticks_t;

extern ticks_t ticks;

static inline ticks_t ticks_read(void) {
  return ticks;
}

static inline void ticks_tick(ticks_t add) {
  ticks += add;
}

static inline ticks_t ticks_diff(ticks_t from, ticks_t to) {
  return from <= to ? to - from : TICKS_MAX - from + to;
}


#endif /* __TICKS_H__ */
