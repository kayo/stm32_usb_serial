#ifndef __DBGLOG_H__
#define __DBGLOG_H__

#include "macro.h"
#include "ringbuf.h"

#define DBGLOG_none 0
#define DBGLOG_err  1
#define DBGLOG_warn 2
#define DBGLOG_info 3

#define DBGLOG_MATCH(level) (_CAT2(DBGLOG_, level) <= _CAT2(DBGLOG_, DBGLOG_LEVEL))

#define dbglog_call(grp, fmt, ...) dbglog_real("[" #grp "] " fmt DBGLOG_ENDLINE, ##__VA_ARGS__)

#if DBGLOG_MATCH(err)
#define dbglog_err(fmt, ...) dbglog_call(err, fmt, ##__VA_ARGS__)
#else
#define dbglog_err(fmt, ...)
#endif

#if DBGLOG_MATCH(warn)
#define dbglog_warn(fmt, ...) dbglog_call(warn, fmt, ##__VA_ARGS__)
#else
#define dbglog_warn(fmt, ...)
#endif

#if DBGLOG_MATCH(info)
#define dbglog_info(fmt, ...) dbglog_call(info, fmt, ##__VA_ARGS__)
#else
#define dbglog_info(fmt, ...)
#endif

#define dbglog(grp, fmt, ...) _CAT2(dbglog_, grp)(fmt, ##__VA_ARGS__)

void dbglog_real(const char *fmt, ...);

extern const ringbuf_config_t dbglog_buffer;

#endif /* __DBGLOG_H__ */
