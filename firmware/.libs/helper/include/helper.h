#ifndef __HELPER_H__
#define __HELPER_H__

#ifndef countof
#define countof(static_array) (sizeof(static_array)/sizeof((static_array)[0]))
#endif

#ifndef roundof
#define roundof(integer_type, float_number) (((integer_type)((float_number)*2)+1)>>1)
#endif

#ifndef floatof
#define floatof(generic_number) ((double)(generic_number))
#endif

#ifndef __packed__
#define __packed__ __attribute__((packed))
#endif

#ifndef __weak__
#define __weak__ __attribute__((weak))
#endif

#ifndef __weak_alias__
#define __weak_alias__(name) __attribute__((weak,alias(#name)))
#endif

#endif /* __HELPER_H__ */
