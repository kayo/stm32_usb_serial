#ifndef __FIFO_H__
#define __FIFO_H__
/**
 * @defgroup fifo Simple FIFO container
 * @brief The embedded implementation of First In First Out queue
 *
 * @{
 */

#define fifo_nm(type, name) fifo_##type##_##name

/**
 * @brief Call FIFO method
 *
 * @param type The name of FIFO type
 * @param name The name of FIFO method
 * @param ... The method arguments
 * @return The method result
 */
#define fifo_fn(type, name, ...) fifo_nm(type, name)(__VA_ARGS__)

#define fifo_size(type, size) (sizeof(fifo_nm(type, t)) + sizeof(fifo_nm(type, data_t)) * (size))

/**
 * @brief The FIFO type definition macro
 *
 * @param type The name of FIFO type
 * @param d_t The type of elements
 * @param s_t The type of counters
 * @return The definitions of types and methods
 */
#define fifo_t(type, d_t, s_t, ...)      \
  typedef d_t fifo_nm(type, data_t);     \
  typedef s_t fifo_nm(type, size_t);     \
  typedef struct {                       \
    fifo_nm(type, size_t) rpos;          \
    fifo_nm(type, size_t) wpos;          \
    fifo_nm(type, size_t) fill;          \
    fifo_nm(type, size_t) size;          \
    fifo_nm(type, data_t) data[];        \
  } fifo_nm(type, t);                    \
  static inline fifo_nm(type, size_t)    \
  fifo_fn(type, fill,                    \
          fifo_nm(type, t) *fifo) {      \
    return fifo->fill;                   \
  }                                      \
  static inline fifo_nm(type, size_t)    \
  fifo_fn(type, free,                    \
          fifo_nm(type, t) *fifo) {      \
    return fifo->size - fifo->fill;      \
  }                                      \
  static inline char                     \
  fifo_fn(type, first,                   \
          fifo_nm(type, t) *fifo,        \
          fifo_nm(type, data_t) *data) { \
    if (fifo->fill == 0) {               \
      return 0; /* queue is empty */     \
    }                                    \
    if (data) {                          \
      *data = fifo->data[fifo->rpos];    \
    }                                    \
    return 1;                            \
  }                                      \
  __VA_ARGS__ void                       \
  fifo_fn(type, init,                    \
          fifo_nm(type, t) *fifo);       \
  __VA_ARGS__ char                       \
  fifo_fn(type, push,                    \
          fifo_nm(type, t) *fifo,        \
          fifo_nm(type, data_t) data);   \
  __VA_ARGS__ void                       \
  fifo_fn(type, pushf,                   \
          fifo_nm(type, t) *fifo,        \
          fifo_nm(type, data_t) data);   \
  __VA_ARGS__ char                       \
  fifo_fn(type, shift,                   \
          fifo_nm(type, t) *fifo,        \
          fifo_nm(type, data_t) *data)
  
/**
 * @brief The FIFO method implementation macro
 *
 * @param type The name of FIFO type
 * @return The implementations of methods
 */
#define fifo_d(type, ...)                \
  __VA_ARGS__ void                       \
  fifo_fn(type, init,                    \
          fifo_nm(type, t) *fifo) {      \
    fifo->rpos = 0;                      \
    fifo->wpos = 0;                      \
    fifo->fill = 0;                      \
  }                                      \
  __VA_ARGS__ char                       \
  fifo_fn(type, push,                    \
          fifo_nm(type, t) *fifo,        \
          fifo_nm(type, data_t) data) {  \
    if (fifo->fill == fifo->size) {      \
      return 0; /* queue is full */      \
    }                                    \
    fifo->fill ++;                       \
    fifo->data[fifo->wpos ++] = data;    \
    if (fifo->wpos == fifo->size) {      \
      fifo->wpos = 0;                    \
    }                                    \
    return 1;                            \
  }                                      \
  __VA_ARGS__ void                       \
  fifo_fn(type, pushf,                   \
          fifo_nm(type, t) *fifo,        \
          fifo_nm(type, data_t) data) {  \
    if (fifo->fill == fifo->size) {      \
      fifo->rpos ++; /* queue is full */ \
    } else {                             \
      fifo->fill ++;                     \
    }                                    \
    fifo->data[fifo->wpos ++] = data;    \
    if (fifo->rpos == fifo->size) {      \
      fifo->rpos = 0;                    \
    }                                    \
    if (fifo->wpos == fifo->size) {      \
      fifo->wpos = 0;                    \
    }                                    \
  }                                      \
  __VA_ARGS__ char                       \
  fifo_fn(type, shift,                   \
          fifo_nm(type, t) *fifo,        \
          fifo_nm(type, data_t) *data) { \
    if (fifo->fill == 0) {               \
      return 0; /* queue is empty */     \
    }                                    \
    fifo->fill --;                       \
    if (data) {                          \
      *data = fifo->data[fifo->rpos ++]; \
    }                                    \
    if (fifo->rpos == fifo->size) {      \
      fifo->rpos = 0;                    \
    }                                    \
    return 1;                            \
  }

/**
 * @brief The FIFO definition macro
 *
 * @param type The type of FIFO
 * @param name The name of FIFO
 */
#define fifo_def(type, name, ...)   \
  __VA_ARGS__ fifo_nm(type, t) name

/**
 * @brief The static FIFO instantiation macro
 *
 * @param type The type of FIFO
 * @param name The name of FIFO
 * @param size The size of FIFO
 */
#define fifo_inst(type, name, size, ...)       \
  __VA_ARGS__ fifo_nm(type, t) name = {        \
    0, 0, 0, size, { [0 ... size - 1] = 0 }    \
  }

/**
 * @brief The dynamic FIFO instantiation helper
 *
 * @param type The type of FIFO
 * @param size The size of FIFO
 * @return The pointer to new FIFO instance
 */
#define fifo_new(type, size_, ...) ({          \
      __VA_ARGS__ fifo_nm(type, t) *__fifo__ = \
        malloc(fifo_size(type, size_));        \
      __fifo__->size = size_;                  \
      fifo_fn(type, init, __fifo__);           \
      __fifo__;                                \
    })

#define fifo_del(fifo) free(fifo)

/**
 * @}
 */
#endif /* __FIFO_H__ */
