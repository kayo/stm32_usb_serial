#ifndef __POOL_H__
#define __POOL_H__
/**
 * @defgroup pool Simple memory pool
 * @brief The embedded implementation of memory pool
 *
 * The memory pool contains some number of chunks of equal size which can be allocated (used) and freed at any moment of time.
 * Unlike a traditional heap memory the memory pools is more light and fast which makes it pretty usable for embedded systems.
 *
 * @{
 */

#include <stdint.h>

typedef uint8_t pool_data_t;

#define pool_nm(type, name) pool_##type##_##name

#define pool_fn(type, name, ...) pool_nm(type, name)(__VA_ARGS__)

#define pool_usage_size(count) (((count) + 8) / 8)
#define pool_chunk_size(chunk, count) ((chunk) * (count))

#define pool_size(type, chunk, count) \
  (sizeof(pool_nm(type, t)) +         \
   pool_usage_size(count) +           \
   pool_chunk_size(chunk, count))

/**
 * @brief The pool type definition macro
 *
 * @param type The name of pool type
 * @param l_t The type of chunk length
 * @param c_t The type of chunks counter
 * @return The definitions of types and methods
 */
#define pool_t(type, l_t, c_t, ...)       \
  typedef l_t pool_nm(type, len_t);       \
  typedef c_t pool_nm(type, cnt_t);       \
  typedef struct {                        \
    pool_nm(type, len_t) chunk;           \
    pool_nm(type, cnt_t) count;           \
    pool_data_t data[];                   \
  } pool_nm(type, t);                     \
  static inline void *                    \
  pool_fn(type, pointer,                  \
          pool_nm(type, t) *pool,         \
          pool_nm(type, cnt_t) index) {   \
    return pool->data +                   \
      pool_usage_size(pool->count) +      \
      index * pool->chunk;                \
  }                                       \
  static inline pool_nm(type, cnt_t)      \
  pool_fn(type, index,                    \
          const pool_nm(type, t) *pool,   \
          const void *ptr) {              \
    return ((uint8_t*)ptr - pool->data -  \
            pool_usage_size(pool->count)) \
      / pool->chunk;                      \
  }                                       \
  __VA_ARGS__ void                        \
  pool_fn(type, init,                     \
          pool_nm(type, t) *pool);        \
  __VA_ARGS__ pool_nm(type, cnt_t)        \
  pool_fn(type, usage,                    \
          pool_nm(type, t) *pool);        \
  __VA_ARGS__ char                        \
  pool_fn(type, alloc,                    \
          pool_nm(type, t) *pool,         \
          pool_nm(type, cnt_t) *index);   \
  __VA_ARGS__ void                        \
  pool_fn(type, free,                     \
          pool_nm(type, t) *pool,         \
          pool_nm(type, cnt_t) index)

/**
 * @brief The pool method implementation macro
 *
 * @param type The name of pool type
 * @return The implementations of methods
 */
#define pool_d(type, ...)                            \
  __VA_ARGS__ void                                   \
  pool_fn(type, init,                                \
          pool_nm(type, t) *pool) {                  \
    pool_nm(type, cnt_t) i = 0;                      \
    for (; i < pool_usage_size(pool->count); i ++) { \
      pool->data[i] = 0;                             \
    }                                                \
  }                                                  \
  __VA_ARGS__ pool_nm(type, cnt_t)                   \
  pool_fn(type, usage,                               \
          pool_nm(type, t) *pool) {                  \
    pool_nm(type, cnt_t) n = 0;                      \
    pool_nm(type, cnt_t) i = 0;                      \
    pool_nm(type, cnt_t) c =                         \
      pool->count / (sizeof(unsigned int) * 8);      \
    for (; i < c; i++) {                             \
      n += __builtin_popcount(((unsigned int*)       \
                               pool->data)[i]);      \
    }                                                \
    i = i * sizeof(unsigned int);                    \
    c = pool_usage_size(pool->count);                \
    for (; i < c; i++) {                             \
      n += __builtin_popcount(pool->data[i]);        \
    }                                                \
    return n;                                        \
  }                                                  \
  __VA_ARGS__ char                                   \
  pool_fn(type, alloc,                               \
          pool_nm(type, t) *pool,                    \
          pool_nm(type, cnt_t) *index) {             \
    pool_nm(type, cnt_t) i = 0;                      \
    pool_nm(type, cnt_t) n;                          \
    pool_nm(type, cnt_t) c =                         \
      pool->count / (sizeof(unsigned int) * 8);      \
    for (; i < c; i++) {                             \
      n = __builtin_popcount(((unsigned int*)        \
                              pool->data)[i]);       \
      if (n < sizeof(unsigned int) * 8) {            \
        i *= sizeof(unsigned int);                   \
        goto find_byte;                              \
      }                                              \
    }                                                \
    i = 0;                                           \
  find_byte:                                         \
    c = pool->count / 8;                             \
    for (; i < c; i++) {                             \
      n = __builtin_popcount(pool->data[i]);         \
      if (n < 8) {                                   \
        c = 8;                                       \
        goto find_bit;                               \
      }                                              \
    }                                                \
    if (i == pool_usage_size(pool->count)) {         \
      goto end;                                      \
    }                                                \
    c = pool->count % 8;                             \
  find_bit:                                          \
    for (n = 0; n < c; n++) {                        \
      if (0 == (pool->data[i] & (1 << n))) {         \
        pool->data[i] |= 1 << n;                     \
        *index = i * 8 + n; /* i << 3 + n */         \
        return 1;                                    \
      }                                              \
    }                                                \
  end:                                               \
    return 0;                                        \
  }                                                  \
  __VA_ARGS__ void                                   \
  pool_fn(type, free,                                \
          pool_nm(type, t) *pool,                    \
          pool_nm(type, cnt_t) index) {              \
    pool_nm(type, cnt_t) i = index / 8; /* >> 3 */   \
    pool_nm(type, cnt_t) n = index % 8; /* & 0x7 */  \
    pool->data[i] &= ~(1 << n);                      \
  }

/**
 * @brief The pool definition macro
 *
 * @param type The type of pool
 * @param name The name of pool object
 */
#define pool_def(type, name, ...)               \
  __VA_ARGS__ pool_nm(type, t) name

/**
 * @brief The static pool instantiation macro
 *
 * @param type The type of pool
 * @param name The name of pool object
 * @param chunk The size of single data chunk
 * @param count The total number of chunks
 */
#define pool_inst(type, name, chunk, count, ...) \
  __VA_ARGS__ pool_nm(type, t) name = {          \
    chunk, count,                                \
    { [0 ...                                     \
       (pool_usage_size(count) +                 \
        pool_chunk_size(chunk, count) -          \
        1)] = 0 }                                \
  }

/**
 * @brief The dynamic pool instantiation helper
 *
 * @param type The type of pool
 * @param chunk The size of single data chunk
 * @param count The total number of chunks
 * @return The pointer to new pool instance
 */
#define pool_new(type, chunk_, count_) ({         \
      pool_nm(type, t) *__pool__ =                \
        malloc(pool_size(type, chunk_, count_));  \
      __pool__->chunk = chunk_;                   \
      __pool__->count = count_;                   \
      pool_fn(type, init, __pool__);              \
      __pool__;                                   \
    })

#define pool_del(pool) free(pool)

#endif /* __POOL_H__ */
