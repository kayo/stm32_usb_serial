#ifndef __RINGBUF_H__
#define __RINGBUF_H__
/**
 * @defgroup ringbuf Ring buffer for raw data
 * @brief Ring buffer with direct data access
 *
 * @{
 */

#include <stdint.h>
#include "macro.h"

/**
 * @brief The ring buffer data type
 */
typedef uint8_t ringbuf_data_t;

/**
 * @brief The ring buffer size type
 */
typedef uint16_t ringbuf_size_t;

/**
 * @brief The ring buffer state type
 */
typedef struct {
  /**
   * @brief The current position to write data
   */
  ringbuf_size_t wpos;
  /**
   * @brief The current length of written data
   */
  ringbuf_size_t wlen;
  /**
   * @brief The current position to read data
   */
  ringbuf_size_t rpos;
  /**
   * @brief The current limit of data to read
   */
  ringbuf_size_t rlim;
} ringbuf_state_t;

/**
 * @brief The ring buffer config type
 */
typedef struct {
  /**
   * @brief The size of buffer in bytes
   */
  ringbuf_state_t *state;
  /**
   * @brief The size of buffer in bytes
   */
  ringbuf_size_t size;
  /**
   * @brief The pointer to data of buffer
   */
  ringbuf_data_t *data;
} ringbuf_config_t;

#define ringbuf_static(name, size, ...)             \
  static ringbuf_data_t _CAT2(name, _data)[size];   \
  static ringbuf_state_t _CAT2(name, _state) = {    \
    0, 0, 0, size                                   \
  };                                                \
  __VA_ARGS__ const ringbuf_config_t name = {       \
    &_CAT2(name, _state), size, _CAT2(name, _data)  \
  }

/**
 * @brief Get size of buffer
 *
 * @param rbc The pointer to ring buffer config
 * @return The real size of buffer
 */
#define ringbuf_size(rbc) ((rbc)->size)

/**
 * @brief Get pointer to beginning of buffer
 *
 * @param rbc The pointer to ring buffer config
 * @return The pointer to memory location of buffer
 */
#define ringbuf_data(rbc) ((rbc)->data)

/**
 * @brief Get writing position
 *
 * @param rbc The pointer to ring buffer config
 * @return The position for writing from beginning of buffer
 */
#define ringbuf_wpos(rbc) ((rbc)->state->wpos)

/**
 * @brief Get number of written bytes
 *
 * @param rbc The pointer to ring buffer config
 * @return The number of written bytes in buffer
 *
 * This is also the number of bytes which available for read.
 */
#define ringbuf_wlen(rbc) ((rbc)->state->wlen)

/**
 * @brief Get reading position
 *
 * @param rbc The pointer to ring buffer config
 * @return The position for reading from beginning of buffer
 */
#define ringbuf_rpos(rbc) ((rbc)->state->rpos)

/**
 * @brief Get upper limit for reading
 *
 * @param rbc The pointer to ring buffer config
 * @return The upper limit of bytes which can be read.
 */
#define ringbuf_rlim(rbc) ((rbc)->state->rlim)

/**
 * @brief Reset ring buffer to initial state
 *
 * @param rbc The pointer to ring buffer config
 */
void ringbuf_reset(const ringbuf_config_t *rbc);

/**
 * @brief Try to alloc contiguous space to write data
 *
 * @param rbc The pointer to ring buffer config
 * @param len The length of required space in bytes
 * @return The pointer to free space or NULL when no free space in buffer
 */
void *ringbuf_alloc(const ringbuf_config_t *rbc, ringbuf_size_t len);

/**
 * @brief Complete data writing
 *
 * @param rbc The pointer to ring buffer config
 * @param len The length of actually written data in bytes
 */
void ringbuf_write(const ringbuf_config_t *rbc, ringbuf_size_t len);

/**
 * @brief Try to get pointer to data in buffer
 *
 * @param rbc The pointer to ring buffer config
 * @param len The pointer to length of required data in bytes
 * @return The pointer to data or NULL when no actual data in buffer
 *
 * The len will be set to actual data length after return.
 */
const void *ringbuf_read(const ringbuf_config_t *rbc, ringbuf_size_t *len);

/**
 * @brief Complete data reading
 *
 * @param rbc The pointer to ring buffer config
 * @param len The length of actually readed data in bytes
 */
void ringbuf_seek(const ringbuf_config_t *rbc, ringbuf_size_t len);

/**
 * @}
 */
#endif /* __RINGBUF_H__ */
