#include <stdint.h> /* uint8_t, uint16_t types */
#include <stdarg.h> /* va_list type and va_*() functions */
#include <stdio.h>  /* vsnprintf() function */
#include <string.h> /* strlen() function */

#include "dbglog.h"

/*
  [....<:::::::>.......]
  [::::>.......<::::|..]
  < - rdp (read position)
  > - wrp (write position)
  | - top (top data limit)
  . - free
  : - data
*/

ringbuf_static(dbglog_buffer, DBGLOG_BUFFER);

void dbglog_real(const char *fmt, ...) {
  va_list ap;
  
  ringbuf_size_t len = strlen(fmt);
  
  for (; ; ) {
    void *ptr = ringbuf_alloc(&dbglog_buffer, len);
    
    if (ptr == NULL) {
      return;
    }
    
    va_start(ap, fmt);
    int cnt = vsnprintf(ptr, len, fmt, ap);
    va_end(ap);
    
    if (cnt < 0) {
      len += len / 2;
    } else {
      ringbuf_write(&dbglog_buffer, cnt);
      break;
    }
  }
}
