#include "fifo.h"

#ifdef COMMON_TEST

#include <stdio.h>
#include <string.h>
#include <stdlib.h>
#include <stdint.h>

#undef NDEBUG
#include <assert.h>

fifo_t(u8u8, uint8_t, uint8_t, static);
fifo_d(u8u8, static);

fifo_t(u16u8, uint16_t, uint8_t);
fifo_d(u16u8);

fifo_t(u16u16, uint16_t, uint16_t);
fifo_d(u16u16);

int main(void) {
  {
    fifo_inst(u8u8, fifo, 111, static);

    assert(0 == fifo_fn(u8u8, fill, &fifo));
    assert(111 == fifo_fn(u8u8, free, &fifo));
    
    assert(1 == fifo_fn(u8u8, push, &fifo, 18));

    assert(1 == fifo_fn(u8u8, fill, &fifo));
    assert(110 == fifo_fn(u8u8, free, &fifo));

    uint8_t val;
    assert(1 == fifo_fn(u8u8, shift, &fifo, &val));

    assert(0 == fifo_fn(u8u8, fill, &fifo));
    assert(111 == fifo_fn(u8u8, free, &fifo));
    
    assert(18 == val);
    
    assert(1 == fifo_fn(u8u8, push, &fifo, 56));
    fifo_fn(u8u8, pushf, &fifo, 60);
    fifo_fn(u8u8, init, &fifo);
    
    assert(0 == fifo_fn(u8u8, fill, &fifo));
    assert(111 == fifo_fn(u8u8, free, &fifo));

    int i;
    for (i = 0; i < 1000; i++) {
      assert(i == fifo_fn(u8u8, fill, &fifo));
      assert(111 - i == fifo_fn(u8u8, free, &fifo));
      if (!fifo_fn(u8u8, push, &fifo, i)) {
        break;
      }
    }
    assert(111 == i);
    assert(111 == fifo_fn(u8u8, fill, &fifo));
    assert(0 == fifo_fn(u8u8, free, &fifo));

    val = 123;
    assert(1 == fifo_fn(u8u8, first, &fifo, &val));
    assert(0 == val);
    fifo_fn(u8u8, pushf, &fifo, val);
    
    assert(111 == fifo_fn(u8u8, fill, &fifo));
    assert(0 == fifo_fn(u8u8, free, &fifo));

    assert(1 == fifo_fn(u8u8, first, &fifo, &val));
    assert(1 == val);
    fifo_fn(u8u8, pushf, &fifo, val);

    for (i = 0; i < 1000; i++) {
      assert(111 - i == fifo_fn(u8u8, fill, &fifo));
      assert(i == fifo_fn(u8u8, free, &fifo));
      if (!fifo_fn(u8u8, shift, &fifo, &val)) {
        break;
      }
      assert(i < 109 ? (i + 2) : (i - 109) == val);
    }
    assert(111 == i);
    assert(0 == fifo_fn(u8u8, fill, &fifo));
    assert(111 == fifo_fn(u8u8, free, &fifo));
  }

  {
    fifo_def(u16u8, *fifo) = fifo_new(u16u8, 255);
    
    assert(255 == fifo->size);
    assert(0 == fifo_fn(u16u8, fill, fifo));
    assert(255 == fifo_fn(u16u8, free, fifo));
    
    int usage = 0;
    
    for (; usage < fifo->size; ) {
      if (random() * 3 / RAND_MAX) {
        usage ++;
        assert(1 == fifo_fn(u16u8, push, fifo, usage));
      } else if (usage > 0) {
        usage --;
        assert(1 == fifo_fn(u16u8, shift, fifo, NULL));
      }
      assert(usage == fifo_fn(u16u8, fill, fifo));
    }

    for (; usage > 0; ) {
      if (!(random() * 3 / RAND_MAX) && usage < fifo->size) {
        usage ++;
        assert(1 == fifo_fn(u16u8, push, fifo, usage));
      } else if (usage > 0) {
        usage --;
        assert(1 == fifo_fn(u16u8, shift, fifo, NULL));
      }
      assert(usage == fifo_fn(u16u8, fill, fifo));
    }
    
    fifo_del(fifo);
  }

  {
    fifo_def(u16u16, *fifo) = fifo_new(u16u16, 54321);
    
    assert(54321 == fifo->size);
    assert(0 == fifo_fn(u16u16, fill, fifo));
    assert(54321 == fifo_fn(u16u16, free, fifo));
    
    int usage = 0;
    
    for (; usage < fifo->size; ) {
      if (random() * 3 / RAND_MAX) {
        usage ++;
        assert(1 == fifo_fn(u16u16, push, fifo, usage));
      } else if (usage > 0) {
        usage --;
        assert(1 == fifo_fn(u16u16, shift, fifo, NULL));
      }
      assert(usage == fifo_fn(u16u16, fill, fifo));
    }

    for (; usage > 0; ) {
      if (!(random() * 3 / RAND_MAX) && usage < fifo->size) {
        usage ++;
        assert(1 == fifo_fn(u16u16, push, fifo, usage));
      } else if (usage > 0) {
        usage --;
        assert(1 == fifo_fn(u16u16, shift, fifo, NULL));
      }
      assert(usage == fifo_fn(u16u16, fill, fifo));
    }
    
    fifo_del(fifo);
  }
  
  return 0;
}

#endif
