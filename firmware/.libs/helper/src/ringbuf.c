#include <stddef.h>

#include "ringbuf.h"

/*
 * [....<:::::::>.......]
 * [::::>.......<::::|..]
 *
 * [ - 0 (the beginning of buffer)
 * ] - size (the end of buffer)
 * < - rpos (read position)
 * > - wpos (write position)
 * | - rlim (read data limit)
 * . - free space
 * : - filled data
 * + - new data (which may be written)
 */

void ringbuf_reset(const ringbuf_config_t *rbc) {
  ringbuf_state_t *rbs = rbc->state;
  
  rbs->wpos = 0;
  rbs->wlen = 0;
  rbs->rpos = 0;
  rbs->rlim = rbc->size;
}

void *ringbuf_alloc(const ringbuf_config_t *rbc, ringbuf_size_t len) {
  ringbuf_state_t *rbs = rbc->state;
  
  if (rbs->wpos < rbs->rpos ||
      (rbs->wpos == rbs->rpos &&
       rbs->wlen > 0)) {
    /* [::::>.......<::::|..] */
    /* wpos before or match to rpos */
    if (len > rbs->rpos - rbs->wpos) {
      /* [::::>++++++<++:::|..] */
      /* we haven't free space of required length in buffer at all */
      return NULL;
    }
    /* [::::>+++++...<::::|..] */
    /* we have enough free space in buffer */
  } else {
    /* [....<:::::::>.......] */
    /* wpos after rpos */
    if (len > rbc->size - rbs->wpos) {
      /* [.....<:::::::>++++++]++ */
      /* we haven't free space of required length at the end of buffer */
      if (len > rbs->rpos) {
        /* [+++++<++:::::>.......] */
        /* we haven't continuous free space of required length in buffer */
        return NULL;
      }
      /* [+++++...<:::::::>|....] */
      /* we have enough free space at beginning of buffer */
      return rbc->data;
    } else {
      /* [.....<:::::::>+++++..|] */
      /* we have enough free space at end of buffer */
    }
  }
  
  return rbc->data + rbs->wpos;
}

void ringbuf_write(const ringbuf_config_t *rbc, ringbuf_size_t len) {
  ringbuf_state_t *rbs = rbc->state;

  if (rbs->wpos < rbs->rpos ||
      (rbs->wpos == rbs->rpos &&
       rbs->wlen > 0)) {
    /* [::::>.......<::::|..] */
    /* wpos before or match to rpos */
    if (len > rbs->rpos - rbs->wpos) {
      /* [::::>++++++<++:::|..] */
      /* we haven't free space of required length in buffer at all */
      return;
    }
    /* [::::>+++++...<::::|..] */
    /* we have enough free space in buffer */
  } else {
    /* [....<:::::::>.......] */
    /* wpos after rpos */
    if (len > rbc->size - rbs->wpos) {
      /* [.....<:::::::>++++++]++ */
      /* we haven't free space of required length at the end of buffer */
      if (len > rbs->rpos) {
        /* [+++++<++:::::>.......] */
        /* we haven't continuous free space of required length in buffer */
        return;
      }
      /* [+++++...<:::::::>|....] */
      /* we have enough free space at beginning of buffer */
      rbs->rlim = rbs->wpos;
      rbs->wpos = 0;
    } else {
      /* [.....<:::::::>+++++..|] */
      /* we have enough free space at end of buffer */
      rbs->rlim = rbc->size;
    }
  }
  
  rbs->wpos += len;
  rbs->wlen += len;
  
  if (rbs->wpos >= rbs->rlim) {
    rbs->wpos = 0;
  }
}

const void *ringbuf_read(const ringbuf_config_t *rbc, ringbuf_size_t *len) {
  ringbuf_state_t *rbs = rbc->state;
  
  if (rbs->wlen == 0) {
    *len = 0;
    return NULL;
  }
  
  if (rbs->rpos >= rbs->rlim) {
    rbs->rpos = 0;
  }
  
  if (rbs->wpos <= rbs->rpos) {
    /* [::::>.......<::::|..] */
    *len = rbs->rlim - rbs->rpos;
  } else {
    /* [....<:::::::>.......] */
    *len = rbs->wpos - rbs->rpos;
  }
  
  return rbc->data + rbs->rpos;
}

void ringbuf_seek(const ringbuf_config_t *rbc, ringbuf_size_t len) {
  ringbuf_state_t *rbs = rbc->state;
  
  rbs->rpos += len;
  rbs->wlen -= len;
  /*
  if (rbs->rpos >= rbs->rlim) {
    rbs->rpos = 0;
  }
  */
}

#ifdef COMMON_TEST

#undef NDEBUG
#include <assert.h>

int main(void) {
  ringbuf_static(buf, 100);

  ringbuf_size_t len;

  /* [<>......100.....|] */
  
  assert(ringbuf_alloc(&buf, 99) == buf_data);
  assert(ringbuf_alloc(&buf, 100) == buf_data);
  assert(ringbuf_alloc(&buf, 101) == NULL);
  
  assert(({
        ringbuf_alloc(&buf, 55);
        ringbuf_write(&buf, 55);
        /* [<:::55:::>...45...|] */
        buf_state.wpos == 55 && buf_state.wlen == 55;
      }));
  
  assert(({
        ringbuf_read(&buf, &len);
        /* [<:::55:::>...45...|] */
        len == 55 && buf_state.rlim == buf.size;
      }));

  assert(({
        ringbuf_alloc(&buf, 40);
        ringbuf_write(&buf, 40);
        /* [<:::95:::>...5...|] */
        buf_state.wpos == 55 + 40 && buf_state.wlen == 55 + 40;
      }));
  
  assert(ringbuf_alloc(&buf, 10) == NULL);

  assert(({
        ringbuf_read(&buf, &len);
        /* [<:::95:::>...5...|] */
        len == 55 + 40;
      }));
  
  assert(({
        ringbuf_seek(&buf, 45);
        /* [...45...<:::50:::>...5...|] */
        buf_state.rpos == 45;
      }));

  assert(({
        ringbuf_alloc(&buf, 3);
        ringbuf_write(&buf, 3);
        /* [...45...<:::53:::>...2...|] */
        buf_state.wpos == 98 && buf_state.wlen == 53;
      }));
  
  assert(({
        ringbuf_alloc(&buf, 40);
        ringbuf_write(&buf, 40);
        /* [:::40:::>...5...<:::53:::|..2..] */
        buf_state.wpos == 40 && buf_state.wlen == 53 + 40 && buf_state.rlim == 98;
      }));
  
  assert(({
        ringbuf_read(&buf, &len);
        /* [:::40:::>...5...<:::53:::|..2..] */
        len == 53;
      }));
  
  assert(({
        ringbuf_seek(&buf, 53);
        /* [<:::40:::>...58...|..2..] */
        buf_state.rpos == 0 && buf_state.wlen == 40;
      }));

  assert(({
        ringbuf_read(&buf, &len);
        /* [<:::40:::>...58...|..2..] */
        len == 40;
      }));

  assert(({
        ringbuf_seek(&buf, 40);
        /* [...40...<>...58...|..2..] */
        len == 40 && buf_state.wlen == 0;
      }));

  assert(({
        ringbuf_read(&buf, &len);
        /* [...40...<>...58...|..2..] */
        len == 0;
      }));

  /* [...40...<>...58...|..2..] */
  assert(buf_state.rpos == 40 && buf_state.wpos == 40);
  
  assert(({
        ringbuf_alloc(&buf, 60);
        /* [...40...<>...60...|] */
        buf_state.rlim == buf.size;
      }));
  
  assert(({
        ringbuf_write(&buf, 60);
        /* [>...40...<:::60:::] */
        buf_state.wpos == 0 && buf_state.wlen == 60;
      }));

  assert(({
        ringbuf_alloc(&buf, 40);
        /* [>...40...<:::60:::] */
        buf_state.rlim == buf.size;
      }));
  
  assert(({
        ringbuf_write(&buf, 40);
        /* [:::40:::><:::60:::] */
        buf_state.wpos == 40 && buf_state.wlen == 100;
      }));

  assert(({
        ringbuf_read(&buf, &len);
        /* [:::40:::><:::60:::] */
        len == 60;
      }));
  
  assert(ringbuf_alloc(&buf, 1) == NULL);
  
  return 0;
}

#endif
