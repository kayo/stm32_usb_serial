#include "pool.h"

#ifdef COMMON_TEST

#include <stdio.h>
#include <string.h>
#include <stdlib.h>

#undef NDEBUG
#include <assert.h>

pool_t(u8u8, uint8_t, uint8_t, static);
pool_d(u8u8, static);

pool_t(u8u16, uint8_t, uint16_t);
pool_d(u8u16);

/*
pool_t(u16u8, uint16_t, uint8_t, static);
pool_d(u16u8, static);

pool_t(u16u16, uint16_t, uint16_t, static);
pool_d(u16u16, static);
*/

#ifdef PRINT_BITFIELD
void print_bitfield(const uint8_t *ptr, uint16_t len) {
  uint8_t i = 0;
  for (; i < len; i++) {
    int8_t n = 0;
    for (; n < 8; n++) {
      printf((ptr[i] & (1 << n)) ? "*" : ".");
    }
    printf(i < len - 1 ? " " : "\n");
  }
}
#else
#define print_bitfield(...)
#endif

int main(void) {
  {
    pool_inst(u8u8, pool, 10, 100, static);
    
    assert(2 == sizeof(pool));
    assert(10 == pool.chunk);
    assert(100 == pool.count);
    
    assert(2 == &pool.data[0] - &pool.chunk);
    assert(0 == pool_fn(u8u8, usage, &pool));
    
    assert(13 == pool_usage_size(100));
    assert(1000 == pool_chunk_size(10, 100));
    
    assert(&pool.data[13+10*0] == pool_fn(u8u8, pointer, &pool, 0));
    assert(&pool.data[13+10*1] == pool_fn(u8u8, pointer, &pool, 1));
    assert(&pool.data[13+10*13] == pool_fn(u8u8, pointer, &pool, 13));

    assert(0 == pool_fn(u8u8, index, &pool, &pool.data[13+10*0]));
    assert(1 == pool_fn(u8u8, index, &pool, &pool.data[13+10*1]));
    assert(13 == pool_fn(u8u8, index, &pool, &pool.data[13+10*13]));
    assert(0 == pool_fn(u8u8, index, &pool, &pool.data[13+10*0+1]));
    assert(1 == pool_fn(u8u8, index, &pool, &pool.data[13+10*1+5]));
    assert(13 == pool_fn(u8u8, index, &pool, &pool.data[13+10*13+9]));
    
    uint8_t idx = ~0;
    assert(0 == pool.data[0]);
    
    assert(1 == pool_fn(u8u8, alloc, &pool, &idx));
    assert(0 == idx);
    assert((1 << 0) == pool.data[0]);

    pool_fn(u8u8, free, &pool, idx);
    assert(0 == pool.data[0]);

    assert(1 == pool_fn(u8u8, alloc, &pool, &idx));
    assert(0 == idx);
    assert((1 << 0) == pool.data[0]);
    
    pool_fn(u8u8, init, &pool);
    assert(0 == pool.data[0]);
    
    int i;
    for (i = 0; i < 1000; i++) {
      uint8_t idx;
      if (1 != pool_fn(u8u8, alloc, &pool, &idx)) {
        break;
      }
    }
    assert(i == 100);
    {
      static const uint8_t f[] = { [0 ... 11] = 0xff, [12] = 0x0f };
      assert(0 == memcmp(f, pool.data, sizeof(f)));
    }
    assert(100 == pool_fn(u8u8, usage, &pool));
    
    for (i = 0; i < 100; i++) {
      pool_fn(u8u8, free, &pool, i);
    }
    assert(i == 100);
    {
      static const uint8_t f[] = { [0 ... 12] = 0x0 };
      assert(0 == memcmp(f, pool.data, sizeof(f)));
    }
    assert(0 == pool_fn(u8u8, usage, &pool));

    {
      uint8_t usage = 0;
      uint8_t i;
      uint8_t idx[100] = { [0 ... 99] = ~0 };
      
      for (; usage < pool.count; ) {
        if (random() * 3 / RAND_MAX) {
          usage ++;
          for (i = 0; i < pool.count; i++) {
            if (idx[i] == (uint8_t)~0) {
              assert(1 == pool_fn(u8u8, alloc, &pool, &idx[i]));
              break;
            }
          }
        } else if (usage > 0) {
          usage --;
          for (i = 0; i < pool.count; i++) {
            if (idx[i] != (uint8_t)~0) {
              pool_fn(u8u8, free, &pool, idx[i]);
              idx[i] = (uint8_t)~0;
              break;
            }
          }
        }
        print_bitfield(pool.data, pool_usage_size(pool.count));
        assert(usage == pool_fn(u8u8, usage, &pool));
      }
      
      for (; usage > 0; ) {
        if (!(random() * 3 / RAND_MAX) && usage < pool.count) {
          usage ++;
          for (i = 0; i < pool.count; i++) {
            if (idx[i] == (uint8_t)~0) {
              assert(1 == pool_fn(u8u8, alloc, &pool, &idx[i]));
              break;
            }
          }
        } else {
          usage --;
          for (i = 0; i < pool.count; i++) {
            if (idx[i] != (uint8_t)~0) {
              pool_fn(u8u8, free, &pool, idx[i]);
              idx[i] = (uint8_t)~0;
              break;
            }
          }
        }
        print_bitfield(pool.data, pool_usage_size(pool.count));
        assert(usage == pool_fn(u8u8, usage, &pool));
      }
    }
  }
  
  {
    pool_def(u8u16, *pool) = pool_new(u8u16, 65, 65535);
    
    assert(4 == sizeof(*pool));
    assert(65 == pool->chunk);
    assert(65535 == pool->count);

    int i;
    for (i = 0; i < 100000; i++) {
      uint16_t idx;
      assert(i == pool_fn(u8u16, usage, pool));
      if (1 != pool_fn(u8u16, alloc, pool, &idx)) {
        break;
      }
    }
    assert(i == 65535);
    assert(i == pool_fn(u8u16, usage, pool));
    {
      static const uint8_t f[] = { [0 ... 8190] = 0xff, [8191] = 0x7f };
      assert(0 == memcmp(f, pool->data, sizeof(f)));
    }

    for (; i > 0; i--) {
      assert(i == pool_fn(u8u16, usage, pool));
      pool_fn(u8u16, free, pool, i - 1);
    }
    assert(0 == pool_fn(u8u16, usage, pool));

    pool_del(pool);
  }
  
  return 0;
}

#endif
