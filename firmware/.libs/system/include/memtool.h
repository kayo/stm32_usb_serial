/**
 * @file memtool.h
 * @brief Memory consumption measurement tool
 *
 * @section Memory tool
 */
#ifndef MEMTOOL_H
#define MEMTOOL_H "memtool.h"

#include <stddef.h>

#ifndef MEMTOOL_CHECK
/**
 * @brief Enable memory checking tool
 */
#define MEMTOOL_CHECK 0
#endif

#ifndef MEMTOOL_PREFILL
/**
 * @brief The memory prefill value
 *
 * The RAM will be filled using this value to determine real memory usage.
 * By default we use 0x55, but you can simply redefine (for ex. -DMEMTOOL_PREFILL=0xaa)
 */
#define MEMTOOL_PREFILL 0x55
#endif

#ifndef MEMTOOL_FREELEN
/**
 * @brief The free memory length (bytes)
 *
 * The number of bytes, which filled using @p MEMTOOL_PREFILL, to determine unused memory.
 * By default we use 8 bytes, but you can simply redefine (for ex. -DMEMTOOL_FREELEN=4)
 */
#define MEMTOOL_FREELEN 8
#endif

#if MEMTOOL_FREELEN < 1
#error "The MEMTOOL_FREELEN will be greater than 0."
#endif

#if MEMTOOL_FREELEN < 4
#warning "The MEMTOOL_FREELEN too low."
#endif

#if MEMTOOL_FREELEN > 32
#warning "The MEMTOOL_FREELEN too high."
#endif

/**
 * @brief The memory usage info
 */
typedef struct {
  /**
   * @brief Max used heap (bytes)
   *
   * The maximum size of heap memory, which been used by application.
   */
  int heap_usage;
  /**
   * @brief Max used stack (bytes)
   *
   * The maximum size of stack, which been required by application.
   */
  int stack_usage;
} memtool_info_t;

/**
 * @brief Fill memory using pattern
 *
 * You need call this function before init code of your application to prepare memory.
 * The currently unused memory will be filled using @p MEMTOOL_PREFILL value.
 */
void memtool_prefill_real(void);

/**
 * @brief Measure actual memory usage
 *
 * You may use this function to get real used memory in any reasonable place.
 * Usually you need call this after several full runs of mainloop of application.
 * The free memory detection uses @p MEMTOOL_FREELEN to determine unused memory.
 * Maybe you need to  this value if detection inaccurate.
 *
 * @param res The pointer to result
 */
void memtool_measure_real(memtool_info_t *res);

#if MEMTOOL_CHECK
#define memtool_infodef(name, ...) __VA_ARGS__ memtool_info_t name
#define memtool_prefill memtool_prefill_real
#define memtool_measure memtool_measure_real
#else
#define memtool_infodef(name, ...)
#define memtool_prefill()
#define memtool_measure(res)
#endif

#endif /* MEMTOOL_H */
