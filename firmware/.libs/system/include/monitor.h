#ifndef __MONITOR_H__
#define __MONITOR_H__

#include "macro.h"

#define MONITOR_DRIVER_cycle 0
#define MONITOR_DRIVER_sleep 1
#define MONITOR_DRIVER_mutex 2

#define MONITOR_DRIVER_IS(x) (_CAT2(MONITOR_DRIVER_, x) == _CAT2(MONITOR_DRIVER_, MONITOR_DRIVER))

#if MONITOR_DRIVER_IS(sleep)
#include "sleep.h"
#if MONITOR_DEBUG
#include <libopencm3/stm32/dbgmcu.h>
#endif
#elif MONITOR_DRIVER_IS(mutex)
#include <libopencm3/cm3/sync.h>
#endif

#if MONITOR_USE_SYSTICK
#define monitor_systick_handler(...)     \
  void sys_tick_handler(void) {          \
    /* Wake-up monitor (Main thread) */  \
    __VA_ARGS__;                         \
  }
#else
#define monitor_systick_handler(...)
#endif

#if MONITOR_DRIVER_IS(sleep)
#define monitor_def()                              \
  monitor_systick_handler(disable_sleep_on_exit())
#elif MONITOR_DRIVER_IS(mutex)
#define monitor_def()                                          \
  static volatile mutex_t _monitor_tick_mutex;                 \
  monitor_systick_handler(mutex_unlock(&_monitor_tick_mutex))
#else /* !MONITOR_USE_SLEEP && !MONITOR_USE_MUTEX */
#define monitor_def()                                  \
  static volatile bool _monitor_tick_fired = false;    \
  monitor_systick_handler(_monitor_tick_fired = true)
#endif

#if MONITOR_DRIVER_IS(mutex)
#define monitor_init() mutex_lock(&_monitor_tick_mutex);
#elif MONITOR_DRIVER_IS(sleep) && MONITOR_DEBUG
/* Wo be able to debug in stop mode */
#define monitor_init() DBGMCU_CR |= DBGMCU_CR_SLEEP | DBGMCU_CR_STOP;
#else /* !MONITOR_USE_MUTEX && !MONITOR_USE_SLEEP */
#define monitor_init()
#endif

#define monitor_done()

#if MONITOR_DRIVER_IS(sleep)
#define monitor_wait()    \
  enable_sleep_on_exit(); \
  wait_for_interrupt()
#elif MONITOR_DRIVER_IS(mutex)
#define monitor_wait()              \
  mutex_lock(&_monitor_tick_mutex)
#else
#define monitor_wait() {            \
    for (; !_monitor_tick_fired; )  \
      asm volatile("nop");          \
    _monitor_tick_fired = false;    \
  }
#endif

#endif /* __MONITOR_H__ */
