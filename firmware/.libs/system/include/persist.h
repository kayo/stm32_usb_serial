#ifndef __PERSIST_H__
#define __PERSIST_H__ "persist.h"

#include <stdbool.h>

#define persist_load(ptr) persist_load_real((uint8_t*)(ptr), sizeof(*(ptr)))
#define persist_save(ptr) persist_save_real((const uint8_t*)(ptr), sizeof(*(ptr)))

bool persist_load_real(uint8_t *ptr, uint8_t len);
bool persist_save_real(const uint8_t *ptr, uint8_t len);

#endif /* __PERSIST_H__ */
