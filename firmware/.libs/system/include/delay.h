#ifndef __DELAY_H__
#define __DELAY_H__

#include <stdint.h>

typedef uint32_t delay_t;

void delay_init(void);
#define delay_done()

delay_t delay_snap(void);
void delay_wait(delay_t sc, delay_t dt);
delay_t delay_meas(delay_t sc);

static inline void
delay_us(delay_t us) {
  delay_t cc = delay_snap();
  delay_wait(cc, us * MCU_FREQUENCY_MHZ);
}

static inline void
delay_ms(delay_t ms) {
  delay_us(ms * 1000);
}

#endif /* __DELAY_H__ */
