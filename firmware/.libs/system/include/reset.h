#ifndef RESET_H
#define RESET_H "reset.h"

#include <libopencm3/cm3/scb.h>

static inline void data_memory_barrier(void) {
  asm volatile ("dmb");
}

static inline void data_sync_barrier(void) {
  asm volatile ("dsb");
}

static inline void do_system_reset(void) {
  SCB_AIRCR = SCB_AIRCR_VECTKEY | SCB_AIRCR_SYSRESETREQ;
  data_sync_barrier();
  for (;;);
}

#endif /* RESET_H */
