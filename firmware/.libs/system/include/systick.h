#ifndef __SYSTICK_H__
#define __SYSTICK_H__

void systick_init(void);
void systick_done(void);

typedef uint32_t systick_t;
systick_t systick_read(void);

#endif /* __SYSTICK_H__ */
