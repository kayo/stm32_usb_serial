#include <libopencm3/cm3/nvic.h>
#include <libopencm3/cm3/systick.h>

#include "systick.h"
#include "macro.h"
#include "config.h"

#if SYSTICK_FREQUENCY_KHZ > 0
#define SYSTICK_CLKSRC STK_CSR_CLKSOURCE_EXT
#define SYSTICK_PERIOD ((SYSTICK_FREQUENCY_KHZ)*(SYSTICK_PERIOD_MS))
#else /* SYSTICK_FREQUENCY_KHZ == 0 */
/* Use internal clock source */
#if SYSTICK_DIVIDER == 1
#define SYSTICK_CLKSRC STK_CSR_CLKSOURCE_AHB
#else
#define SYSTICK_CLKSRC _CAT2(STK_CSR_CLKSOURCE_AHB_DIV, SYSTICK_DIVIDER)
#endif
#define SYSTICK_PERIOD ((MCU_FREQUENCY_MHZ)*1000/(SYSTICK_DIVIDER)*(SYSTICK_PERIOD_MS))
#endif /* SYSTICK_FREQUENCY_KHZ > 0 */

#if SYSTICK_PERIOD >= (1<<24)
#error "systick period too big"
#endif

void systick_init(void) {
  /* Enable systick interrupt */
	nvic_enable_irq(NVIC_SYSTICK_IRQ);
  
  /* 72MHz / 8 = 9MHz counts per second */
	systick_set_clocksource(SYSTICK_CLKSRC);
  
  /* SYSTICK_PERIOD = systick_interval * 9MHz */
	systick_set_reload(SYSTICK_PERIOD-1);
  
  /* Enable systick interrupt */
	systick_interrupt_enable();
  
  /* Enable systick counter for monitor step function */
	systick_counter_enable();
}

void systick_done(void) {
  /* Disable sys tick counter for monitor step function */
  systick_counter_disable();
  systick_interrupt_disable();
  
  /* Disable systick interrupt */
	nvic_disable_irq(NVIC_SYSTICK_IRQ);
}

systick_t systick_read(void) {
	return systick_get_value();
}
