#include "memtool.h"

#include <string.h>

extern char end; /* Set by linker. */
extern unsigned int _stack;
#define stack ((char*)(&_stack))

static volatile void *get_msp(void) {
  void *result;
  
  asm volatile ("mrs %0, msp\n\t" 
                "mov r0, %0 \n\t"
                "bx  lr     \n\t" : "=r" (result));
  
  return result;
}

void memtool_prefill_real(void) {
  const char *msp = (const char*)get_msp();
  memset(&end, MEMTOOL_PREFILL, msp - &end);
}

static const char pattern[] = {
  [0 ... MEMTOOL_FREELEN-1] = MEMTOOL_PREFILL
};

void memtool_measure_real(memtool_info_t *res) {
  const char *msp = (const char*)get_msp();
  const char *ptr;
  
  /* seek to beginning of pattern */
  ptr = memmem(&end, msp - &end,
               pattern, sizeof(pattern));

  if (ptr) {
    res->heap_usage = ptr - &end;
  
    /* seek to end of pattern */
    ptr = memrchr(ptr, MEMTOOL_PREFILL, msp - ptr);
  
    res->stack_usage = stack - ptr;
  } else {
    res->heap_usage = -1;
    res->stack_usage = -1;
  }
}
