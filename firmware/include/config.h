/* common config */
#define led_red C,0,0,od /* port,pad,level,mode */
#define led_green A,5,0,od /* port,pad,level,mode */
#define led_yellow A,7,0,od /* port,pad,level,mode */
#define leds_list red,green,yellow

#define uart0_dev 1,(A,10),(A,9)      /* port,rx(port,pad),tx(port,pad) */
#define uart0_dma 1,5,4               /* dma,rx_channel,tx_channel */
#define uart0_rs485 (C,11),re,(C,12)  /* de(port,pad)[,re,re(port,pad)] */

#define uart1_dev 2,(A,3),(A,2)       /* port,rx(port,pad),tx(port,pad) */
#define uart1_dma 1,6,7               /* dma,rx_channel,tx_channel */
#define uart1_rs485 (A,0),re,(A,1)    /* de(port,pad)[,re,re(port,pad)] */

#define uart2_dev 3,(B,11),(B,10)     /* port,rx(port,pad),tx(port,pad) */
#define uart2_dma 1,3,2               /* dma,rx_channel,tx_channel */
#define uart2_rs485 (B,1),re,(B,14)   /* de(port,pad)[,re,re(port,pad)] */

#define usb_lines (A,11),(A,12) /* d-(port,pad),d+(port,pad) */
#define usb_detect C,8,1 /* port,pad,level */
#define usb_connect C,9,1 /* port,pad,level */

#define usb_acm0 1,2 /* bulk end-point,interrupt end-point */
#define usb_acm1 3,4 /* bulk end-point,interrupt end-point */
#define usb_acm2 5,6 /* bulk end-point,interrupt end-point */

#define usb_hid 7 /* bulk end-point (used for rs485 function set) */
