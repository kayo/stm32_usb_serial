#ifndef __IO_H__
#define __IO_H__

#include <stdint.h>
#include <stdbool.h>

#include "fifo.h"
#include "pool.h"

#if IO_QUEUE_LENGTH < 256
typedef uint8_t io_idx_t;
#elif IO_QUEUE_LENGTH < 65536
typedef uint16_t io_idx_t;
#else
typedef uint32_t io_idx_t;
#endif

typedef uint8_t io_len_t;
typedef uint8_t io_val_t;

/* I/O chunks queue */
fifo_t(io, io_val_t, io_idx_t);

/* I/O chunks pool */
pool_t(io, io_len_t, io_idx_t);
pool_def(io, io_pool, extern);

#define io_nm(name) io_##name
#define io_fn(name, ...) io_nm(name)(__VA_ARGS__)

void io_reset(fifo_io_t *fifo);

static inline io_idx_t io_fill(fifo_io_t *fifo) {
  return fifo_io_fill(fifo);
}

static inline io_idx_t io_free(fifo_io_t *fifo) {
  return fifo_io_free(fifo);
}

bool io_put_req(fifo_io_t *fifo, io_idx_t *idx, void **data, io_len_t *size);
void io_put_end(fifo_io_t *fifo, io_idx_t index, io_len_t size);
static inline void io_put_byte(void **data, io_len_t *size, uint8_t val) {
  io_val_t **ptr = (io_val_t **)data;
  (*ptr)[-1] += 1;
  (*ptr)[0] = val;
  (*ptr) ++;
  size --;
}

bool io_get_req(fifo_io_t *fifo, io_idx_t *idx, const void **data, io_len_t *size);
void io_get_end(fifo_io_t *fifo, io_idx_t index);

#define io_queue(name, size)         \
  fifo_inst(io, name, size, static)

#endif /* __IO_H__ */
