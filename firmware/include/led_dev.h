#ifndef __LED_H__
#define __LED_H__

#include "macro.h"

#define fast_gpio_set(port, pads) (GPIO_BSRR(port) = (pads))
#define fast_gpio_clear(port, pads) (GPIO_BSRR(port) = ((pads) << 16))
#define fast_gpio_state(port, pads) (GPIO_ODR(port) & (pads))

#define LED_ON_0 fast_gpio_clear
#define LED_OFF_0 fast_gpio_set
#define LED_ON_1 fast_gpio_set
#define LED_OFF_1 fast_gpio_clear
#define LED_ST_0 !fast_gpio_state
#define LED_ST_1 fast_gpio_state

#ifdef STM32F1
#define LED_OUTPUT_pp GPIO_CNF_OUTPUT_PUSHPULL
#define LED_OUTPUT_od GPIO_CNF_OUTPUT_OPENDRAIN
#endif

#define led_fn(id, fn, ...) _CAT4(led_, id, _, fn)(__VA_ARGS__)
#define led_cf(id, n) _CAT2(_NTH, n)(_CAT2(led_, id))

#define led_on(id) led_fn(id, on)
#define led_off(id) led_fn(id, off)
#define led_get(id) led_fn(id, get)
#define led_init(id) led_fn(id, init)
#define led_done(id) led_fn(id, done)

#define led_def(id)                               \
  static inline void led_fn(id, on, void) {       \
    _CAT2(LED_ON_, led_cf(id, 2))                 \
      (_CAT2(GPIO, led_cf(id, 0)),                \
       _CAT2(GPIO, led_cf(id, 1)));               \
  }                                               \
  static inline void led_fn(id, off, void) {      \
    _CAT2(LED_OFF_, led_cf(id, 2))                \
      (_CAT2(GPIO, led_cf(id, 0)),                \
       _CAT2(GPIO, led_cf(id, 1)));               \
  }                                               \
  static inline char led_fn(id, get, void) {      \
    return _CAT2(LED_ST_, led_cf(id, 2))          \
      (_CAT2(GPIO, led_cf(id, 0)),                \
       _CAT2(GPIO, led_cf(id, 1)));               \
  }                                               \
  static inline void led_fn(id, init, void) {     \
    led_fn(id, off);                              \
    gpio_set_mode(_CAT2(GPIO, led_cf(id, 0)),     \
                  GPIO_MODE_OUTPUT_2_MHZ,         \
                  _CAT2(LED_OUTPUT_,              \
                        led_cf(id, 3)),           \
                  _CAT2(GPIO, led_cf(id, 1)));    \
  }                                               \
  static inline void led_fn(id, done, void) {     \
    gpio_set_mode(_CAT2(GPIO, led_cf(id, 0)),     \
                  GPIO_MODE_INPUT,                \
                  GPIO_CNF_INPUT_FLOAT,           \
                  _CAT2(GPIO, led_cf(id, 1)));    \
  }

#endif /* __LED_H__ */
