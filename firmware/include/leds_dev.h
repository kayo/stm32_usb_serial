#ifndef __LEDS_H__
#define __LEDS_H__

#include "hid_proto.h"

enum {
  /* states */
  led_turn_off = 0x1,
  led_turn_on = 0x2,
};

void leds_init(void);
void leds_done(void);
void leds_step(void);
void leds_set_mode(void);
void leds_conf(uint8_t led, uint8_t mode);
void leds_trig(uint8_t event, uint8_t state);

#define leds_off(event) leds_trig(_CAT2(led_evt_, event), led_turn_off)
#define leds_on(event) leds_trig(_CAT2(led_evt_, event), led_turn_on)
#define leds_onoff(event) leds_trig(_CAT2(led_evt_, event), led_turn_on | led_turn_off)

#endif /* __LEDS_H__ */
