#ifndef __USB_DEV_H__
#define __USB_DEV_H__

#include <libopencm3/usb/usbd.h>

extern usbd_device *usb_dev;

void usb_dev_init(void);
void usb_dev_done(void);

#endif /* __USB_DEV_H__ */
