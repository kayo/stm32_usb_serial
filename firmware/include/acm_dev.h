#ifndef __ACM_DEV_H__
#define __ACM_DEV_H__

#define ACM_IFACE_DEF(id)                 \
  {                                       \
    .num_altsetting = 1,                  \
    .iface_assoc = &acm_iface_assoc[id],  \
    .altsetting = &acm_comm_iface[id],    \
  },                                      \
  {                                       \
    .num_altsetting = 1,                  \
    .altsetting = &acm_data_iface[id],    \
  },

#ifdef usb_acm0
#define ACM0_COUNT 1
#define ACM0_IFACE ACM_IFACE_DEF(0)
#else
#define ACM0_IFACE
#define ACM0_COUNT 0
#endif

#ifdef usb_acm1
#define ACM1_COUNT 1
#define ACM1_IFACE ACM_IFACE_DEF(1)
#else
#define ACM1_COUNT 0
#define ACM1_IFACE
#endif

#ifdef usb_acm2
#define ACM2_COUNT 1
#define ACM2_IFACE ACM_IFACE_DEF(2)
#else
#define ACM2_COUNT 0
#define ACM2_IFACE
#endif

#define ACM_COUNT (ACM0_COUNT+ACM1_COUNT+ACM2_COUNT)
#define acm_ifaces ACM0_IFACE ACM1_IFACE ACM2_IFACE

extern const struct usb_interface_descriptor acm_comm_iface[];
extern const struct usb_interface_descriptor acm_data_iface[];
extern const struct usb_iface_assoc_descriptor acm_iface_assoc[];

void acm_dev_init(void);
void acm_dev_done(void);
void acm_set_rs485(void);
void acm_set_config(usbd_device *usbd_dev);

#if DBGLOG_MATCH(err)
void acm_log_poll(void);
#else
#define acm_log_poll()
#endif

#endif /* __ACM_DEV_H__ */
