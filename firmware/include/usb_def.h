#ifndef __USB_DEF_H__
#define __USB_DEF_H__

#define USB_CLASS_MISC 0xEF
#define USB_SUB_CLASS_COMMON 2
#define USB_PROT_IFACE_ASSOC 1

#define USB_REQ_IN(req) (((req)->bmRequestType & USB_REQ_TYPE_DIRECTION) == USB_REQ_TYPE_IN)
#define USB_REQ_OUT(req) (!USB_REQ_IN(req))

#ifndef __packed__
#define __packed__ __attribute__((packed))
#endif

#endif /* __USB_DEF_H__ */
