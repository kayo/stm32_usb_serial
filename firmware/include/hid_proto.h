#ifndef __HID_PROTO_H__
#define __HID_PROTO_H__

#include <stdint.h>

#define HID_USAGE_PAGE_USB_UART3 2, 0xff5e
#define HID_USAGE_USB_UART3 1, 0x01

#define HID_REPORT_REBOOT 0xa0
#define HID_USAGE_REBOOT 1, 0xf0

#define HID_REPORT_RS485_MODE 0x01
#define HID_USAGE_UART_ID(n) 1, (0xa0 + (n))

#define HID_REPORT_LEDS_MODE 0x02
#define HID_USAGE_LED_ID(n) 1, (0xb0 + (n))
#define HID_USAGE_LED_EVT 1, 0x20
#define HID_USAGE_LED_ACT 1, 0x21

#ifndef __packed__
#define __packed__ __attribute__((packed))
#endif

#include "setting.h"

struct hid_report_reboot {
  uint8_t report_id;
  uint8_t __dummy;
} __packed__;

struct hid_report_uart_opts {
  uint8_t report_id;
  uart_opts_t uart;
} __packed__;

struct hid_report_leds_opts {
  uint8_t report_id;
  leds_opts_t leds;
} __packed__;

#endif /* __HID_PROTO_H__ */
