#ifndef __HID_DEV_H__
#define __HID_DEV_H__

#if CONFIG_THROUGH_HID
#define hid_ifaces                   \
  {                                  \
    .num_altsetting = 1,             \
    .altsetting = &hid_ctrl_iface,   \
  },

extern const struct usb_interface_descriptor hid_ctrl_iface;

void hid_set_config(usbd_device *usbd_dev);
#else /* CONFIG_THROUGH_HID */
#define hid_ifaces
#define hid_set_config(...)
#endif /* CONFIG_THROUGH_HID */

#endif /* __HID_DEV_H__ */
