#ifndef __EVENTS_H__
#define __EVENTS_H__

#include <stdint.h>

typedef uint8_t events_t;

enum {
  evt_system_reboot = 1 << 0,
};

#define set_event(evt) (events |= (evt))
#define has_event(evt) (events & (evt))
#define get_event(evt) (events &= ~(evt))

#define events_init() (events = 0)

extern events_t events;

#endif /* __EVENTS_H__ */
