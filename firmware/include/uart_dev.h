#ifndef __UART_DEV_H__
#define __UART_DEV_H__

#include <stdbool.h>
#include <stdint.h>
#include "macro.h"
#include "io.h"

typedef io_idx_t uart_idx_t;
typedef io_len_t uart_len_t;

#define uart_none io_none

enum {
  uart_stopbits_1 = 0,
  uart_stopbits_1p5 = 1,
  uart_stopbits_2 = 2,
};

enum {
  uart_parity_none = 0,
  uart_parity_odd = 1,
  uart_parity_even = 2,
};

enum {
  uart_receiver_on = 1 << 0,
  uart_transmitter_on = 1 << 1,
  
  uart_active_state = (uart_receiver_on |
                       uart_transmitter_on),
  
  uart_break_state = 1 << 2,

  uart_framing_error = 1 << 4,
  uart_parity_error = 1 << 5,
  uart_overrun_error = 1 << 6,
  
  uart_state_mask = (uart_break_state |
                     uart_overrun_error |
                     uart_parity_error |
                     uart_framing_error),
};

#define uart_nm(id, fn) _CAT4(uart, id, _, fn)
#define uart_fn(id, fn, ...) uart_nm(id, fn)(__VA_ARGS__)
#define uart_cf(id, k, n) _CAT2(_NTH, n)(_CAT(uart, id, _, k))

#define uart_def(id)                  \
  void uart_fn(id, init, void);       \
  void uart_fn(id, done, void);       \
  void uart_fn(id, on, void);         \
  void uart_fn(id, off, void);        \
  void uart_fn(id, st_handle, void);  \
  uint8_t uart_fn(id, status, void);  \
  void uart_fn(id, conf,              \
               uint32_t baudrate,     \
               uint8_t databits,      \
               uint8_t stopbits,      \
               uint8_t parity);       \
  void uart_fn(id, set_rs485,         \
               bool enable);          \
  void uart_fn(id, rx_handle, void);  \
  bool uart_fn(id, rx_request,        \
               uart_idx_t *idx,       \
               const void **ptr,      \
               uart_len_t *len);      \
  void uart_fn(id, rx_confirm,        \
               uart_idx_t idx);       \
  bool uart_fn(id, tx_request,        \
               uart_idx_t *idx,       \
               void **ptr);           \
  void uart_fn(id, tx_confirm,        \
               uart_idx_t idx,        \
               uart_len_t len)

#endif /* __UART_DEV_H__ */
