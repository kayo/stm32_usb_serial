#ifndef __CDC_DEF_H__
#define __CDC_DEF_H__

#include "usb_def.h"

#define USB_CDC_CALL_MGMT_OVER_DATA_IFACE     (1<<1)
#define USB_CDC_CALL_MGMT_HANDLES_ITSELF      (1<<0)
#define USB_CDC_CALL_MGMT_DOES_NOT_HANDLE     (0)

#define USB_CDC_ACM_SUP_NETWORK_CONNECTION    (1<<3)
#define USB_CDC_ACM_SUP_SEND_BREAK            (1<<2)
#define USB_CDC_ACM_SUP_LINE_CODING_AND_STATE (1<<1)
#define USB_CDC_ACM_SUP_COMM_FEATURE          (1<<0)

#define USB_CDC_LINE_STATE_ACTIVATE_CARRIER (1<<1)
#define USB_CDC_LINE_STATE_DTE_IS_PRESENT   (1<<0)

/* Requests */
#define USB_CDC_REQ_SET_COMM_FEATURE       0x02
#define USB_CDC_REQ_GET_COMM_FEATURE       0x03
#define USB_CDC_REQ_CLEAR_COMM_FEATURE     0x04
#define USB_CDC_REQ_SET_AUX_LINE_STATE     0x10
#define USB_CDC_REQ_SET_HOOK_STATE         0x11
#define USB_CDC_REQ_PULSE_SETUP            0x12
#define USB_CDC_REQ_SEND_PULSE             0x13
#define USB_CDC_REQ_SET_PULSE_TIME         0x14
#define USB_CDC_REQ_RING_AUX_JACK          0x15
/*#define USB_CDC_REQ_SET_LINE_CODING        0x20*/ /* already defined in cdc.h */
#define USB_CDC_REQ_GET_LINE_CODING        0x21
/*#define USB_CDC_REQ_SET_CONTROL_LINE_STATE 0x22*/ /* already defined in cdc.h */
#define USB_CDC_REQ_SEND_BREAK             0x23
#define USB_CDC_REQ_SET_RINGER_PARAMS      0x30
#define USB_CDC_REQ_GET_RINGER_PARAMS      0x31
#define USB_CDC_REQ_SET_OPERATION_PARAMS   0x32
#define USB_CDC_REQ_GET_OPERATION_PARAMS   0x33
#define USB_CDC_REQ_SET_LINE_PARAMS        0x34
#define USB_CDC_REQ_GET_LINE_PARAMS        0x35
#define USB_CDC_REQ_DIAL_DIGITS            0x36

/* Notifications */
#define USB_CDC_NOTIFY_NETWORK_CONNECTION  0x00
#define USB_CDC_NOTIFY_RESPONSE_AVAILABLE  0x01
#define USB_CDC_NOTIFY_AUX_JACK_HOOK_STATE 0x08
#define USB_CDC_NOTIFY_RING_DETECT         0x09
/*#define USB_CDC_NOTIFY_SERIAL_STATE        0x20*/ /* already defined in cdc.h */
#define USB_CDC_NOTIFY_LINE_STATE_CHANGE   0x23
#define USB_CDC_NOTIFY_CALL_STATE_CHANGE   0x28

#define USB_CDC_SERIAL_STATE_OVERRUN_ERROR (1<<6)
#define USB_CDC_SERIAL_STATE_PARITY_ERROR  (1<<5)
#define USB_CDC_SERIAL_STATE_FRAMING_ERROR (1<<4)
#define USB_CDC_SERIAL_STATE_RING_SIGNAL   (1<<3)
#define USB_CDC_SERIAL_STATE_BREAK_STATE   (1<<2)
#define USB_CDC_SERIAL_STATE_TX_CARRIER    (1<<1)
#define USB_CDC_SERIAL_STATE_RX_CARRIER    (1<<0)

struct usb_cdc_serial_state_notification {
  struct usb_cdc_notification req;
  struct {
    uint16_t bmSerialState;
  } __packed__ data;
} __packed__;

#endif /* __CDC_DEF_H__ */
