#ifndef __SETTING_H__
#define __SETTING_H__

#include <stdint.h>

#define bits_get(src, len, off) (((src) >> (off)) & ((1 << (len)) - 1))
#define bits_put(len, off, val) (((val) & ((1 << (len)) - 1)) << (off))
#define bits_set(dst, len, off, val) ((dst) = ((dst) & ~(((1 << (len)) - 1) << (off))) | bits_put(len, off, val))

enum {
  uart_name_a = 0,
  uart_name_b = 1,
  uart_name_c = 2,
};

enum {
  rs485_mode_off = 0,
  rs485_mode_on = 1,

  rs485_mode_min = 0,
  rs485_mode_max = 1,
  rs485_mode_len = 1,
};

#define rs485_mode_get(val, uart) bits_get(val, rs485_mode_len, uart)
#define rs485_mode_set(val, uart, mode) bits_set(val, rs485_mode_len, uart, mode ? 1 : 0)

typedef struct {
  uint8_t rs485;
} uart_opts_t;

enum {
  led_name_red = 0,
  led_name_green = 1,
  led_name_yellow = 2,
};

enum {
  /**
   * @brief Turn off the LED
   */
  led_evt_unused = 0x0,
  /**
   * @brief LED on when USB connection established
   */
  led_evt_connection = 0x1,
  /**
   * @brief LED indicate USB control requests
   */
  led_evt_control = 0x2,
  /**
   * @brief LED indicate ACM data transfer
   */
  led_evt_transfer = 0x3,
  /**
   * @brief LED indicate ACM data reception
   */
  led_evt_reception = 0x4,
  /**
   * @brief LED indicate ACM data transmission
   */
  led_evt_transmission = 0x5,
  /**
   * @brief LED indicate data transfer errors
   */
  led_evt_transfer_error = 0x6,
  
  led_evt_min = 0x0,
  led_evt_max = 0x6,
  led_evt_len = 4,
};

enum {
  /* actions */
  led_act_light,
  led_act_blink,
  led_act_flash,

  led_act_min = led_act_light,
  led_act_max = led_act_flash,
  led_act_len = 4,
};

#define led_evt_get(mode) bits_get(mode, 8 - led_evt_len, 0)
#define led_evt_set(mode, evt) bits_set(mode, 8 - led_evt_len, 0, evt)

#define led_act_get(mode) bits_get(mode, led_evt_len, 8 - led_evt_len)
#define led_act_set(mode, act) bits_set(mode, led_evt_len, 8 - led_evt_len, act)

#define led_mode_mk(evt, act) (bits_put(8 - led_evt_len, 0, evt) | bits_put(led_evt_len, 8 - led_evt_len, act))

typedef struct {
  uint8_t mode[3];
} leds_opts_t;

typedef struct {
  uart_opts_t uart;
  leds_opts_t leds;
} setting_t;

extern setting_t setting;

void setting_load(void);
void setting_save(void);

#endif /* __SETTING_H__ */
