#ifndef __OUTLOG_H__
#define __OUTLOG_H__

#include <stdio.h>

#define NL "\n"

#define log(fmt, ...) fprintf(stdout, fmt NL, ##__VA_ARGS__)
#define err(fmt, ...) fprintf(stderr, fmt NL, ##__VA_ARGS__)

#endif /* __OUTLOG_H__ */
