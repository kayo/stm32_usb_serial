#include <string.h>

#include "helper.h"
#include "outlog.h"
#include "hid_proto.h"
#include "hidapi.h"

#define strpcmp(str, cst) strncmp(str, cst, sizeof(cst)-1)

static const char *program = SOFTWARE_PROGRAM;
static const char const* version = SOFTWARE_VERSION;

static int help(void) {
  log("%s %s" NL, program, version);
  log("Usage: %s [device] <commands>" NL, program);
  log("Commands:");
  log("  list,l                   Show all HID devices");
  log("  get,g <param>            Get parameter value");
  log("  set,s <param> <value>    Set parameter value");
  
  return 0;
}

static int do_list(void) {
  struct hid_device_info *devs, *dev;
  
  devs = hid_enumerate(0x0, 0x0);
  
  for (dev = devs; dev != NULL; dev = dev->next) {
    /*
			if (dev->usage_page != HID_USAGE_PAGE_USB_UART3) {
      continue;
			}
    */
    
    log("%s [%04hx:%04hx] (0x%x,0x%x)",
        dev->path, dev->vendor_id, dev->product_id,
        dev->usage_page, dev->usage);
    
    if (dev->manufacturer_string != NULL &&
        dev->manufacturer_string[0] != '\0') {
      log("  Vendor:  %ls", dev->manufacturer_string);
    }
    
    if (dev->product_string != NULL &&
        dev->product_string[0] != '\0') {
      log("  Product: %ls", dev->product_string);
    }
    
    if (dev->serial_number != NULL &&
        dev->serial_number[0] != '\0') {
      log("  Serial:  %ls", dev->serial_number);
    }
  }
  
  hid_free_enumeration(devs);

  return 0;
}

static hid_device *device = NULL;

static int do_open(const char *path) {
  device = hid_open_path(path);
  
  if (device == NULL) {
    err("Unable to open device: %s", path);
    return 1;
  }
  
  return 0;
}

static void do_close(void) {
  if (device == NULL) {
    return;
  }
  
  hid_close(device);
  
  return;
}

typedef struct {
  enum {
    par_uart_rs485,
    par_led_evt,
    par_led_act,
  } type;
  union {
    struct {
      uint8_t id;
    } uart;
    struct {
      uint8_t id;
    } led;
  };
} par_info_t;

static int parse_par(const char *name,
                     par_info_t *param) {
  uint8_t pos = 0;
  
  if (0 == strpcmp(&name[pos], "uart")) {
    pos += 4;

    if (name[pos] == '.') { /* select UART by name */
      pos ++;

      if (0 == strpcmp(&name[pos], "A") ||
          0 == strpcmp(&name[pos], "a")) {
        param->uart.id = uart_name_a;
      } else if (0 == strpcmp(&name[pos], "B") ||
                 0 == strpcmp(&name[pos], "b")) {
        param->uart.id = uart_name_b;
      } else if (0 == strpcmp(&name[pos], "C") ||
                 0 == strpcmp(&name[pos], "c")) {
        param->uart.id = uart_name_c;
      } else {
        err("Invalid UART name. Available UARTs: A, B and C");
        return 1;
      }
      
      for (; name[pos] != '\0' && name[pos] != '.'; pos++);
    } else { /* select UART by number */
      if (name[pos] < '0' || name[pos] > '2') {
        err("Invalid UART number. Available UARTs: 0, 1 and 2");
        return 1;
      }
      
      param->uart.id = name[pos] - '0';
      pos ++;
    }
    
    if (0 == strcmp(&name[pos], ".rs485")) {
      param->type = par_uart_rs485;
      
      return 0;
    }
  } else if (0 == strpcmp(&name[pos], "led")) {
    pos += 3;

    if (name[pos] == '.') { /* select LED by name */
      pos ++;

      if (0 == strpcmp(&name[pos], "red") ||
          0 == strpcmp(&name[pos], "r")) {
        param->led.id = led_name_red;
      } else if (0 == strpcmp(&name[pos], "green") ||
                 0 == strpcmp(&name[pos], "g")) {
        param->led.id = led_name_green;
      } else if (0 == strpcmp(&name[pos], "yellow") ||
                 0 == strpcmp(&name[pos], "y")) {
        param->led.id = led_name_yellow;
      } else {
        err("Invalid LED name. Available LEDs: red, green and yellow");
        return 1;
      }
      
      for (; name[pos] != '\0' && name[pos] != '.'; pos++);
    } else { /* select LED by number */
      if (name[pos] < '0' || name[pos] > '2') {
        err("Invalid LED number. Available LEDs: 0, 1 and 2");
        return 1;
      }
      
      param->led.id = name[pos] - '0';
      pos ++;
    }
    
    if (0 == strcmp(&name[pos], ".evt") ||
        0 == strcmp(&name[pos], ".event")) {
      param->type = par_led_evt;
      
      return 0;
    } else if (0 == strcmp(&name[pos], ".act") ||
               0 == strcmp(&name[pos], ".action")) {
      param->type = par_led_act;
      
      return 0;
    }
  }
  
  err("Invalid parameter. Available parameters:");
  err("uartX.rs485 (on|off), ledX.evt (event) and ledX.act (action)");
  return 1;
}

typedef struct {
  enum {
    act_reboot,
  } type;
} act_info_t;

static int parse_act(const char *name,
                     act_info_t *action) {
  if (0 == strpcmp(name, "reboot")) {
    action->type = act_reboot;
  } else {
    err("Invalid action: %s", name);
    err("Use one of: reboot.");
    return 1;
  }
  
  return 0;
}

static const char *rs485_mode_name[] = {
  [0] = "off",
  [1] = "on",
};

static const char *led_evt_name[] = {
  [led_evt_unused] = "unused",
  [led_evt_connection] = "connection",
  [led_evt_control] = "control",
  [led_evt_transfer] = "transfer",
  [led_evt_reception] = "reception",
  [led_evt_transmission] = "transmission",
  [led_evt_transfer_error] = "errors",
};

static const char *led_evt_info[] = {
  [led_evt_unused] = "Turn off the LED",
  [led_evt_connection] = "LED on when USB connection established",
  [led_evt_control] = "LED indicate USB control requests",
  [led_evt_transfer] = "LED indicate ACM/UART data transfer",
  [led_evt_reception] = "LED indicate ACM/UART data reception",
  [led_evt_transmission] = "LED indicate ACM/UART data transmission",
  [led_evt_transfer_error] = "LED indicate data transfer errors",
};

static const char *led_act_name[] = {
  [led_act_light] = "light",
  [led_act_blink] = "blink",
  [led_act_flash] = "flash",
};

static const char *led_act_info[] = {
  [led_act_light] = "Simple on/off",
  [led_act_blink] = "Blinking",
  [led_act_flash] = "Flashing",
};

static int do_get(const par_info_t *param) {
  switch (param->type) {
  case par_uart_rs485: {
    struct hid_report_uart_opts req = {
      .report_id = HID_REPORT_RS485_MODE,
    };
    
    if (sizeof(req) == hid_get_feature_report(device, (void*)&req, sizeof(req))) {
      uint8_t mode = rs485_mode_get(req.uart.rs485, param->uart.id);
      log("%s", mode < countof(rs485_mode_name) ? rs485_mode_name[mode] : "invalid");
      return 0;
    }
    
    err("Unable to get UART RS485 mode.");
  } break;
  case par_led_evt: {
    struct hid_report_leds_opts req = {
      .report_id = HID_REPORT_LEDS_MODE,
    };
    
    if (sizeof(req) == hid_get_feature_report(device, (void*)&req, sizeof(req))) {
      uint8_t evt = led_evt_get(req.leds.mode[param->led.id]);
      log("%s (0x%02x)", evt < countof(led_evt_name) ? led_evt_name[evt] : "invalid", evt);
      return 0;
    }
    
    err("Unable to get LED mode event.");
  } break;
  case par_led_act: {
    struct hid_report_leds_opts req = {
      .report_id = HID_REPORT_LEDS_MODE,
    };
    
    if (sizeof(req) == hid_get_feature_report(device, (void*)&req, sizeof(req))) {
      uint8_t act = led_act_get(req.leds.mode[param->led.id]);
      log("%s (0x%02x)", act < countof(led_act_name) ? led_act_name[act] : "invalid", act);
      return 0;
    }
    
    err("Unable to get LED mode action.");
  } break;
  }
  
  return 1;
}

static int do_set(const par_info_t *param, const char *value) {
  switch (param->type) {
  case par_uart_rs485: {
    struct hid_report_uart_opts req = {
      .report_id = HID_REPORT_RS485_MODE,
    };
    
    if (sizeof(req) == hid_get_feature_report(device, (void*)&req, sizeof(req))) {
      uint8_t old_mode = rs485_mode_get(req.uart.rs485, param->uart.id);
      uint8_t new_mode = rs485_mode_min;

      for (; new_mode <= rs485_mode_max; new_mode++) {
        if (0 == strcmp(value, rs485_mode_name[new_mode])) {
          break;
        }
      }
      
      if (new_mode == old_mode) {
        /* nothing to do */
        return 0;
      }
      
      rs485_mode_set(req.uart.rs485, param->uart.id, new_mode);
      
      if (sizeof(req) == hid_send_feature_report(device, (void*)&req, sizeof(req))) {
        return 0;
      }
      
      err("Unable to set UART RS485 mode.");
    } else {
      err("Unable to get UART RS485 mode.");
    }
  } break;
  case par_led_evt: {
    struct hid_report_leds_opts req = {
      .report_id = HID_REPORT_LEDS_MODE,
    };
    
    if (sizeof(req) == hid_get_feature_report(device, (void*)&req, sizeof(req))) {
      uint8_t old_evt = led_evt_get(req.leds.mode[param->led.id]);
      uint8_t new_evt = led_evt_min;

      for (; new_evt <= led_evt_max; new_evt++) {
        if (0 == strcmp(value, led_evt_name[new_evt])) {
          break;
        }
      }

      if (new_evt > led_evt_max) {
        err("Invalid LED mode event. Available options:");
        
        uint8_t evt = led_evt_min;
        for (; evt <= led_evt_max; evt++) {
          err("  %s\t- %s", led_evt_name[evt], led_evt_info[evt]);
        }
        
        return 1;
      }
      
      if (new_evt == old_evt) {
        /* nothing to do */
        return 0;
      }
      
      led_evt_set(req.leds.mode[param->led.id], new_evt);
      
      if (sizeof(req) == hid_send_feature_report(device, (void*)&req, sizeof(req))) {
        return 0;
      }
      
      err("Unable to set LED mode event.");
    } else {
      err("Unable to get LED mode event.");
    }
  } break;
  case par_led_act: {
    struct hid_report_leds_opts req = {
      .report_id = HID_REPORT_LEDS_MODE,
    };
    
    if (sizeof(req) == hid_get_feature_report(device, (void*)&req, sizeof(req))) {
      uint8_t old_act = led_act_get(req.leds.mode[param->led.id]);
      uint8_t new_act = led_act_min;

      for (; new_act <= led_act_max; new_act++) {
        if (0 == strcmp(value, led_act_name[new_act])) {
          break;
        }
      }

      if (new_act > led_act_max) {
        err("Invalid LED mode action. Available options:");
        
        uint8_t act = led_act_min;
        for (; act <= led_act_max; act++) {
          err("  %s\t- %s", led_act_name[act], led_act_info[act]);
        }
        
        return 1;
      }
      
      if (new_act == old_act) {
        /* nothing to do */
        return 0;
      }
      
      led_act_set(req.leds.mode[param->led.id], new_act);
      
      if (sizeof(req) == hid_send_feature_report(device, (void*)&req, sizeof(req))) {
        return 0;
      }
      
      err("Unable to set LED mode action.");
    } else {
      err("Unable to get LED mode action.");
    }
  } break;
  }
  
  return 1;
}

static int do_run(const act_info_t *action) {
  switch (action->type) {
  case act_reboot: {
    struct hid_report_reboot req = {
      .report_id = HID_REPORT_REBOOT,
    };
    
    if (sizeof(req) == hid_send_feature_report(device, (void*)&req, sizeof(req))) {
      return 0;
    }
    
    err("Unable to run reboot action.");
  } break;
  }
  
  return 1;
}

static inline int match_cmd(const char *arg,
                            const char *long_cmd,
                            const char *short_cmd) {
  return (short_cmd != NULL && 0 == strcmp(arg, short_cmd))
    || (long_cmd != NULL && 0 == strcmp(arg, long_cmd));
}

int main(int argc, char* argv[]) {
  { /* set program name */
    const char *str = strrchr(argv[0], '/');
    
    program = str == NULL ? argv[0] : str + 1;
  }
  
  if (argc == 1) {
    return help();
  }

  int res;
  
  res = hid_init();

  if (res != 0) {
    err("Unable to initialize HID API.");
    return res;
  }
  
  int argi = 1;
  
  for (; argi < argc && res == 0; argi++) {
    if (match_cmd(argv[argi], "list", "l")) {
      res = do_list();
    } else if (device == NULL) {
      res = do_open(argv[argi]);
    } else if (match_cmd(argv[argi], "get", "g")) {
      if (argi + 1 < argc) {
        par_info_t param;
        res = parse_par(argv[argi + 1], &param);
        if (res == 0) {
          res = do_get(&param);
          argi += 1;
        }
      } else {
        err("Missing parameter name within the get command.");
        res = 1;
      }
    } else if (match_cmd(argv[argi], "set", "s")) {
      if (argi + 2 < argc) {
        par_info_t param;
        res = parse_par(argv[argi + 1], &param);
        if (res == 0) {
          res = do_set(&param, argv[argi + 2]);
          argi += 2;
        }
      } else if (argi + 1 < argc) {
        err("Missing parameter value within the set command.");
        res = 2;
      } else {
        err("Missing parameter name within the set command.");
        res = 1;
      }
    } else if (match_cmd(argv[argi], "run", "r")) {
      if (argi + 1 < argc) {
        act_info_t action;
        res = parse_act(argv[argi + 1], &action);
        if (res == 0) {
          res = do_run(&action);
          argi ++;
        }
      } else {
        err("Missing operation name within the run command.");
        res = 1;
      }
    } else {
      err("Invalid command: %s", argv[argi]);
      res = 3;
    }
  }
  
  do_close();

  {
    int res_ = hid_exit();
    
    if (res == 0 && res_ != 0) {
      err("Unable to finalize HID API.");
      return res_;
    }
  }

  return res;
}
