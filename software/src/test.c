#include <string.h>
#include <stdlib.h>

#include <fcntl.h>
#include <unistd.h>
#include <termios.h>
#include <errno.h>

#include "macro.h"
#include "helper.h"
#include "outlog.h"

#define strpcmp(str, cst) strncmp(str, cst, sizeof(cst)-1)

static const char *program = NULL;

static int help(void) {
  log("Usage: %s [device] <test-command>" NL, program);
  log("Test commands:");
  log("  echo <l> <c> <tty1> <tty2> [<b> <d> <s> <p>] - test closed to itself UART");
  log("  ping <l> <c> <tty1> <tty2> [<b> <d> <s> <p>] - test data send and receive");
  log("Parameters:");
  log("  <l>     - data chunk length in bytes");
  log("  <c>     - data chunks count");
  log("  <tty(X)> - serial device(s) to use");
  log("  <b>     - baud rate");
  log("  <d>     - data bits (usually 5, 6, 7 or 8)");
  log("  <s>     - stop bits (one of 1, 1.5 or 2)");
  log("  <p>     - parity (none, odd or even)");
  
  return 0;
}

typedef struct {
  enum {
    cmd_echo,
    cmd_ping,
  } type;
  unsigned chunk;
  unsigned count;
  int tty[2];
} cmd_t;

typedef struct {
  speed_t baudrate;
  tcflag_t databits;
  tcflag_t stopbits;
  tcflag_t parity;
} cfg_t;

static int setup(int fd, const cfg_t *cfg) {
  struct termios tty;
  
  memset(&tty, 0, sizeof(tty));
  
  if (0 != tcgetattr(fd, &tty)) {
    err("error %d from tcgetattr", errno);
    return 1;
  }
  
  cfsetospeed(&tty, cfg->baudrate);
  cfsetispeed(&tty, cfg->baudrate);

  tty.c_cc[VMIN]  = 0; /* read doesn't block */
  tty.c_cc[VTIME] = 1; /* 100 mS read timeout */

  tty.c_lflag = 0; /* no signaling chars, no echo, no canonical processing */

  tty.c_iflag &= ~(0
                   | IGNBRK /* disable break processing */
                   | IXON | IXOFF | IXANY /* disable xon/xoff ctrl */
                   );

  tty.c_oflag = 0; /* no remapping, no delays */

  tty.c_cflag &= ~(0
                   | CSIZE /* reset data bits */
                   | CSTOPB /* reset stop bits */
                   | (PARENB | PARODD) /* reset parity */
                   | CRTSCTS /* disable rts/cts */
                   );
  
  tty.c_cflag |= 0
    | cfg->databits /* select data bits */
    | cfg->stopbits /* select stop bits */
    | cfg->parity /* select parity */
    | CLOCAL /* ignore modem controls */
    | CREAD /* enable reading */
  ;
  
  if (0 != tcsetattr(fd, TCSANOW, &tty)) {
    err("error %d from tcsetattr", errno);
    return 1;
  }
  
  return 0;
}

#define min(a, b) ((a) < (b) ? (a) : (b))

static const struct {
  char a, b;
} ranges[] = {
  {'0', '9'},
  {'A', 'Z'},
  {'a', 'z'},
  {':', ';'},
};

static void mkdata(char *buf, unsigned len) {
  unsigned i = 0;
  unsigned r = 0;
  
  for (; i < len; ) {
    unsigned l = min((unsigned)(ranges[r].b - ranges[r].a + 1), len - i);
    unsigned c = 0;
    
    for (; c < l; c++, i++) {
      buf[i] = ranges[r].a + c;
    }
    
    if (r < countof(ranges) - 1) {
      r++;
    } else {
      r = 0;
    }
  }
}

static int ckdata(const char *buf1, const char *buf2, unsigned len) {
  unsigned i = 0;

  for (; i < len; i++) {
    if (buf1[i] != buf2[i]) {
      return i;
    }
  }

  return -1;
}

static int doecho(const cmd_t *cmd) {
  unsigned i = 0;
  ssize_t wlen;
  ssize_t rlen;
  char wbuf[cmd->chunk];
  char rbuf[cmd->chunk];
  int epos;
  
  for (; i < cmd->count; i++) {
    mkdata(wbuf, cmd->chunk);

    wlen = write(cmd->tty[0], wbuf, cmd->chunk);
    if (0 > wlen) {
      err("Unable to write data.");
      return errno;
    } else if (cmd->chunk != wlen) {
      err("Unable to send %u bytes", (unsigned)wlen);
      return 1;
    }

#if 1
    {
      unsigned roff = 0;
    recv:
      rlen = read(cmd->tty[1], rbuf + roff, cmd->chunk - roff);
      if (0 > rlen) {
        err("Unable to read data.");
        return errno;
      } else {
        roff += rlen;
        if (roff < cmd->chunk) {
          goto recv;
        } else {
          rlen = roff;
        }
      }
    }
#else
    rlen = read(cmd->tty[1], rbuf, cmd->chunk);
    if (0 > rlen) {
      err("Unable to read data.");
      return errno;
    }
#endif
    
    if (wlen != rlen) {
      err("Mismatch data length (sent %u bytes but received %u).", (unsigned)wlen, (unsigned)rlen);
      return 2;
    }

    epos = ckdata(wbuf, rbuf, cmd->chunk);
    if (epos >= 0) {
      err("Mismatch data at position %u (sent %c but received %c).", (unsigned)epos, wbuf[epos], rbuf[epos]);

      {
        char pbuf[cmd->count];
        unsigned p = 0;
        
        for (; p < (unsigned)epos; pbuf[p++] = ' ');
        pbuf[epos] = '^';
        pbuf[epos + 1] = '\0';
        
        err("sent:     [%s]", wbuf);
        err("           %s", pbuf);
        err("received: [%s]", rbuf);
      }
      
      return 2;
    }
    
    log("Test #%u PASSED", i + 1);
  }

  return 0;
}

static int doping(cmd_t *cmd) {
  (void)cmd;
  
  return 0;
}

int main(int argc, char* argv[]) {
  { /* set program name */
    const char *str = strrchr(argv[0], '/');
    
    program = str == NULL ? argv[0] : str + 1;
  }

  int res = 1;
  
  if (argc == 1) {
    res = help();
    goto exit;
  }
  
  cmd_t cmd;
  
  if (0 == strpcmp(argv[1], "echo")) {
    cmd.type = cmd_echo;
    if (argc < 4) {
      err("Not enough arguments for echo command.");
      goto exit;
    }
  } else if (0 == strpcmp(argv[1], "ping")) {
    cmd.type = cmd_ping;
    if (argc < 5) {
      err("Not enough arguments for ping command.");
      goto exit;
    }
  } else {
    err("Invalid command: %s.", argv[1]);
    goto exit;
  }

  cmd.chunk = atoi(argv[2]);
  cmd.count = atoi(argv[3]);
  
  cmd.tty[0] = open(argv[4], O_RDWR | O_NOCTTY | O_SYNC);
  cmd.tty[1] = -1;
  
  if (cmd.tty[0] < 0) {
    err("Unable to open tty: %s", argv[4]);
    res = cmd.tty[0];
    goto stop;
  }
  
  if (0 != strcmp(argv[4], argv[5])) {
    cmd.tty[1] = open(argv[5], O_RDWR | O_NOCTTY | O_SYNC);
    
    if (cmd.tty[1] < 0) {
      err("Unable to open tty: %s", argv[5]);
      res = cmd.tty[1];
      goto stop;
    }
  } else {
    cmd.tty[1] = cmd.tty[0];
  }

  {
    cfg_t cfg;
    
    if (6 < argc) {
      switch (atoi(argv[6])) {
#define select_baudrate(br) case br: cfg.baudrate = _CAT2(B, br); break;
        _MAP(select_baudrate, 0, 50, 75, 110, 134, 150, 200, 300, 600, 1200, 1800, 2400, 4800, 9600, 19200, 38400, 57600, 115200, 230400, 460800, 500000, 576000, 921600, 1000000, 1152000, 1500000, 2000000, 2500000, 3000000, 3500000, 4000000)
#undef select_baudrate
      default:
        err("Unsupported baudrate: %s. Use one of: 0, 50, 75, 110, 134, 150, 200, 300, 600, 1200, 1800, 2400, 4800, 9600, 19200, 38400, 57600, 115200, 230400, 460800, 500000, 576000, 921600, 1000000, 1152000, 1500000, 2000000, 2500000, 3000000, 3500000 or 4000000.", argv[6]);
        goto stop;
      }
    } else {
      cfg.baudrate = B115200; /* 115200 bauds by default */
      goto start;
    }
    
    if (7 < argc) {
      switch (atoi(argv[7])) {
#define select_databits(db) case db: cfg.databits = _CAT2(CS, db); break;
        _MAP(select_databits, 5, 6, 7, 8)
#undef select_databits
      default:
        err("Unsupported databits: %s. Use one of: 5, 6, 7 or 8.", argv[7]);
        goto stop;
      }
    } else {
      cfg.databits = CS8; /* 8 data bits by default */
      goto start;
    }
  
    if (8 < argc) {
      if (0 == strpcmp(argv[8], "1")) {
        cfg.stopbits = 0;
      } else if (0 == strpcmp(argv[8], "2")) {
        cfg.stopbits = CSTOPB;
      } else {
        err("Unsupported stopbits: %s, Use 1 or 2.", argv[8]);
        goto stop;
      }
    } else {
      cfg.stopbits = 0; /* 1 stop bit by default */
      goto start;
    }

    if (9 < argc) {
      if (0 == strpcmp(argv[9], "none")) {
        cfg.parity = 0;
      } else if (0 == strpcmp(argv[9], "even")) {
        cfg.parity = PARENB;
      } else if (0 == strpcmp(argv[9], "odd")) {
        cfg.parity = PARENB | PARODD;
      } else {
        err("Unsupported parity: %s. Use one of none, even or odd.", argv[9]);
        goto stop;
      }
    } else {
      cfg.parity = 0;
    }
    
  start:
    setup(cmd.tty[0], &cfg);
    
    if (cmd.tty[0] != cmd.tty[1]) {
      setup(cmd.tty[1], &cfg);
    }
  }

  switch (cmd.type) {
  case cmd_echo:
    res = doecho(&cmd);
    break;
  case cmd_ping:
    res = doping(&cmd);
    break;
  }
  
 stop:
  if (cmd.tty[0] >= 0) {
    close(cmd.tty[0]);
  }

  if (cmd.tty[0] != cmd.tty[1] &&
      cmd.tty[1] >= 0) {
    close(cmd.tty[1]);
  }
  
 exit:
  return res;
}
