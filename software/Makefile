# Base path to build root
BASEPATH := $(or $(subst $(dir $(abspath $(CURDIR)/xyz)),,$(dir $(abspath $(lastword $(MAKEFILE_LIST))))),./)

FWDIR := $(BASEPATH)../firmware/
MKDIR := $(FWDIR).rules
LIBSDIR := $(FWDIR).libs
INCDIR := $(BASEPATH)include
SRCDIR := $(BASEPATH)src

-include config.mk

# Common
include $(MKDIR)/macro.mk
include $(MKDIR)/option.mk
include $(MKDIR)/build.mk
include $(MKDIR)/stalin.mk
include $(MKDIR)/pkg-config.mk

TARGET.LIBS += libtool
libtool.INHERIT := software libhidapi
libtool.SRCS := $(addprefix $(SRCDIR)/,\
  tool.c)

TARGET.OPTS += libtool
libtool.OPTS := $(SRCDIR)/software.cf

TARGET.BINS += tool
tool.NAME := usb_serial3_tool
tool.INHERIT := software libtool
tool.DEPLIBS += libhidapi
tool.DEPLIBS* += libtool

TARGET.LIBS += libtest
libtest.INHERIT := software
libtest.SRCS := $(addprefix $(SRCDIR)/,\
  test.c)

TARGET.BINS += test
test.INHERIT := software libtest
test.DEPLIBS* += libtest

# Application
software.INHERIT += stalin firmware
software.CDIRS += $(INCDIR)
software.CDIRS += $(FWDIR)include
software.CDIRS += $(LIBSDIR)/helper/include
software.LDDIRS += $(LDDIR)
software.GDBOPTS += -ex 'set pagination off'

$(call use_toolchain,software,$(COMPILER_NAME))

# Provide rules
$(call ADDRULES,\
OPT_RULES:TARGET.OPTS)

$(call pkg-config,libhidapi,hidapi-$(hidapi.backend))

udev.rules := etc/99-usb_serial-hid.rules
udev.subsystem.libusb := usb
udev.subsystem.hidraw := hidraw
build: build.udev.rules
build.udev.rules: $(udev.rules)
$(udev.rules):
	$(Q)echo 'UDEV GEN $@'
	@mkdir -p $(dir $@)
	@echo '# Generated UDEV rules for USB Serial 3-port adapter' > $@
	@echo >> $@
	@echo '# Forces ModemManager to ignore attached ttyACMx devices' >> $@
	@echo '$(if $(call option-true,$(udev.modem_manager.ignore)),,# )ATTRS{idVendor}=="$(usb.vendor.id)", ATTRS{idProduct}=="$(usb.product.id)", ENV{ID_MM_DEVICE_IGNORE}="1"' >> $@
	@echo >> $@
	@echo '# Config through HID using hidapi-$(hidapi.backend)' >> $@
	@echo '$(if $(call option-true,$(udev.hid.rules)),,# )SUBSYSTEM=="$(udev.subsystem.$(hidapi.backend))", ATTRS{idVendor}=="$(usb.vendor.id)", ATTRS{idProduct}=="$(usb.product.id)"$(if $(udev.group),$(strip ,) GROUP="$(udev.group)"), MODE="$(udev.mode)"' >> $@

clean: clean.udev.rules
clean.udev.rules:
	$(Q)echo 'UDEV CLEAN $(udev.rules)'
	@rm -f $(udev.rules)

# Provide rules
$(call ADDRULES,\
LIB_RULES:TARGET.LIBS\
BIN_RULES:TARGET.BINS)
